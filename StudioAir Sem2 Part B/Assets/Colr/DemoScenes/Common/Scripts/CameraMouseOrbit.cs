﻿using UnityEngine;
using System.Collections;

public class CameraMouseOrbit : MonoBehaviour {

		public Transform target;
		public float distance = 5.0f;

    public float minRendDistance = 50;
    public float maxRendDistance = 150;
    float xSpeed = 120.0f;
		 float ySpeed = 120.0f;

		 float yMinLimit = -20f;
		 float yMaxLimit = 80f;

		public float distanceMin = .5f;
		public float distanceMax = 15f;

		float x = 0.0f;
		float y = 0.0f;

        [Space(20)]
         bool autoMovement = false;
         float autoSpeedX = 0.2f;
         float autoSpeedY = 0.1f;
         float autoSpeedDistance = -0.1f;
        
		// Use this for initialization
		void Start () 
		{
				Vector3 angles = transform.eulerAngles;
				x = angles.y;
				y = angles.x;

				Rigidbody rigidbody = GetComponent<Rigidbody>();

				// Make the rigid body not change rotation
				if (rigidbody != null)
				{
						rigidbody.freezeRotation = true;
				}
		}

    void LateUpdate()
    {
        if (target || true)
        {
            if (Input.touchCount == 2)
            {
                if ((Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary) && (Input.GetTouch(1).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Stationary))
                {
                    //Vector2 deltaPos = (Input.GetTouch(0).deltaPosition + Input.GetTouch(0).deltaPosition) / 2;
                    //x += deltaPos.x * xSpeed * 0.02f;
                    //y += deltaPos.y * ySpeed * 0.02f;
                    if (Input.GetTouch(0).position.x < Input.GetTouch(1).position.x)
                    {
                        Vector2 deltPosLeft = (Input.GetTouch(0).deltaPosition - Input.GetTouch(1).deltaPosition);
                        distance = Mathf.Clamp(distance + deltPosLeft.x * 0.5f, distanceMin, distanceMax);

                    } else
                    {
                        Vector2 deltPosLeft = (Input.GetTouch(1).deltaPosition - Input.GetTouch(0).deltaPosition);
                        distance = Mathf.Clamp(distance + deltPosLeft.x * 0.5f, distanceMin, distanceMax);
                    }
                }
            }
           
           /*
            if (false && Input.GetMouseButton(0))
            {
                x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
            }
            */

            else if (autoMovement)
            {
                x += autoSpeedX * distance * 0.2f;
                y += autoSpeedY;
                distance += autoSpeedDistance;
            }


            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            //distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

            RaycastHit hit;
            if (Physics.Linecast(target.position, transform.position, out hit))
            {
               // distance -= hit.distance;
            }
            

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance ;//+ target.position;

            //transform.rotation = rotation;
           transform.localPosition = position;
        }
      //  CAMeshRender._RenderDist = (int)distance.normalMap(distanceMin, distanceMax, minRendDistance, maxRendDistance);
    }

		public static float ClampAngle(float angle, float min, float max)
		{
				if (angle < -360F)
						angle += 360F;
				if (angle > 360F)
						angle -= 360F;
				return Mathf.Clamp(angle, min, max);
		}
}
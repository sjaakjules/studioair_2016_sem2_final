﻿using UnityEngine;
using System.Collections;

public class MoveToTerrain : MonoBehaviour {
    Vector3 tempPosition;
    Vector3 startPosition;
    float tempIntensity;
    public float heightChange = 1;

    Transform parentsTransform;

    int index;

    public bool GridPositionIsConstant = true;

	// Use this for initialization
	void Start ()
    {
        startPosition = transform.position;
        index = CSV.FindIndex(transform.position);
        tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, index);
        if (!float.IsNaN(tempIntensity))
        {
            tempPosition = transform.position;
            tempPosition.y = startPosition.y + CSV.getIntensity(SingleMeshRender._intensityMap, index) + heightChange;
            transform.position = tempPosition;
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GridPositionIsConstant == false)
        {
            index = CSV.FindIndex(transform.position);
        }
        tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, index);
        if (!float.IsNaN(tempIntensity))
        {
            tempPosition = transform.position;
            tempPosition.y = startPosition.y + CSV.getIntensity(SingleMeshRender._intensityMap, index) + heightChange;
            transform.position = tempPosition;

        }
    }
}

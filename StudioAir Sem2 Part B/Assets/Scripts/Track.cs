﻿using UnityEngine;
using System.Collections;

public class Track : MonoBehaviour {

    public GameObject target;
    int index;

    // Use this for initialization
    void Start () 
	{
        //agentList = GameObject.FindGameObjectsWithTag("Agent1");
        //target = agentList[0];

        target = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () 
	{
        /*
		if (newTarget == true)
		{
            if (index < agentList.Length)
			{
				index = index + 1;
			}

			else
			{
                index = 0;
            }
			
			target = agentList[index];
        }
		*/

		if (target != null)
		{
		transform.position = target.transform.position;
        transform.rotation = target.transform.rotation;
		}

		else
		{
            target = GameObject.FindGameObjectWithTag("Player");
        }

    }
}

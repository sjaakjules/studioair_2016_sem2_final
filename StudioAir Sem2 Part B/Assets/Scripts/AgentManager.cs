﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



public class AgentManager : MonoBehaviour
{
    

    [Header("Load your Agents here:")]
    public GameObject[] Agent;



    [Header("System Population")]
    public int InitialNumberOfAgents = 10;
    public int maxNumberOfAgents = 10;
    public int minNumberOfAgents = 0;



    public CSVmaps AgentsSpawnFromThisCSV = CSVmaps.Topo;

    public bool spawnInitialAgentsRandomly = true;
    public bool canReproduce = true;

    public static int _maxNumberOfAgents;
    public static int _minNumberOfAgents;
    public static int _InitialNumberOfAgents;
    public static bool _canReproduce;
    public static int nAgentsTypes;

    public static int spawnedAgents;
    


    // Use this for initialization
    void Start()
    {
        updateStaticVariables();

        nAgentsTypes = Agent.Length;

        if (spawnInitialAgentsRandomly)
        {
            SpawnRandomAgentRandomly();
        }
        else
        {
            List<Vector3> intensityMap = CSV.dataMap;

            // This will randomise the list so you can limit how many nodes you will generate.
            List<int> indicie = Enumerable.Range(0, CSV.dataMap.Count).ToList();
            indicie.Shuffle();

            // The For loop will cycle over the shuffled list of the data points within the loaded CSV intesity/data maps.
            for (int i = 0; i < intensityMap.Count; i++)
            {
                if (spawnedAgents >= maxNumberOfAgents)
                {
                    break;
                }
                else
                {
                    Vector3 Position = intensityMap[indicie[i]];

                    float SpawnIntensity = CSV.getIntensity(AgentsSpawnFromThisCSV, indicie[i]);

                    ////////////////////////////////////////////////////////////////////////////////
                    //  
                    //                              REF: 2A - Spawn Agents
                    //                  
                    //                      For agents spawning from CSV
                    //                      1       Specify the type of agent
                    //                      2       Count the number of agents spawned
                    //                      
                    ////////////////////////////////////////////////////////////////////////////////
                    if (SpawnIntensity > 5 && SpawnIntensity < 10)
                    {

                        GameObject tempObj = Instantiate(Agent[0], Position, Quaternion.identity) as GameObject;
                        tempObj.transform.parent = gameObject.transform;

                        spawnedAgents++;
                    }
                    else if (SpawnIntensity > 10)
                    {

                        GameObject tempObj = Instantiate(Agent[1], Position, Quaternion.identity) as GameObject;
                        tempObj.transform.parent = gameObject.transform;

                        spawnedAgents++;
                    }
                    
                    else if (SpawnIntensity >10)
                    {
                        GameObject tempObj = Instantiate(Agent[3], Position, Quaternion.identity) as GameObject;
                        tempObj.transform.parent = gameObject.transform;

                        spawnedAgents++;
                    }
                }
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        updateStaticVariables();
    }
    

    void SpawnRandomAgentRandomly()
    {
        for (int i = 0; i < InitialNumberOfAgents; i++)
        {
            int newAgentType = Random.Range(0, nAgentsTypes);
            Vector3 position = CSV.dataMap[Random.Range(0, CSV.dataMap.Count)];

            GameObject tempObj = Instantiate(Agent[newAgentType], position, Quaternion.identity) as GameObject;  // Creates an agent at a random location of the dataMap
            
            tempObj.transform.parent = gameObject.transform;
            tempObj.transform.name = newAgentType.ToString();

            spawnedAgents++;
        }
    }

    void updateStaticVariables()
    {
        _canReproduce = canReproduce;
        _maxNumberOfAgents = maxNumberOfAgents;
        _minNumberOfAgents = minNumberOfAgents;
        _InitialNumberOfAgents = InitialNumberOfAgents;
    }

}

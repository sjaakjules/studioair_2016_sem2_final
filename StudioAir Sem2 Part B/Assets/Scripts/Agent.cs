﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


public class Agent : MonoBehaviour {


    //private LineRenderer lineRenderer;

    // Public Variables (settings of the agent)

    [Header("1. What intensity map does the agent read")]
    public CSVmaps AgentReadsThisMap = 0;
    public bool moveAgentTowardsHigherValue = true;
    public float AgentSpeed;

    [Header("2. How does the agent read the landscape near by")]
    public int sightWidth;
    public float sightLength;


    [Header("3. Define how the agent affects intensity maps")]
    public float[] changeToIntensityValuePerMap;
    public bool changeSight = false;
    //CSVmaps[] CSVIntensityMapChange;

    [Header("4. What trail does the agent leave")]
    public GameObject AgentDropping;         // This is the gameObject which is placed where a pheromone is deposited


    [Header("5. Reproduction")]
    public CSVmaps AgentGainsEnergyFromThisMap;
    public float StartingEnergy = 1;
    public float energyLossPerMove = 0f;
    public float reproduceWhenThisEnegy = 50;
    public float energyAtThisMoment;

    bool LayPheromones = false;
    static bool haveSetupPheromones = false;
    
    static int totalAgents = 0;
    int agentNumber;
    bool hasSetupHistory = false;
    bool isUpdatingHistory = false;


    // Variables which vary each update
    Vector3 newDirection = Vector3.zero;        // This is the direction the agent will move each frame update, is reset to zero at the start of each update.
    Vector3 IntensityDirection = Vector3.zero;

    public int gridIndexAtAgent;                        // This is the index within the list of grid locations which is closest to this agent.
    GameObject PickupAtAgent;

    public List<int> gridIndiciesInSight;       // This is a list of indicies within the list of grid locations which is within the site of the agent.
    //List<GameObject> pickupsInSight;        // This is a list of pickups within the sight of the agent
    public List<float[]> intensityInSight;         // This is a list of the intensitys where the first intensity relates to the index within the indiciesInSight list.
    //List<Pheromone> pheromoneInSight;       // This is a list of pheromones in sight. 
    //List<int> pheromoneIndexInSight;       // This is a list of pheromones in sight.
    //List<int> pheromoneGameObjectIndex;

    List<Vector3> AgentPositionHistory = new List<Vector3>();
    List<float[]> AgentIntensityHistory = new List<float[]>();
    List<int> AgentIndexHistory = new List<int>();
    List<float> HistroyTime = new List<float>();
    
    //AgentType movementAgentType = 0;
    float pheromoneStartingValue;
    float pheromoneIncreaseValue;
    //bool moveTowardsPheromone = false;
    float VisualSizeChangeToPickup;
    


    // Use this for initialization
    void Start () {
                

        // Setup Complex Variables
        gridIndiciesInSight = new List<int>(CSV.dataMap.Count);
        //pickupsInSight = new List<GameObject>(CSV.dataMap.Count);
        intensityInSight = new List<float[]>(CSV.dataMap.Count);
        //pheromoneInSight = new List<Pheromone>(CSV.dataMap.Count);
        //pheromoneIndexInSight = new List<int>(CSV.dataMap.Count);
        //pheromoneGameObjectIndex = new List<int>(CSV.dataMap.Count);

        // Create an empty grid of pheromones, they will all be set with standard values, (strength=0)
        if (!haveSetupPheromones)
        {
            Pheromone.Grid = new Pheromone[CSV.dataMap.Count];
            haveSetupPheromones = true;
        }

        energyAtThisMoment = StartingEnergy;
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (SaveStateManager._saveAgentStates && !hasSetupHistory)
        {
            setupAgentHistory();
            InvokeRepeating("updateCSV", SaveStateManager._agentSaveStateTime* 1.5f, SaveStateManager._agentSaveStateTime);
        }
        
        // Find where a new agent will spawn, this is the map index next to the agent,
        gridIndexAtAgent = CSV.FindIndex(transform.position);

        if (gridIndexAtAgent < 0 || gridIndexAtAgent >= CSV.dataMap.Count)
        {
            transform.Rotate(0, 180, 0);
            transform.Translate(Vector3.forward * 2 * AgentSpeed * Time.deltaTime);//(transform.TransformVector(transform.InverseTransformVector(transform.forward) * 2 * AgentSpeed));
        }
        else
        {
            //updateEnergy();

            // Check all the grid locations and isolate the grid indicies / pickups / pheromones which are within sight
            CheckSight();

            RotateIntensity();



            // -------------------------------------------------------------------------------------
            //                   Change the intensity where the agent is
            // -------------------------------------------------------------------------------------
            for (int i = 0; i < changeToIntensityValuePerMap.Length; i++)
            {
                CSV.addToIntensityValue(changeToIntensityValuePerMap[i], (CSVmaps)i, gridIndexAtAgent);
            }


            // Add a pheromone, this will add a new gameobject to the grid point closest to the agent.
            if (LayPheromones)
            {
                //addPheromone();
            }

            // This will change the energy of the agent.
            updateEnergy();

            //PartCRules.AgentSeed(gridIndiciesInSight, gridIndexAtAgent);

        }
        if (SaveStateManager._saveAgentStates && !isUpdatingHistory && hasSetupHistory)
        {
            InvokeRepeating("addHistoryInfo", SaveStateManager._agentSaveStateTime, SaveStateManager._agentSaveStateTime);
            isUpdatingHistory = true;
        }
        else if (!SaveStateManager._saveAgentStates && isUpdatingHistory)
        {
            CancelInvoke("updateCSV");
            CancelInvoke("addHistoryInfo");
            hasSetupHistory = false;
            isUpdatingHistory = false;
        }

    }

    void FixedUpdate()
    {
        // This will move the agent forwards, the direction should have been modified above.
        moveAgent();
    }


    void OnValidate()
    {
        if (changeToIntensityValuePerMap.Length != CSV.nCSVMaps)
        {
            System.Array.Resize(ref changeToIntensityValuePerMap, CSV.nCSVMaps);
        }
    }

    void updateEnergy()
    {

        // loose energy each update
        energyAtThisMoment += energyLossPerMove;
        // Gain energy according to food map
        energyAtThisMoment += CSV.getIntensity(AgentGainsEnergyFromThisMap, gridIndexAtAgent);

        //////////////////////////////////
        //      Kill the Agent if no energy
        if (energyAtThisMoment <= 0)
        {
            AgentManager.spawnedAgents--;
            if (AgentManager.spawnedAgents < AgentManager._minNumberOfAgents)
            {
                randomeSpawnAgent();
            }
            Destroy(gameObject);
        }

        //////////////////////////////////
        //      Reproduce Agent if high energy
        if (AgentManager._canReproduce && AgentManager.spawnedAgents < AgentManager._maxNumberOfAgents && energyAtThisMoment >= reproduceWhenThisEnegy)
        {
            int newAgentIndex = gridIndexAtAgent;
            if (newAgentIndex <= 0)
            {
                newAgentIndex = 1;
            }
            else if (newAgentIndex >= CSV.dataMap.Count)
            {
                newAgentIndex = CSV.dataMap.Count - 2;
            }
            else
            {
                newAgentIndex++;
            }

            energyAtThisMoment = StartingEnergy;

            // Find the vector position of the new agent location
            Vector3 position = CSV.dataMap[newAgentIndex];

            // Create the new agent
            GameObject tempObj = Instantiate(gameObject, position, Quaternion.identity) as GameObject;  // Creates an agent at a random location of the dataMap

           // tempObj.GetComponent<Agent>().setupAgentHistory();

            // Add it to the agent manager
            tempObj.transform.parent = gameObject.transform.parent;
            AgentManager.spawnedAgents++;

        }
    }

    void randomeSpawnAgent()
    {
        int newAgentIndex = Random.Range(0,CSV.dataMap.Count);

        // Find the vector position of the new agent location
        Vector3 position = CSV.dataMap[newAgentIndex];

        // Create the new agent
        GameObject tempObj = Instantiate(gameObject, position, Quaternion.identity) as GameObject;  // Creates an agent at a random location of the dataMap
      //  tempObj.GetComponent<Agent>().setupAgentHistory();
        // Add it to the agent manager
        tempObj.transform.parent = gameObject.transform.parent;
        AgentManager.spawnedAgents++;
    }

    void CheckSight()
    {
        // variables unique to each gridpoint, 
        Vector3 gpFromAgent;
        float gpLengthFromAgent;
        float gpAngleFromAgent;
        GameObject tempPickup;


        //pickupsInSight.Clear();
        intensityInSight.Clear();
        //pheromoneInSight.Clear();
        //pheromoneIndexInSight.Clear();
        gridIndiciesInSight.Clear();

        int indexRange = Mathf.CeilToInt(sightLength / (2*HexMetrics.InnerRadius));
        int i;
        HexCoordinates agentLocation = HexCoordinates.hFi(gridIndexAtAgent);
        HexCoordinates neighbourLocation;
        for (int x = -indexRange; x <= indexRange; x++)
        {
            for (int z = -indexRange; z <= indexRange; z++)
            {
                if (Mathf.Abs(x + z) <= indexRange)
                {
                    neighbourLocation = new HexCoordinates(agentLocation.X + x, agentLocation.Z + z);
                    i = HexCoordinates.iFh(neighbourLocation);
                    if (i >= 0 && i < CSV.dataMap.Count)
                    {
                        //gpFromAgent = CSV.dataMap[i] - CSV.dataMap[gridIndexAtAgent]; //transform.InverseTransformPoint(CSV.dataMap[i]);
                        gpFromAgent = transform.InverseTransformPoint(CSV.dataMap[i]);
                        gpFromAgent.y = 0;

                        gpLengthFromAgent = gpFromAgent.magnitude;//Mathf.Sqrt(Mathf.Pow(gpFromAgent.x, 2) + Mathf.Pow(gpFromAgent.z, 2));

                        gpAngleFromAgent = Mathf.Rad2Deg * Mathf.Abs(2 * Mathf.Atan2(gpFromAgent.x, gpFromAgent.z));

                        if (gpLengthFromAgent >= 0 && gpLengthFromAgent <= sightLength && gpAngleFromAgent >= 0 && gpAngleFromAgent <= sightWidth)
                        {
                            gridIndiciesInSight.Add(i);

                            /*
                            if (SpawnPickups.SpawnedPickupIndicies.Contains(i))
                            {
                                tempPickup = SpawnPickups.SpawnedPickups[SpawnPickups.SpawnedPickupIndicies.FindIndex(xt => xt == i)];
                                if (tempPickup.activeInHierarchy)
                                {
                                    pickupsInSight.Add(SpawnPickups.SpawnedPickups[SpawnPickups.SpawnedPickupIndicies.FindIndex(xt => xt == i)]);
                                }
                            }
                            */
                            float[] intensityValues = CSV.getIntensities(i);
                            if (intensityValues != null)
                            {
                                intensityInSight.Add(intensityValues);
                            }
                            /*
                            if (Pheromone.Grid[i].totalStrength() != 0)
                            {
                                pheromoneInSight.Add(Pheromone.Grid[i]);
                                pheromoneIndexInSight.Add(i);
                            }
                            */
                        }
                    }
                }
            }
        }

        if (changeSight)
        {
            for (int j = 0; j < gridIndiciesInSight.Count; j++)
            {
                float distanceToAgent = Vector3.Distance(transform.position, HexCoordinates.pFi(gridIndiciesInSight[j]));
                for (int k = 0; k < changeToIntensityValuePerMap.Length; k++)
                {
                    CSV.addToIntensityValue(changeToIntensityValuePerMap[k] *( (1 - distanceToAgent / sightLength) < 0? 0 : (1 - distanceToAgent / sightLength)), (CSVmaps)k, gridIndiciesInSight[j]);
                }
            }
        }

    }

    public void setupAgentHistory()
    {
        agentNumber = totalAgents;
        totalAgents = totalAgents + 1;
        CSV.Savecsv(agentNumber, AgentPositionHistory, AgentIntensityHistory, AgentIndexHistory, HistroyTime,transform.name, true);
        hasSetupHistory = true;
        isUpdatingHistory = false;
    }

    void addHistoryInfo()
    {
        if (CSV.getIntensities(gridIndexAtAgent) != null)
        {
            AgentPositionHistory.Add(transform.position);
            AgentIndexHistory.Add(gridIndexAtAgent);
            AgentIntensityHistory.Add(CSV.getIntensities(gridIndexAtAgent));
            HistroyTime.Add(Time.time);
        }
    }

    void updateCSV()
    {
        CSV.Savecsv(agentNumber, AgentPositionHistory, AgentIntensityHistory, AgentIndexHistory, HistroyTime,transform.name, false);
    }

    void addPheromone()
    {
        // Check that the closest grid point doesnt have a pheromone, if it doesnt make one.
        if (Pheromone.Grid[gridIndexAtAgent].strength == null)
        {
            Pheromone.Grid[gridIndexAtAgent] = new Pheromone(pheromoneStartingValue, 0);

            // Create a game object at the grid locaiton.
            Vector3 pheromonePositon = CSV.dataMap[gridIndexAtAgent];
            pheromonePositon.y = 0;

            GameObject tempObj = Instantiate(AgentDropping, pheromonePositon, Quaternion.identity) as GameObject;        // Create the copy of the gameObject that the specified position "pheromonePosition"
            

            tempObj.transform.parent = gameObject.transform.parent;                                                   // Add the new gameobject to the "Pheromone Controller" so we can keep track of all the pheromones.
        }
        else // This means there is a pheromone at the closest grid point.
        {
            Pheromone.Grid[gridIndexAtAgent].strength[0] += pheromoneIncreaseValue;
        }
    }

    // The agent only moves forwards and depending on what it sees will change it's direction. The direction is controlled in the If statements of the Update Loop.
    void moveAgent()
    {
        // intensity direction is local
        /*
        if (!float.IsNaN(newDirection.x) || !float.IsNaN(newDirection.y) || !float.IsNaN(newDirection.z))
        {
            //currentTarget = Vector3.Lerp(currentTarget, newDirection, rotationSpeed);
            //transform.LookAt(transform.TransformPoint(newDirection));
            //newDirection = transform.TransformVector(newDirection * AgentSpeed);
            //newDirection.y = 0;
            //transform.Translate(newDirection);
            transform.LookAt(transform.TransformPoint(newDirection));
            Vector3 newPosition = transform.position + transform.TransformVector(Vector3.forward * AgentSpeed);
            // newPosition.y = CSV.data[2][gridIndicie];
            transform.position = newPosition;                   // The TransformVector(Vector3) will change local forwards to global coordinates.

        }
        */

        Vector3 newPosition = transform.position + transform.TransformVector(Vector3.forward * AgentSpeed*Time.fixedDeltaTime);
        transform.position = newPosition;                   // The TransformVector(Vector3) will change local forwards to global coordinates.
    }
    
    void RotateIntensity()
    {

        // -------------------------------------------------------------------------------------
        //       Move to the location of the largest intensity value
        // -------------------------------------------------------------------------------------

        // If there are gridpoints in sight react to the intensity of each of the grid points.
        if (intensityInSight.Count > 0)
        {
            // At the moment This will change the direction of this agent to point towards the grid location with the highest intensity value

            // Find the grid index with the highest intensity value
            int maxIntensitySightIndex = 0;
            float maxIntensity = intensityInSight[0][(int)AgentReadsThisMap];

            int minIntensitySightIndex = 0;
            float minIntensity = intensityInSight[0][(int)AgentReadsThisMap];

            for (int i = 1; i < intensityInSight.Count; i++)
            {
                if (intensityInSight[i][(int)AgentReadsThisMap] > maxIntensity)
                {
                    maxIntensitySightIndex = i;
                    maxIntensity = intensityInSight[i][(int)AgentReadsThisMap];
                }
                if (intensityInSight[i][(int)AgentReadsThisMap] < minIntensity)
                {
                    minIntensitySightIndex = i;
                    minIntensity = intensityInSight[i][(int)AgentReadsThisMap];
                }
            } 


            if (moveAgentTowardsHigherValue)
            {
                IntensityDirection = transform.InverseTransformPoint(CSV.dataMap[gridIndiciesInSight[maxIntensitySightIndex]]);
                newDirection = IntensityDirection.normalized;
                transform.LookAt(transform.TransformPoint(newDirection));
            }
            else
            {
                IntensityDirection = transform.InverseTransformPoint(CSV.dataMap[gridIndiciesInSight[minIntensitySightIndex]]);
                newDirection = IntensityDirection.normalized;
                transform.LookAt(transform.TransformPoint(newDirection));
            }

        }
        else
        {

            //IntensityDirection = transform.InverseTransformPoint(Quaternion.Euler(0,Random.Range(-sightWidth/2, sightWidth),0 )*transform.forward);
        }
    }


    void RotatePheromone()
    {
        /*
        if (pheromoneInSight.Count > 0)
        {
            float totalPheromoneStrength = 0;
            foreach (Pheromone pheromone in pheromoneInSight)
            {
                totalPheromoneStrength += pheromone.strength[(int)movementAgentType];
            }
            /*
            for (int i = 0; i < pheromoneInSight.Count; i++)
            {
                if (moveTowardsPheromone)
                {
                    PheromoneDirection += transform.InverseTransformPoint(CSV.dataMap[pheromoneIndexInSight[i]].normalized) * pheromoneInSight[i].strength[(int)movementAgentType] / totalPheromoneStrength;
                }
                else
                {
                    PheromoneDirection += transform.InverseTransformPoint(CSV.dataMap[pheromoneIndexInSight[i]].normalized) * pheromoneInSight[i].strength[(int)movementAgentType] / totalPheromoneStrength;
                    PheromoneDirection = PheromoneDirection * -1;
                }
            }

            // Find the grid index with the highest intensity value
            int maxPheromoneIndex = 0;
            float maxPheromone = pheromoneInSight[0].strength[(int)movementAgentType];

            int minPheromoneIndex = 0;
            float minPheromone = pheromoneInSight[0].strength[(int)movementAgentType];

            for (int i = 1; i < intensityInSight.Count; i++)
            {
                if (pheromoneInSight[i].strength[(int)movementAgentType] > maxPheromone)
                {
                    maxPheromoneIndex = i;
                    maxPheromone = intensityInSight[i][(int)AgentReadsThisMap];
                }
                if (pheromoneInSight[i].strength[(int)movementAgentType] < minPheromone)
                {
                    minPheromoneIndex = i;
                    minPheromone = intensityInSight[i][(int)AgentReadsThisMap];
                }
            }

            if (moveTowardsPheromone)
            {
                PheromoneDirection = transform.InverseTransformPoint(CSV.dataMap[pheromoneIndexInSight[maxPheromoneIndex]].normalized);
            }
            else
            {
                PheromoneDirection = transform.InverseTransformPoint(CSV.dataMap[pheromoneIndexInSight[maxPheromoneIndex]].normalized);
                PheromoneDirection = PheromoneDirection * -1;
            }
    }
            */
    }
    

    void pickupManipulation()
    {
        /*
        // -------------------------------------------------------------------------------------
        //
        //                              ADVANCE SETTINGS 
        //
        //                   For a type of agent, change the pickup
        // -------------------------------------------------------------------------------------
        if (agentType == AgentType.Type1)
        {
            if (SpawnPickups.SpawnedPickupIndicies.Contains(gridIndexAtAgent))
            {
                PickupAtAgent = SpawnPickups.SpawnedPickups[SpawnPickups.SpawnedPickupIndicies.FindIndex(x => x == gridIndexAtAgent)];
                if (PickupAtAgent.activeInHierarchy)
                {

                    // -------------------------------------------------------------------------------------
                    //
                    //                              ADVANCE SETTINGS 
                    //
                    //                   CHANGE THE PICKUP YOU ARE STANDING ON
                    // -------------------------------------------------------------------------------------
                    if (PickupAtAgent.CompareTag("PickupType1"))
                    {
                        if (PickupAtAgent.transform.localScale.x < 4)
                        {
                            PickupAtAgent.transform.localScale *= (1 + VisualSizeChangeToPickup); // Reduces the scale
                        }
                    }
                    else if (PickupAtAgent.CompareTag("PickupType2"))
                    {
                        if (PickupAtAgent.transform.localScale.x < 2)
                        {
                            PickupAtAgent.transform.localScale *= (1 + VisualSizeChangeToPickup);  // Increases the scale
                        }
                    }
                    else if (PickupAtAgent.CompareTag("PickupType3"))
                    {

                    }
                }
            }
        }
        */
    }
}

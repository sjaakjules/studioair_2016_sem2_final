﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MultiSizeSystem : MonoBehaviour
{


    public int maxNumberSize2 = 5;
    public int maxNumberSize3 = 5;
    public int maxNumberSize4 = 5;

    static int _maxNumberSize2;
    static int _maxNumberSize3;
    static int _maxNumberSize4;

    public int numberOfWeedsToCreateSize2 = 1000;
    public int numberOfWeedsToCreateSize3 = 3000;
    public int numberOfWeedsToCreateSize4 = 5000;

    static int _numberOfWeedsToCreateSize2;
    static int _numberOfWeedsToCreateSize3;
    static int _numberOfWeedsToCreateSize4;

    static Matrix4x4 voxTransform = Matrix4x4.identity;
    static MaterialPropertyBlock block;
    static int level;
    static float scaledHealth;

    public GameObject[] Corporation1 = new GameObject[5];
    public GameObject[] Corporation2 = new GameObject[5];
    public GameObject[] Corporation3 = new GameObject[5];
    public GameObject[] Corporation4 = new GameObject[5];
    static Mesh[] _Corporation1;
    static Mesh[] _Corporation2;
    static Mesh[] _Corporation3;
    static Mesh[] _Corporation4;

    public Material[] corporationMaterial1 = new Material[5];
    static Material[] _corporationMaterial1;
    public Material[] corporationMaterial2 = new Material[5];
    static Material[] _corporationMaterial2;
    public Material[] corporationMaterial3 = new Material[5];
    static Material[] _corporationMaterial3;
    public Material[] corporationMaterial4 = new Material[5];
    static Material[] _corporationMaterial4;


    public bool PreScaledMeshes = false;


    [Header("Low Poly Mesh and Materials")]
    public float PercentageChanceForLowPoly = 100;
    public static float _PercentageChanceForLowPoly;
    public GameObject[] LowPolyMesh = new GameObject[4];
    static Mesh[] _LowPolyMesh;

    public Material[] LowPolyMaterial = new Material[4];
    static Material[] _LowPolyMaterial;

    static bool _PreScaledMeshes;
    static Mesh CellMesh;
    static Material CellMaterial;


    // Use this for initialization
    void Start()
    {
        _Corporation1 = new Mesh[Corporation1.Length];
        _Corporation2 = new Mesh[Corporation2.Length];
        _Corporation3 = new Mesh[Corporation3.Length];
        _Corporation4 = new Mesh[Corporation4.Length];
        _LowPolyMesh = new Mesh[LowPolyMesh.Length];
        for (int i = 0; i < Corporation1.Length; i++)
        {
            _Corporation1[i] = Corporation1[i].GetComponent<MeshFilter>().sharedMesh;
        }
        for (int i = 0; i < Corporation2.Length; i++)
        {

            _Corporation2[i] = Corporation2[i].GetComponent<MeshFilter>().sharedMesh;
        }
        for (int i = 0; i < Corporation3.Length; i++)
        {

            _Corporation3[i] = Corporation3[i].GetComponent<MeshFilter>().sharedMesh;
        }
        for (int i = 0; i < Corporation4.Length; i++)
        {

            _Corporation4[i] = Corporation4[i].GetComponent<MeshFilter>().sharedMesh;
        }
        for (int i = 0; i < LowPolyMesh.Length; i++)
        {

            _LowPolyMesh[i] = LowPolyMesh[i].GetComponent<MeshFilter>().sharedMesh;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _maxNumberSize2 = maxNumberSize2;
        _maxNumberSize3 = maxNumberSize3;
        _maxNumberSize4 = maxNumberSize4;
        _numberOfWeedsToCreateSize2 = numberOfWeedsToCreateSize2;
        _numberOfWeedsToCreateSize3 = numberOfWeedsToCreateSize3;
        _numberOfWeedsToCreateSize4 = numberOfWeedsToCreateSize4;
        _PreScaledMeshes = PreScaledMeshes;
        _corporationMaterial1 = corporationMaterial1;
        _corporationMaterial2 = corporationMaterial2;
        _corporationMaterial3 = corporationMaterial3;
        _corporationMaterial4 = corporationMaterial4;
        _LowPolyMaterial = LowPolyMaterial;
        _PercentageChanceForLowPoly = PercentageChanceForLowPoly;
    }


    /// <summary>
    /// This is a dummy function which is never called and does nothing. 
    /// This has useful static variables which you can access in any script. Just copy and paste the lines below.
    /// </summary>
    void info()
    {
        // First thing for your new script is to have the two extra "Using" statements at the start of your script.
        //              using System.Collections.Generic;
        //              using System.Linq;



        //          Global lists of the cells that are larger:
        //              - CAManager.CellSizes[size]

        //      CAManager.CellSizes    ->   This is a list of the different size heCells in the system. 
        //                                  This is a list of lists, where the first square brackets specify what size 
        //                                  you want, and that will return a list of all the grid indicies that are that size.
        //          Ie, CAManager.CellSizes[1] will return a List<int> of all the grid locations that include the neighbours around them
        //              CAManager.CellSizes[2] will include the neighbours two cells away.  and so on.
        //


        //          Making you cells larger:

        //      HexCell.makeLarge(cell, gridIndex, newSize);   ->   This will make that cell large if it can. It cant 
        //                                                          if it's new size is in the build envelope of a larger size hexcell.  


    }

    // I have this function linked within the JulianRules.cs script. 
    //  You can use the inspector with CA Parameters game object to tune and modift the system.
    // The current system will generate a level 1 if it can, and then randomly larger levels after there are sufficient number of that weed species.
    public static void MakeLarger(HexCell cell, int gridIndex, List<HexDirection>[] Neighbours, List<HexDirection> hiddenNeighbours, System.Random num)
    {
        if (cell.health > 200)
        {
            // Make a level 1 cell if the neighbours are all the same and there are no surrounding larger cells.
            if (cell.NewSize < 0 && cell.type > CorpType.Tree && Neighbours[(int)cell.type].Count == 6 && hiddenNeighbours.Count == 0 && num.Next(0, 10000) < 3000)
            {
                cell.NewSize = 1;
                CAManager.CellSizes[cell.NewSize].Add(gridIndex);
                cell.largeIndex = gridIndex;
            }
            // Else have a random chance to make larger cells. Use the MakeLarge function to check and turn off larger cells.
            else if (cell.size < 2 && CAManager.CellSizes[2].Count() < _maxNumberSize2 && cell.type > CorpType.Tree && CAManager._typeCount[(int)cell.type] > _numberOfWeedsToCreateSize2 && num.Next(0, 1000) < 100)
            {
                HexCell.makeLarge(cell, gridIndex, 2);
            }
      //      else if (cell.size < 3 && CAManager.CellSizes[3].Count() < _maxNumberSize3 && cell.type > CorpType.Tree && CAManager._typeCount[(int)cell.type] > _numberOfWeedsToCreateSize3 && num.Next(0, 1000) < 100)
      //      {
      //          HexCell.makeLarge(cell, gridIndex, 3);
      //      }
            else if (cell.size < 4 && cell.health > 300 && CAManager.CellSizes[4].Count() < _maxNumberSize4 && cell.type > CorpType.Tree && CAManager._typeCount[(int)cell.type] > _numberOfWeedsToCreateSize4 && num.Next(0, 1000) < 100)
            {
                HexCell.makeLarge(cell, gridIndex, 4);
            }
        }
    }

    public static void MultiSizeRender(HexCell cell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {
        if (cell != null && cell.health > 0 )
        {
            cell.currentPosition = Vector3.Lerp(cell.currentPosition, cell.cellPosition, Time.deltaTime);
            level = cell.health / 100;  // This is zero for trees, then weeds will have 4 levels when their health is between 100-199 / 200-299 and so on
            scaledHealth = (1.0f * (cell.health - (100 * level) + 100) / 100);      // Scale health from Total 0 to 500 to relative 1 to 2 for each level


            if (cell.type == CorpType.Tree  )
            {
                if (cell.health > SuchRulesMuchCoding._StressDecreaseAmount + 10)    // ->   Replace "> JulianRules._StressDecreaseAmount" with "> 0" to show all trees, not jsut stressed ones
                {
                    //voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), cell.currentPosition.z), Quaternion.Euler(-0, 0, 0), new Vector3(1 + .3f * (1.0f * cell.health) / 1000f, 1 + .3f * (1.0f * cell.health) / 1000f, 1 + .3f * (1.0f * cell.health) / 1000f));
                    voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) +1, cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3((.3f * cell.health)/20, 0.0001f, (0.3f * cell.health)/20));
                    Graphics.DrawMesh(meshPerType[0], voxTransform, MaterialsPerType[0], 0, null, 0, block);

                }
            }
            else if (cell.type > CorpType.Tree && (!cell.isHidden || (cell.size == 4)))
            {
                if (cell.type == CorpType.Blackberry)
                {
                    if (cell.size == 0 && cell.health < 200)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        if (CSV.lowRender[gridIndex] == 0)
                        {
                            CellMesh = _Corporation1[0];
                            CellMaterial = _corporationMaterial1[0];
                        }
                        else
                        {
                            CellMesh = _LowPolyMesh[0];
                            CellMaterial = _LowPolyMaterial[0];
                        }
                    }
                    else if (cell.size == 0 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[1];
                        CellMaterial = _corporationMaterial1[1];
                    }
                    else if (cell.size == 0 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[2];
                        CellMaterial = _corporationMaterial1[2];

                    }
                    else if (cell.size == 0)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[3];
                        CellMaterial = _corporationMaterial1[3];

                    }

                    else if (cell.size == 1 && cell.health > 200 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[4];
                        CellMaterial = _corporationMaterial1[4];
                    }
                    else if (cell.size == 1 && cell.health > 300 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[5];
                        CellMaterial = _corporationMaterial1[5];
                    }
                    else if (cell.size == 1)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[6];
                        CellMaterial = _corporationMaterial1[6];
                    }


                    else if (cell.size == 2 && cell.health > 200 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[7];
                        CellMaterial = _corporationMaterial1[7];
                    }
                    else if (cell.size == 2)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[8];
                        CellMaterial = _corporationMaterial1[8];
                    }



                    else if (cell.size == 4)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation1[9];
                        CellMaterial = _corporationMaterial1[9];
                    }
                }






                else if (cell.type == CorpType.Fennel)
                {
                    if (cell.size == 0 && cell.health < 200)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        
                        if (CSV.lowRender[gridIndex] == 0)
                        {
                            CellMesh = _Corporation2[0];
                            CellMaterial = _corporationMaterial2[0];
                        }
                        else
                        {
                            CellMesh = _LowPolyMesh[1];
                            CellMaterial = _LowPolyMaterial[1];
                        }
                    }
                    else if (cell.size == 0 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[1];
                        CellMaterial = _corporationMaterial2[1];
                    }
                    else if (cell.size == 0 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[2];
                        CellMaterial = _corporationMaterial2[2];

                    }
                    else if (cell.size == 0)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[3];
                        CellMaterial = _corporationMaterial2[3];

                    }

                    else if (cell.size == 1 && cell.health > 200 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[4];
                        CellMaterial = _corporationMaterial2[4];
                    }
                    else if (cell.size == 1 && cell.health > 300 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[5];
                        CellMaterial = _corporationMaterial2[5];
                    }
                    else if (cell.size == 1)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[6];
                        CellMaterial = _corporationMaterial2[6];
                    }


                    else if (cell.size == 2 && cell.health > 200 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[7];
                        CellMaterial = _corporationMaterial2[7];
                    }
                    else if (cell.size == 2)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[8];
                        CellMaterial = _corporationMaterial2[8];
                    }



                    else if (cell.size == 4)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation2[9];
                        CellMaterial = _corporationMaterial2[9];
                    }

                }





                else if (cell.type == CorpType.Stinkwort)
                {
                    if (cell.size == 0 && cell.health < 200)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        
                        if (CSV.lowRender[gridIndex] == 0)
                        {
                            CellMesh = _Corporation3[0];
                            CellMaterial = _corporationMaterial3[0];
                        }
                        else
                        {
                            CellMesh = _LowPolyMesh[2];
                            CellMaterial = _LowPolyMaterial[2];
                        }
                    }
                    else if (cell.size == 0 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[1];
                        CellMaterial = _corporationMaterial3[1];
                    }
                    else if (cell.size == 0 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[2];
                        CellMaterial = _corporationMaterial3[2];

                    }
                    else if (cell.size == 0)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[3];
                        CellMaterial = _corporationMaterial3[3];

                    }

                    else if (cell.size == 1 && cell.health > 200 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[4];
                        CellMaterial = _corporationMaterial3[4];
                    }
                    else if (cell.size == 1 && cell.health > 300 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[5];
                        CellMaterial = _corporationMaterial3[5];
                    }
                    else if (cell.size == 1)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[6];
                        CellMaterial = _corporationMaterial3[6];
                    }


                    else if (cell.size == 2 && cell.health > 200 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[7];
                        CellMaterial = _corporationMaterial3[7];
                    }
                    else if (cell.size == 2)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[8];
                        CellMaterial = _corporationMaterial3[8];
                    }



                    else if (cell.size == 4)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation3[9];
                        CellMaterial = _corporationMaterial3[9];
                    }

                }





                else if (cell.type == CorpType.Hawthorn)
                {
                    if (cell.size == 0 && cell.health < 200)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        
                        if (CSV.lowRender[gridIndex] == 0)
                        {
                            CellMesh = _Corporation4[0];
                            CellMaterial = _corporationMaterial4[0];
                        }
                        else
                        {
                            CellMesh = _LowPolyMesh[3];
                            CellMaterial = _LowPolyMaterial[3];
                        }
                    }
                    else if (cell.size == 0 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[1];
                        CellMaterial = _corporationMaterial4[1];
                    }
                    else if (cell.size == 0 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[2];
                        CellMaterial = _corporationMaterial4[2];

                    }
                    else if (cell.size == 0)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[3];
                        CellMaterial = _corporationMaterial4[3];

                    }

                    else if (cell.size == 1 && cell.health > 200 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[4];
                        CellMaterial = _corporationMaterial4[4];
                    }
                    else if (cell.size == 1 && cell.health > 300 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[5];
                        CellMaterial = _corporationMaterial4[5];
                    }
                    else if (cell.size == 1)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[6];
                        CellMaterial = _corporationMaterial4[6];
                    }
                    else if (cell.size == 2 && cell.health > 200 && cell.health < 300)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[7];
                        CellMaterial = _corporationMaterial4[7];
                    }
                    else if (cell.size == 2 && cell.health > 300 && cell.health < 400)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[8];
                        CellMaterial = _corporationMaterial4[8];
                    }
                    else if (cell.size == 2 && cell.health > 400 && cell.health < 500)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[9];
                        CellMaterial = _corporationMaterial4[9];
                    }
                    else if (cell.size == 4)
                    {
                        voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) , cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, 1, 1));
                        CellMesh = _Corporation4[10];
                        CellMaterial = _corporationMaterial4[10];
                    }
                    
                }
                Graphics.DrawMesh(CellMesh, voxTransform, CellMaterial, 0, null, 0, block);
            }
        }
    }
}



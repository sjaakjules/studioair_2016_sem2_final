﻿using UnityEngine;
using System.Collections;

public class CAMeshRender : MonoBehaviour {

    public GameObject player;

    [Header("What shape do each Voxel type use?")]
    public GameObject[] MeshGeomPerType = new GameObject[HexCell.nTypes];


    [Header("What materials do each voxel type use?")]
    public Material[] MaterialsPerType = new Material[HexCell.nTypes];

	public MeshRendererStyles MeshRendererStylesPerType = MeshRendererStyles.Standard;

    [Header("How many voxels are drawn on screen?")]
    public int RenderDist;
    public static int _RenderDist;

    float cap = 0;
    
    Mesh[] meshPerType;
    MaterialPropertyBlock block;
    float health;
    HexCell CAcell;
    
    Matrix4x4 voxTransform = Matrix4x4.identity;

    HexCoordinates playerPosition;

    // SETUP - IGNORE
    void Start()
    {
        meshPerType = new Mesh[HexCell.nTypes];
        block = new MaterialPropertyBlock();

        for (int i = 0; i < HexCell.nTypes; i++)
        {
            meshPerType[i] = MeshGeomPerType[i].GetComponent<MeshFilter>().sharedMesh;
        }
    }


    // Update is called once per frame
    void Update()
    {
        _RenderDist = RenderDist;
        previewMaterials();

        playerPosition = HexCoordinates.hFp(player.transform.position);
        int i;
        HexCoordinates neighbourLocation;
        for (int x = -RenderDist; x <= RenderDist; x++)
        {
            for (int z = -RenderDist; z <= RenderDist; z++)
            {
                if (Mathf.Abs(x + z) <= RenderDist)
                {
                    neighbourLocation = new HexCoordinates(playerPosition.X + x, playerPosition.Z + z);
                    i = HexCoordinates.iFh(neighbourLocation);
                    if (i >= 0 && i < CSV.dataMap.Count)
                    {
                        CAcell = CAManager.CAGrid[i];

                        PartCRules.ApplyCustomMesh(CAcell, i, meshPerType, MaterialsPerType, MeshRendererStylesPerType);
                    }
                }
            }
        }
        
    }

    void OnValidate()
    {
        if (MeshGeomPerType.Length != HexCell.nTypes)
        {
            System.Array.Resize(ref MeshGeomPerType, HexCell.nTypes);
        }
        if (MaterialsPerType.Length != HexCell.nTypes)
        {
            System.Array.Resize(ref MaterialsPerType, HexCell.nTypes);
        }
    }

    void previewMaterials()
    {
        for (int i = 0; i < MaterialsPerType.Length; i++)
        {
            Vector3 currentPos = new Vector3(20 + i * 2, 1, 20);
            Graphics.DrawMesh(meshPerType[i], currentPos, Quaternion.identity, MaterialsPerType[i], 0, null, 0, block);
        }
    }

}

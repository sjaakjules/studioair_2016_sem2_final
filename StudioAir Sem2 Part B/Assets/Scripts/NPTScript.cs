﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPTScript : MonoBehaviour {
    


    // inspector Veariables
    public float spreadPercent = 0.4f;
    static float _spreadPercent;

    public float ignitionPercent = 0.0001f;
    static float _ignitionPercent;
    
    // temporary Variables
    static float tempIntensity;
    static int tempGridIndex;

    static MaterialPropertyBlock block;
    static Matrix4x4 voxTransform = Matrix4x4.identity;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        _spreadPercent = spreadPercent;
        _ignitionPercent = ignitionPercent;
    }
	/*
    public static void ComplexForestFireRules(HexCell cell, List<HexDirection> ShrubNeighbours, System.Random num)
    {
        tempGridIndex = HexCoordinates.iFh(cell.coordinates);
        tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, tempGridIndex);

        if (cell.type == CorpType.Empty && num.Next(0,100000) < (int)(_spreadPercent * 100))
        {
            cell.changeType(CorpType.shrubs, 1);
        }
        else if (cell.type == CorpType.shrubs && ShrubNeighbours.Count > 2 && num.Next(0, 10000) < (int)(_ignitionPercent * 100))
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (cell.type == CorpType.litFire)
        {
            cell.changeType(CorpType.Empty, 0);
        }
    }

    public static void StandardRenderer(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {

        if (CAcell != null && (int)CAcell.type > 0)
        {
            CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);

            voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(.8f, .8f, .8f) * 3);
            tempIntensity = CSV.getIntensity(CSVmaps.Map1, gridIndex);


            if (CAcell.health > 0)
            {
                if (CAcell.type != (CorpType)1)
                {
                    Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
                }

                else
                {
                    if (tempIntensity <= 2)
                    {
                        Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
                    }

                    else
                    {
                        voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f, .8f, 0.8f) * 3);
                        Graphics.DrawMesh(meshPerType[0], voxTransform, MaterialsPerType[0], 0, null, 0, block);
                    }
                }

            }
        }
    }
        */
}

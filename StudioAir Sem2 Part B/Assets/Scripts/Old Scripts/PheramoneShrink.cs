﻿using UnityEngine;
using System.Collections;

public class PheramoneShrink : MonoBehaviour {

    // Use this for initialization
    public float shrinkRate = 1;
    public float minSize = 0.7f;
    public int PheramoneType;

    public float pheromoneStrength = 0;
    int gridIndex;

    void Start () {

        gameObject.transform.position = new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z);

        gridIndex = CSV.FindIndex(transform.position);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Pheromone.Grid[gridIndex].strength == null)
        {
            Pheromone.Grid[gridIndex].strength = new float[AgentManager.nAgentsTypes];
        }
        else
        {
            pheromoneStrength = Pheromone.Grid[gridIndex].strength[PheramoneType];

            if (transform.localScale.x > minSize)
            {
                Pheromone.Grid[gridIndex].strength[PheramoneType] = Pheromone.Grid[gridIndex].strength[PheramoneType] * (1 - shrinkRate / 100);

                gameObject.transform.localScale = gameObject.transform.localScale * (1 - shrinkRate / 100);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CARules_SS : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    // ------------------------------------------------------------------------------------------------------------ //
    // ------------------------------------------------------------------------------------------------------------ //
    //                                       Don't Touch rules below                                                //
    // ------------------------------------------------------------------------------------------------------------ //
    // ------------------------------------------------------------------------------------------------------------ //


    /*
     * 
public static void GrowthHighLevel(HexCell cell, List<HexDirection> VegNeighbourDirections, List<HexDirection> EmptyNeighbourDirections, List<HexDirection> WasteNeighbourDirections, System.Random num, int maxHealth, int startHealth)
{
    //
    // Step 1 - health Rules
    // where/how will it gain/loose health?
    if (VegNeighbourDirections.Count > 0 && CSV.getIntensity(_growthIntensityMap, HexCoordinates.iFp(cell.CellPosition)) > _minGrowthValue)
    {
        cell.changeInHealth += plantHealthPerTurn;
    }
    else
    {
        cell.changeInHealth -= plantHealthPerTurn;
    }

    //
    // Step 2 - Direction
    // Make sure your pointing at a cell not edge of map.
    while (cell.GetNeighbor(cell.Direction) == null)
    {
        cell.Direction = (HexDirection)num.Next(0, 6);
    }

    //
    // Step 3 - Reproduction rules, 
    // when max health where will it duplicate?
    // This will reproduce to a random empty cell.
    if (cell.health >= maxHealth && EmptyNeighbourDirections.Count > 0)
    {
        (cell.GetNeighbor(EmptyNeighbourDirections[num.Next(EmptyNeighbourDirections.Count)])).changeType(cell.type, startHealth);
        cell.changeInHealth += -cell.health / 2;
    }

    //
    // Step 4 -  CA interaction rules,
    // There is no impact on other CA types so no rule within If statement
    if (cell.GetNeighbor(cell.Direction).type != HexType.Empty)
    {

    }

    //
    // Step 5 - Intensity Map interaction, 
    // Change an intensity map with the health of this cell.
    CSV.addToIntensityValue(cell.health - CSV.getIntensity(_VisuliseCAMap, HexCoordinates.iFp(cell.CellPosition)), _VisuliseCAMap, HexCoordinates.iFp(cell.CellPosition));
}


public static void GrowthLowAndUphill(HexCell cell, List<HexDirection> VegNeighbourDirections, List<HexDirection> EmptyNeighbourDirections, List<HexDirection> HigherNeighbours, System.Random num, int maxHealth, int startHealth)
{
    //
    // Step 1 - health Rules
    // where/how will it gain/loose health?
    if (CSV.getIntensity(_growthIntensityMap, HexCoordinates.iFp(cell.CellPosition)) < _minGrowthValue)
    {
        cell.changeInHealth += plantHealthPerTurn;
    }
    else
    {
        cell.changeInHealth -= plantHealthPerTurn;
    }

    //
    // Step 2 - Direction
    // Make sure your pointing at a cell not edge of map.
    while (cell.GetNeighbor(cell.Direction) == null)
    {
        cell.Direction = (HexDirection)num.Next(0, 6);
    }
    // Point the cell in a specific direction
    // for example the direction of the highest neighbour, Then we can grow if empty or invade if other type.
    if (HigherNeighbours.Contains(cell.Direction) == false)
    {
        foreach (HexDirection HigherNeighbour in HigherNeighbours)
        {
            if (cell.GetNeighbor(HigherNeighbour) != null)
            {
                cell.Direction = HigherNeighbour;
                break;
            }
        }
    }

    //
    // Step 3 -  Reproduction rules,
    // If fully grown split into two if there is an empty cell in the direction it is facing.
    if (cell.health >= maxHealth && cell.GetNeighbor(cell.Direction).type == HexType.Empty)
    {
        // Make the empty cell a plant
        (cell.GetNeighbor(cell.Direction)).changeType(cell.type, startHealth);
        cell.changeInHealth += -cell.health / 2;
    }


    //
    // Step 4 -  CA interaction rules,
    // If pointing to not an empty cell, reduce the health of that cell, no matter what type.
    if (cell.GetNeighbor(cell.Direction).type != HexType.Empty)
    {
        cell.GetNeighbor(cell.Direction).changeInHealth -= 2 * plantHealthPerTurn;
    }

    //
    // Step 5 - Intensity Map interaction, 
    // Change an intensity map with the health of this cell.
    CSV.addToIntensityValue(cell.health - CSV.getIntensity(_VisuliseCAMap, HexCoordinates.iFp(cell.CellPosition)), _VisuliseCAMap, HexCoordinates.iFp(cell.CellPosition));
}
public static void applyBehaviour(movementTypes movement, HexCell cell, List<HexDirection>[] NeighbourTypeDirections, float[][] NeighbourValues, System.Random num)
{
    switch (movement)
    {
        case movementTypes.Random:
            AgentCA.MoveRandomly(cell, NeighbourTypeDirections, num);
            break;
        case movementTypes.downHill:
            //AgentCA.MoveDownHill(cell, num, lowerFreq, lowestNeighbour, typeFreq[(int)HexType.Empty], NeighbourTypeDirections);
            break;
        case movementTypes.SameDirection:
            AgentCA.ChangeDirectionRandomly(cell, num);
            break;
        case movementTypes.BetweenValues:
            AgentCA.MoveBetween(cell, num, NeighbourValues[(int)CSVmaps.Topography]);
            break;
        case movementTypes.TowardsHighValue:
            AgentCA.MoveTowards(cell, num, NeighbourValues[(int)CSVmaps.Topography]);
            break;
        case movementTypes.AwayfromHighValue:
            AgentCA.MoveAway(cell, num, NeighbourValues[(int)CSVmaps.Topography]);
            break;
        case movementTypes.growthRules:
           // PartCRules.GrowthRules(cell, NeighbourTypeDirections[(int)HexType.Veg], NeighbourTypeDirections[(int)HexType.Empty], num, CAmanager._maxHealth);
            break;
        default:
            break;
    }
}


public static void GrowthRulesGuide(HexCell cell, int[] typeFreq, HexDirection[,] NeighbourTypeDirections, System.Random num, int plantHealthPerTurn, int maxHealth)
{
    if (typeFreq[(int)cell.type] > 1)
    {
        cell.changeInHealth += (int)CSV.getIntensity(CSVmaps.combined, HexCoordinates.iFp(cell.CellPosition));
    }
    else
    {
        //cell.changeInHealth -= plantHealthPerTurn;
    }

    // If fully grown split into two if there is an empty cell as a neighbour
    if (cell.health >= maxHealth && typeFreq[(int)HexType.Empty] > 0)
    {
        // find the indicie of the new neighbour
        int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
        // Make the empty cell a plant
        (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).changeType(cell.type, maxHealth / 2);
        cell.changeInHealth += -cell.health / 2;
    }
}


public static void AgentRulesGuide(HexCell cell, int[] typeFreq, HexDirection[,] NeighbourTypeDirections, System.Random num, int bacteriaWalkingHealthPerTurn, int bacteriaEatingHealthPerTurn, int bacteriaPlantLifeDrain, int maxHealth)
{

    // Change direction if edge of map
    while (cell.GetNeighbor(cell.Motion) == null)
    {
        cell.Motion = (HexDirection)num.Next(0, 6);
    }

    if (cell.GetNeighbor(cell.Motion).type != HexType.Veg && typeFreq[(int)HexType.Veg] > 0)
    {
        cell.Motion = NeighbourTypeDirections[(int)HexType.Veg, num.Next(typeFreq[(int)HexType.Veg])];
    }

    //  ////////////////////////////////////////////
    //  Do something in the direction it is facing
    switch (cell.GetNeighbor(cell.Motion).type)
    {
        case HexType.Empty:
            // if there is an empty locations, move there
            cell.changeInHealth += bacteriaWalkingHealthPerTurn;
            cell.GetNeighbor(cell.Motion).ReplaceCell(cell);
            break;
        case HexType.Veg:
            // If there is a plant hex infront of it, eat it. If you have full health, split
            cell.changeInHealth += bacteriaEatingHealthPerTurn;
            cell.GetNeighbor(cell.Motion).changeInHealth += bacteriaPlantLifeDrain;
            break;
        /*
    case HexType.Plague:
        // if it is a bacteria change direction
        cell.changeInHealth += bacteriaWalkingHealthPerTurn;
        do
        {
            cell.Motion = (HexDirection)num.Next(0, 6);
        } while (cell.GetNeighbor(cell.Motion) == null);
        break;
        default:
            break;
    }

    // ////////////////////////////////////////////
    //       Split if it is fully grown
    if (cell.health >= maxHealth && typeFreq[(int)HexType.Empty] > 0)
    {
        // find the indicie of the new neighbour
        int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
        // Make the empty cell a plant
        (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).changeType(cell.type, maxHealth / 2);
        cell.changeInHealth += -cell.health / 2;
    }
}

/*
/// <summary>
/// DON'T TOUCH
/// This is julians testing script, copy this one and modify but don't edit!
/// </summary>
/// <param name="cell"></param> This is the cell which is being modified
/// <param name="lowerFreq"></param> The number of cells lower than this one
/// <param name="lowerNeighbour"></param> The array of hexDirections which are lower
/// <param name="lowestNeighbour"></param> The neighbour which is lowest
/// <param name="typeFreq"></param> The types of the neighbours, either empty of water
/// <param name="lowestHeight"></param> The height of the lowest neighbour, Will be 9999f if there is nothing lower.
/// <param name="NeighbourTypeDirections"></param> A list of different types with the direction of each of them
/// <param name="num"></param> A random number generator
public static void WaterRulesGuide(HexCell cell, int lowerFreq, HexDirection[] lowerNeighbour, HexDirection lowestNeighbour, int[] typeFreq, float lowestHeight, HexDirection[,] NeighbourTypeDirections, System.Random num)
{
    if (lowestHeight != 9999f)   // Will be 9999 if this cell is the lowest
    {
        if (cell.GetNeighbor(lowestNeighbour).type == HexType.Water)
        {
            cell.GetNeighbor(lowestNeighbour).changeInHealth += cell.health / 3;
            cell.changeInHealth += -cell.health / 3;
        }
        else
        {
            cell.GetNeighbor(lowestNeighbour).ReplaceCell(cell);
        }
    }
    else if (lowerFreq > 0)
    {
        if (cell.GetNeighbor(lowerNeighbour[num.Next(lowerFreq)]).type == HexType.Water)
        {
            cell.GetNeighbor(lowestNeighbour).changeInHealth += cell.health / 3;
            cell.changeInHealth += -cell.health / 3;
        }
        else
        {
            cell.GetNeighbor(lowerNeighbour[num.Next(lowerFreq)]).ReplaceCell(cell);
        }
    }
    else if (typeFreq[(int)HexType.Empty] > 0)
    {
        // find the indicie of the new neighbour
        int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
        // Make the empty cell a plant
        (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).ReplaceCell(cell);
    }
}
*/

}

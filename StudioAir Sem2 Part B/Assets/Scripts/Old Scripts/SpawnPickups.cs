using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;


public class SpawnPickups : MonoBehaviour {


    //[Header("The names of your Pickup Types (in EnumManager)")]
    //public PickupTypes PickupTypes;

    [Header("Drag you pickups here.")]
    public GameObject[] pickups;

    public int maxPickups = 10;
    public int spawnedPickups;

    int spawnedType1Pickups;   // <== Custom counter, used to only spawn a cirtain amount of a pickup type

    public CSVmaps CSVSpawnPickupMap = 0;

    public static List<GameObject> SpawnedPickups = new List<GameObject>();
    public static List<int> SpawnedPickupIndicies = new List<int>();
    List<Vector3> intensityMap = new List<Vector3>();
    List<int> indicie;

    Vector3 location;

	MeshRenderer pickUpRender;

    


	// Use this for initialization
	void Start () 
	{
        intensityMap = CSV.dataMap;
        
        // This will randomise the list so you can limit how many nodes you will generate.
        indicie = Enumerable.Range(0, CSV.dataMap.Count).ToList();
        indicie.Shuffle();
        
        // The For loop will cycle over the shuffled list of the data points within the loaded CSV intesity/data maps.
        for (int i = 0; i < intensityMap.Count; i++)
        {
            if (spawnedPickups >= maxPickups)
            {
                break;
            }
            else
            {
                Vector3 Position = intensityMap[indicie[i]];

                float spawnIntensity = CSV.getIntensity(CSVSpawnPickupMap,indicie[i]);


                ////////////////////////////////////////////////////////////////////////////////
                //
                //                          REF: 1A - Spawn Pickups
                //
                //                       For each pickup to spawn,
                //          1       Set what type it is,
                //          2       Set the height of the pickup
                //          3       Set the colour
                //          4       Set the scale
                //          5       Count the pickups spawned
                //          
                ////////////////////////////////////////////////////////////////////////////////
                if (spawnIntensity > 15)      // High intensity  
                {
                    Position.y = 3;   

                    GameObject tempObj = Instantiate(pickups[0], Position, Quaternion.identity) as GameObject;  

                    tempObj.GetComponent<MeshRenderer>().material.color = new Color(0.6f, 0.4f, 0.2f);           

                    tempObj.transform.localScale = new Vector3(spawnIntensity / 10, spawnIntensity / 10, spawnIntensity / 10);                        

                    tempObj.transform.parent = gameObject.transform;                                          
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;
                    spawnedType1Pickups++;
                }
                else if (spawnIntensity > 5 && spawnIntensity < 12)      // Mid intensity
                {
                    Position.y = 2;    // Make sure the pickup is 3 m high.
                    GameObject tempObj = Instantiate(pickups[1], Position, Quaternion.identity) as GameObject;      

                    tempObj.GetComponent<MeshRenderer>().material.color = Color.grey;                          

                    tempObj.transform.localScale = new Vector3(2f, 2f, 2f);                                 

                    tempObj.transform.parent = gameObject.transform;
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;
                }
                else                                                    // All other points
                {
                    /*
                    Position.y = 2;    // Make sure the pickup is 3 m high.
                    GameObject tempObj = Instantiate(pickups[2], Position, Quaternion.identity) as GameObject;          

                    tempObj.GetComponent<MeshRenderer>().material.color = new Color(0f, (spawnIntensity - 5) / 7, (spawnIntensity - 5) / 7);      

                    tempObj.transform.localScale = new Vector3(1, 1, 1);

                    tempObj.transform.parent = gameObject.transform;
                    SpawnedPickups.Add(tempObj);
                    SpawnedPickupIndicies.Add(indicie[i]);

                    spawnedPickups++;
                    */
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    
}



////////////////////////////////////////////////////////////////////////////////
//                   IGNORE!!!! DO NOT TOUCH BELOW
////////////////////////////////////////////////////////////////////////////////

public static class CustomFunctions
{
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

}

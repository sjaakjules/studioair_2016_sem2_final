﻿using UnityEngine;
using System.Collections;

public class SaveStateManager : MonoBehaviour {

    [Header("Agent history")]
    public bool saveAgentStates = false;
    public static bool _saveAgentStates;
    public float agentSaveStateTime = 0.5f;
    public static float _agentSaveStateTime;

    [Header("Intensity Map History")]
    public bool saveIntensityStates = false;
    public static bool _saveIntensityStates;
    public float intensitySaveStateTime = 0.5f;
    public static float _intensitySaveStateTime;

    [Header("CA History")]
    public bool saveCAStates = false;
    public static bool _saveCAStates;
    public int CASaveFrequency = 20;
    public static int _CASaveFrequency;

    // Use this for initialization
    void Awake () {
        _saveAgentStates = saveAgentStates;
        _agentSaveStateTime = agentSaveStateTime;
        _saveIntensityStates = saveIntensityStates;
        _intensitySaveStateTime = intensitySaveStateTime;
        _saveCAStates = saveCAStates;
        _CASaveFrequency = CASaveFrequency;
    }
	
	// Update is called once per frame
	void Update () {
        
    }
    void OnValidate()
    {
        _saveAgentStates = saveAgentStates;
        _agentSaveStateTime = agentSaveStateTime;
        _saveIntensityStates = saveIntensityStates;
        _intensitySaveStateTime = intensitySaveStateTime;
        _saveCAStates = saveCAStates;
        _CASaveFrequency = CASaveFrequency;
    }
}

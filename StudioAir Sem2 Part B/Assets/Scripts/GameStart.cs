﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameStart : MonoBehaviour {

    public GameObject overlay;
    public Text GameTimer;
    public float EndGameTimer = 600F;
    public GameObject GameOverScreen;
    public Camera TopView;
    public Camera RTSCam;

	// Use this for initialization
	void Start ()
    {

        overlay.SetActive(true);
        RTSCam.enabled = true;
        TopView.enabled = false;
        
	}
	
	// Update is called once per frame
	void Update () {

        EndGameTimer -= Time.deltaTime;

        if (Input.touchCount >= 1)

        {
            Time.timeScale = 1;
            overlay.SetActive(false);
        }

        if (EndGameTimer <= 0 && EndGameTimer > -4)

        {
            GameOverScreen.SetActive(true);
        }

        if (EndGameTimer < -4 && Input.touchCount >= 1)

        {
            GameOverScreen.SetActive(false);
            Time.timeScale = 0;
            TopView.enabled = true;
            RTSCam.enabled = false;

        }

        if (EndGameTimer > 0)
        {
            GameTimer.text = "YOU HAVE " + (int)EndGameTimer + "s LEFT";
        }
        else
            GameTimer.text = "";


    }
}

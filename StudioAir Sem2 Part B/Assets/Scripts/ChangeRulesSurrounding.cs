﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class ChangeRulesSurrounding : MonoBehaviour
{
    public GameObject buff;
    public int sizeOfEffect = 3;
    public float mapValue = 1;
    public float secondsToDraw = 5f;
    public bool isDrawing = false;
    public bool isChanging = false;
    public bool FollowPlayer = false;
    public bool FollowThisGameObject = false;
    public bool MakePainting = false;

    // Use this for initialization
    Renderer rend;
    Collider col;

    void Start()
    {
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider>();
        rend.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (isDrawing)
        {
            int gridIndex = HexCoordinates.iFp(transform.position);
            if (gridIndex >= 0 && gridIndex < CSV.dataMap.Count)
            {
                HexCoordinates hexCoor = HexCoordinates.hFp(transform.position);
                for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                {
                    for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                    {
                        if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                        {
                            if (FollowPlayer && InputManager._playerPosition != null)
                            {
                                HexCoordinates playerHex = HexCoordinates.hFp(InputManager._playerPosition.transform.position);
                                int playerIndex = HexCoordinates.iFh(new HexCoordinates(playerHex.X + dx, playerHex.Z + dz));
                                if (playerIndex >= 0 && playerIndex < CSV.dataMap.Count)
                                {
                                    lock (CSV.specialRulesLock)
                                    {
                                        if (CSV.SpecialRules.ContainsKey(playerIndex))
                                        {
                                            CSV.SpecialRules[playerIndex] = mapValue;
                                        }
                                        else
                                        {
                                            CSV.SpecialRules.Add(playerIndex, mapValue);
                                        }
                                    }
                                }
                            }
                            if (FollowThisGameObject)
                            {
                                int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                {

                                    lock (CSV.specialRulesLock)
                                    {
                                        if (CSV.SpecialRules.ContainsKey(tempIndex))
                                        {
                                            CSV.SpecialRules[tempIndex] = mapValue;
                                        }
                                        else
                                        {
                                            CSV.SpecialRules.Add(tempIndex, mapValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    void TurnOn()
    {
        rend.enabled = false;
        col.enabled = false;
        isDrawing = true;
        isChanging = true;
        InputManager._CanPaint = true;
        InputManager._mapValue = mapValue;
        if (buff != null)
        {
            buff.SetActive(true);
        }

    }

    void TurnOff()
    {
        isDrawing = false;
        isChanging = false;
        rend.enabled = true;
        col.enabled = true;

        InputManager._CanPaint = false;
        if (buff != null)
        {
            buff.SetActive(false);
        }
    }

    public void changeDelay(float seconds)
    {
        if (!isChanging)
        {
            isChanging = true;
            TurnOn();
            if (float.IsNaN(seconds))
            {
                Invoke("TurnOff", secondsToDraw);
            }
            else
            {
                Invoke("TurnOff", seconds);
            }
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CSVmaps { Topo, Elevation, humanactivity, Hydrology, Vegetation, River }

public enum Rules
{
    nothing, SuchRulesMuchCoding, JulianRules
}

public enum MeshRendererStyles
{
    Standard, HealthHeight, SingleMesh, Custom, scaleHealth, julianRenderere, MultiSizeRender
}
public enum CorpType
{
    Empty, Tree,  Blackberry, Fennel, Stinkwort, Hawthorn
	// Old Hextypes // FreshShoots, shrubs, tree, litFire, EstablishedFire, burningEmber, Ash
}



public class PartCRules : MonoBehaviour
{
    // To have variables in the inspector you need two copies of them, one public and one static.
    // Each update you need to update the static variable with the public one, this is beacuse the CA information is in a seperate part of memory.
    [Header("CA settings")]
    public CSVmaps VisuliseCAMap;
    static CSVmaps _VisuliseCAMap;



    public void Update()
    {
        _VisuliseCAMap = VisuliseCAMap;
    }

    
    public static void Info(HexCell cell, List<HexDirection> VegNeighbourDirections, List<HexDirection> EmptyNeighbourDirections, List<HexDirection> FireNeighbourDirections, System.Random num)
    {
        // /////////////////////////////
        //
        //        Useful functions
        //
        // /////////////////////////////

        // /////////////////////////////
        //      Cell Information
        // /////////////////////////////
       
        int health = cell.health;               // This returns the health of the cell
        cell.health = 5;                        // This will change the health of hte cell to 5
        cell.health += 2;                       // This will ADD 2 to the cell's health.

        CorpType type = cell.type;               // The type of the cell
        
        
		cell.changeType(CorpType.Tree, 1);      // To change the type of this cell, in this example "FreshShoots and a starting health of 1
        cell.changeType(cell.type + 1, 1);          // This will change the cell type to the next one in the order or HexTypes

        HexCell neighbour = cell.GetNeighbor(HexDirection.N);   // This will return a link to the cell north of this cell where you can modify it's properties.
        int neighbourHealth = neighbour.health;                 // Then you can get information of your neighbour, such as health.

        (cell.GetNeighbor(HexDirection.N)).changeType(cell.type, 1);     // This will change the neighbour to this cell type.

        cell.changeType((cell.GetNeighbor(HexDirection.N)).type, 1);    // This will change this cell to the neighbour cell type.

        // /////////////////////////////
        //   Grid location Information 
        // /////////////////////////////

        int index = HexCoordinates.iFp(cell.cellPosition);  // The index of the cell within the grid. Will be -1 if the Vector3 is not within the grid.
        // There are many ways to get the index of the grid, you can use rectangular coordinates which range from 0 to their max values given by:
        int maxX = CSV.gridXLength;
        int maxZ = CSV.gridZLength;

        int indexFromRec = HexCoordinates.iFr(0, 5);        // This will return the index of the cell in the first column (x=0) and 5 rows from the bottom (z = 5)

        // You can also get the center positions of the cell in global coordinates,
        Vector3 CellPosition = HexCoordinates.pFi(indexFromRec);


        // /////////////////////////////
        //      Intensity Information 
        // /////////////////////////////

        // To get the intensity map values you can always call the following function.        
       // float intensity = CSV.getIntensity(CSVmaps.Map1, index);    // The intensity value at the cell given by the index for a specific intensity map.

        // To change the intensity map values you can always call the following function.
      //  CSV.addToIntensityValue(1, CSVmaps.Map1, index);            // This will add 1 to the existing intensity value of the specified intensity map at the cell given by the index.

        // If you want to change the value to something specific you can simply subtract the existing value and add your new value,
     //   CSV.addToIntensityValue(1 - (CSV.getIntensity(CSVmaps.Map1, index)), CSVmaps.Map1, index);

        // /////////////////////////////
        //      other Functions
        // /////////////////////////////

        int randomNumber = num.Next(0, 101);    // This will give you a random number between 0 and 100, as the upper number is not included.

        // How to expand to a random empty cell.
        if (EmptyNeighbourDirections.Count > 0)
        {
            (cell.GetNeighbor(EmptyNeighbourDirections[num.Next(EmptyNeighbourDirections.Count)])).changeType(cell.type, 1);
        }
    }



    // /////////////////////////////////////////////////////////////////////////////////////// //
    //
    //                               Setup your new rules
    //
    //               Link your custom rules above with type behaviours that 
    //              have been specified in the inspector of the CAmanager.
    //
    //                              Input descriptions:
    //                                  
    //                              NeighbourDirections
    //          This is a list of the neighbour directions relative to this cell 
    //                  separated into the different hex cell types. 
    //       so NeighbourTypeDirections[(int)HexType.Empty] will return a list of 
    //          HexDirections relative to this cell which are of Empty type.
    //
    //                              NeighbourValues
    // This is a list of intensity values separated into the directions surrounding this cell. 
    // so NeighbourValues[(int)CSVmaps.Map1] will return an array as large as the amount of 
    //              neighbours, 6 and be the intensity values located by the HexDirection.
    //
    // /////////////////////////////////////////////////////////////////////////////////////// //

	public static void applyBehaviour(Rules CellBehaviour, HexCell cell, List<HexDirection>[] NeighbourDirections,List<int>[] NeighbourHealth,List<int>[] LevelNeighbourHealth, List<HexDirection>[] LevelNeighbourPos, List<HexDirection> sizeNeighboursPos, List<int> neighbourSize, List<HexDirection> hiddenNeighbours, System.Random num)
	{
        switch (CellBehaviour)
        {

            case Rules.SuchRulesMuchCoding:
                SuchRulesMuchCoding.TestingRules(cell, NeighbourDirections, NeighbourHealth, LevelNeighbourHealth, LevelNeighbourPos, sizeNeighboursPos, neighbourSize, hiddenNeighbours, num);
                break;
            case Rules.JulianRules:
                JulianRules.TestingRules(cell, NeighbourDirections, NeighbourHealth, LevelNeighbourHealth, LevelNeighbourPos, sizeNeighboursPos,neighbourSize,  hiddenNeighbours, num);
                break;

            /* OLD RULES
                    case Rules.Rule:
                        CustomRules.simpleRules (cell, NeighbourDirections, num);
                        break;
                    case Rules.slowSpread:
                        CustomRules.ChanceHealth (cell, NeighbourDirections [(int)HexType.tree], NeighbourDirections [(int)HexType.litFire], num);
                        break;
                    case Rules.FemkeRules1:
                        CustomRules.ComplexForestFireRules (cell, NeighbourDirections [(int)HexType.tree], NeighbourDirections [(int)HexType.litFire], NeighbourDirections [(int)HexType.EstablishedFire], num);
                        break;

                            */
            default:
                break;
        }
	}

    public static void ApplyCustomMesh(HexCell cell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType, MeshRendererStyles RenderStyle)
    {
        switch (RenderStyle)
        {
            case MeshRendererStyles.Standard:
                NTPRulesHelp.StandardRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.HealthHeight:
                NTPRulesHelp.healthHeightRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.SingleMesh:
                NTPRulesHelp.SingleRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.scaleHealth:
                NTPRulesHelp.ScaleRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.Custom:
			SuchRulesMuchCoding.SingleRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.julianRenderere:
                SuchRulesMuchCoding.SingleRenderer(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            case MeshRendererStyles.MultiSizeRender:
                MultiSizeSystem.MultiSizeRender(cell, gridIndex, meshPerType, MaterialsPerType);
                break;
            default:
                break;
        }
    }
    
}


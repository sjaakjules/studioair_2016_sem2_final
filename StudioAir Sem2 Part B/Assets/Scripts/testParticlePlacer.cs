﻿using UnityEngine;
using System.Collections.Generic;

public class testParticlePlacer : MonoBehaviour 
{
	public bool showDebugMessages;
	public ParticleSystem theParticleSystem;
	public Transform objPrefab;
	public float fDistanceModifier = 1f;
	private Vector3 mainCamPos;
	private ParticleSystem.Particle[] arrParticles;
	private List<Vector3> listObjects = new List<Vector3>();
	private float farDistance = 0f;
	public bool isPositioned;
    public static List<Vector3> CAPos = new List<Vector3>();

    public List<Vector3> LerpPos;
    Vector3[] pos;

    public float size;


	[Range(0,255)]
    public byte r, g, b;



    /*
         USAGE NOTES
          1. In the particle system disable EVERYTHING except for Renderer.
          
         */

    // Use this for initialization
    void Start () 
	{

		isPositioned = true;
        size = 1;

        listObjects = new List<Vector3>(CSV.dataMap);
		LerpPos = new List<Vector3>(CSV.dataMap);

        for (int i = 0; i < CSV.dataMap.Count; i++)
        {
            listObjects[i] = new Vector3(listObjects[i].x, CSV.getIntensity(CSVmaps.Topo, i), listObjects[i].z);
            LerpPos[i] = listObjects[i];

        }

        InvokeRepeating("whispMovement", 2, 2);

    }

	// Update is called once per frame
	void Update () {
		// Keep the position of the main camera
		mainCamPos = Camera.main.transform.position;
	}


	private void LateUpdate()
	{

		if (isPositioned) 
		{
			// This whole block should only fire once when isPositioned is false. Set to true to prevent firing on the next frame
			isPositioned = true;

			// Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
			//  to tell how many particles are needed
			
			//arrParticles = new ParticleSystem.Particle[listObjects.Count];
			arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

			// Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
			//  This gets a generic set of particles to modify and set BACK to the system once completed
			theParticleSystem.GetParticles(arrParticles);

			// Run a loop through the number of particles.. i is our counter value
			for (int i = 0; i < arrParticles.Length; i++) {
				// For ease of use assign a ParticleSystem.particle for the current object
				ParticleSystem.Particle par = arrParticles [i];

                // Now assign the values you wish to change via 'par'.
                //par.position = new Vector3 (listObjects [i].x * fDistanceModifier, listObjects [i].y * fDistanceModifier, listObjects [i].z * fDistanceModifier);

                /*
					if (CSV.getIntensity((CSVmaps)1, i) == 0)
                {
                    par.size = 0;
					//par.position = listObjects[i] = CSV.dataMap[i];
                }

				else
				{
					par.size = size;
					par.position = listObjects[i] = Vector3.Lerp(listObjects[i], LerpPos[i], Time.deltaTime);

				}
                */


                //if (CAManager.CAGrid[i].health > JulianRules._StressDecreaseAmount)

                if (CAManager.CAGrid[i].health > SuchRulesMuchCoding._StressDecreaseAmount)
                {
                    par.size = 0;
					par.position = listObjects[i] = CSV.dataMap[i];
                }

				else
				{
					par.size = size;
					par.position = listObjects[i] = Vector3.Lerp(listObjects[i], LerpPos[i], Time.deltaTime);

				}
			

                par.velocity = Vector3.zero;
				par.remainingLifetime = 900f;

                //par.color = new Color32(20,255,0,255);
				par.color = new Color32(r,g,b,255);

                // Critical step! You MUST assign the modified particle BACK into the current position of the particles
                //  This seems to be how the data is saved to the particle.
                arrParticles [i] = par;

			
			}

			// Apply the particle changes to the particle system
			//  Note there is no need for Emit or Play here. Execute SetParticles and it works.
			theParticleSystem.SetParticles (arrParticles, arrParticles.Length);
		}

	}
	    

	void GenerateObjects() {

        //listObjects should be populated by whatever means you need - DB, array, text file et
        //listObjects = CAPos;
        listObjects = CSV.dataMap;
        int i = 0;

		theParticleSystem.Clear ();
		theParticleSystem.maxParticles = listObjects.Count;

		foreach (Vector3 obj in listObjects) 
		{
			// Get the X Y and Z coordinates for the object
			float x = obj.x * fDistanceModifier;
            float y = obj.y * fDistanceModifier;
			float z = obj.z * fDistanceModifier;

			// Set the position to a vector3 
			Vector3 objPos = new Vector3 (x,y,z);

			if(showDebugMessages)
				Debug.Log("Position of obj: " + objPos.ToString());

			// Create the game objects that sit at the positions in space where objs exist
			//  Set the position and parent the object accordingly
			//clone.name = obj.name;
			Transform clone = (Transform)Instantiate(objPrefab, objPos, Quaternion.identity);

			clone.SetParent(gameObject.transform);

			i++;
		}

		if(showDebugMessages)
			Debug.Log("Generate GenerateObjects has finished.");
	}

    void whispMovement()
	{
        float offset;
        int i = 0;

        foreach (Vector3 cell in LerpPos)
		{
			offset = Random.Range(-100,100);
			if (listObjects[i].y < CSV.dataMap[i].y)
			{
			LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + Mathf.Abs(offset / 100), cell.z + (offset / 100));	
			}
			else
			{
LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + (offset / 100), cell.z + (offset / 100));
			}
			
            i++;
        }  
    }

}
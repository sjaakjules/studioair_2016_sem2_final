﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Sprites;

public class GUItext : MonoBehaviour {

    public Slider treeSlider;
    public Text Geemo;
    public Text kills;
    public Text Chemia;
    public Text Water;
    public Text Land;
    public Text trees;


    // Use this for initialization
    void Start () 
	{
	
	}

    // Update is called once per frame
    void Update()
    {
        Geemo.text =

            CAManager._typeCount[2].ToString();

        Chemia.text =

            CAManager._typeCount[4].ToString();

        Water.text =

            CAManager._typeCount[3].ToString();

        Land.text =

            CAManager._typeCount[5].ToString();


        kills.text =

            "BUILDINGS DESTROYED: " + CAManager._totalKilled;

        trees.text =

            CAManager._typeCount[1].ToString();

        treeSlider.value = CAManager._typeCount[1];
    }
}

//        pop.text =
        
     //       "Trees alive: " + CAManager._typeCount[1].ToString() + "\n" +
       //     "GeeMo: " + CAManager._typeCount[2].ToString() + "\n" +
         //   "Water Corp: " + CAManager._typeCount[3].ToString() + "\n" +
           // "Chemia Industries: " + CAManager._typeCount[4].ToString() + "\n" +
        //    "Land Inc: " + CAManager._typeCount[5].ToString() + "\n" +
         //   "Corp Kills: " + CAManager._totalKilled;
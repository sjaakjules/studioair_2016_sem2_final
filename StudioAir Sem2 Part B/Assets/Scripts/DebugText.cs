﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugText : MonoBehaviour {

    public Text DebugFont;
    public Text Go;
    public float myTimer = 10.0F;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
        }

        if (Time.fixedTime < 10)
        {

            DebugFont.text = "HOLD PHONE STILL FOR " + "\n" + (int)myTimer + " SECONDS";
        }

        else
		DebugFont.text = "";

        if (Time.fixedTime > 10 && Time.fixedTime < 13)
        {
            Go.text = "ALL SET!" + "\n" + "TAP TO PLAY!";
        }

        else
        {
            Go.text = "";
        }

        

	}
}

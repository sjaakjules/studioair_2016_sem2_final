﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NTPRulesHelp : MonoBehaviour
{

    // To have variables in the inspector you need two copies of them, one public and one static.
    // Each update you need to update the static variable with the public one, this is beacuse the CA information is in a seperate part of memory.
    [Header("CA settings")]
    public CSVmaps VisuliseCAMap;
    static CSVmaps _VisuliseCAMap;

    [Header("How resiliant is the forrest, 5 will die out, 10 will spread whole site")]
    public float spreadPercent = 20;
    static float _spreadPercent;

    [Header("How many spot fires pop upm 0.001 is often, 0.00001 1 every few seconds")]
    public float ignitionPercent = 0.0001f;
    static float _ignitionPercent;

    [Header("How fast does the fire spread/ die off, 10 is slow 30 is very fast. (when maxHealth is 50)")]
    public int maxChangeInHealth = 30;
    static int _maxChangeInHealth;

    public int stressincrease = 10;
    static int _stressincrease;

    public float stressSpreadChance = 20;
    static float _stressSpreadChance;

    public int stressDecrease = 20;
    static int _stressDecrease;


    public float[] typeChangePercent = new float[HexCell.nTypes];
    static float[] _typeChangePercent = new float[HexCell.nTypes];
    
    public GameObject[] CustomMeshes = new GameObject[5];
    static Mesh[] _CustomMeshes;
    

    public CorpType minRenderType;
    static CorpType _minRenderType;


    static int tempTypeNumber;
    static MaterialPropertyBlock block;
    static Matrix4x4 voxTransform = Matrix4x4.identity;
    static float tempIntensity;

    public void Start()
    {
        for (int i = 0; i < CustomMeshes.Length; i++)
        {
            if (CustomMeshes[i] != null)
            {
                _CustomMeshes[i] = CustomMeshes[i].GetComponent<MeshFilter>().sharedMesh;
            }
        }
    }

    public void Update()
    {
        _spreadPercent = spreadPercent;
        _VisuliseCAMap = VisuliseCAMap;
        _maxChangeInHealth = maxChangeInHealth;
        _ignitionPercent = ignitionPercent;
        _minRenderType = minRenderType;
        _stressincrease = stressincrease;
        _stressSpreadChance = stressSpreadChance;
        _stressDecrease = stressDecrease;
        for (int i = 0; i < HexCell.nTypes; i++)
        {
            _typeChangePercent[i] = typeChangePercent[i];
        }
    }

    //

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    //
    //
    //                  CA RUles
    //
    //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
   /* public static void ComplexForestFireRules(HexCell cell, List<HexDirection>[] TypeNeighbourDirections, System.Random num)
    {
        tempTypeNumber = (int)cell.type;

        if (TypeNeighbourDirections[(int)CorpType.litFire].Count > 0 &&          // If there is a neighbour on fire
            tempTypeNumber > 0 && tempTypeNumber < (int)CorpType.litFire &&      // If this cell is any type of tree
            num.Next(0, 10000) < (int)(_spreadPercent * 100))                   // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
                cell.changeType(CorpType.litFire, 1);
        }
        else if (tempTypeNumber > 0 && tempTypeNumber < (int)CorpType.litFire &&          // If this cell is any type of tree
                    num.Next(0, 100000000) < (int)(_ignitionPercent * 1000000))                       // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (num.Next(0,10000) < (int)(_typeChangePercent[(int)cell.type] * 100) &&
                    cell.type != CorpType.Tree)
        {
            tempTypeNumber++;
            if (tempTypeNumber >= HexCell.nTypes)
            {
                tempTypeNumber = 0;
            }
            cell.changeType((CorpType)tempTypeNumber, 1);
        }
    }

    public static void WeedBuildingRules(HexCell cell, List<HexDirection>[] TypeNeighbourDirections, System.Random num)
    {
        tempTypeNumber = (int)cell.type;

        if (TypeNeighbourDirections[(int)CorpType.litFire].Count > 0 &&          // If there is a neighbour on fire
            tempTypeNumber > 2 && tempTypeNumber < (int)CorpType.litFire &&      // If this cell is any type of tree
            num.Next(0, 10000) < (int)(_spreadPercent * 100))                   // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (tempTypeNumber > 0 && tempTypeNumber < (int)CorpType.litFire &&          // If this cell is any type of tree
                    num.Next(0, 100000000) < (int)(_ignitionPercent * 1000000))                       // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (num.Next(0, 10000) < (int)(_typeChangePercent[(int)cell.type] * 100) &&    // percentage to change type
                    cell.type < CorpType.Tree)                                               // Is empty / shoot / shrub type
        {
            tempTypeNumber++;
            cell.changeType((CorpType)tempTypeNumber, 1);
        }
        else if (cell.type > CorpType.Tree)
        {
            cell.health += num.Next(0, _maxChangeInHealth);
        }

        if (cell.type > CorpType.Tree &&
                    cell.health >= CAManager._maxHealth)
        {
            tempTypeNumber++;
            if (tempTypeNumber >= HexCell.nTypes)
            {
                tempTypeNumber = 0;
            }
            cell.changeType((CorpType)tempTypeNumber, 1);
        }
    }
	public static void WeedBuildingWithStress(HexCell cell, List<HexDirection>[] TypeNeighbourDirections,List<int>[] neighbourHealth,List<int>[] LevelNeighbourHealth,  System.Random num)
    {
        
        // Veg rules
        if (TypeNeighbourDirections[(int)CorpType.litFire].Count > 0 &&          // If there is a neighbour on fire
            neighbourHealth[(int)CorpType.litFire].Max() > 30 &&
            cell.type > CorpType.shrubs && cell.type < CorpType.litFire &&      // If this cell is any type of tree
            num.Next(0, 10000) < (int)(_spreadPercent * 100))                   // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (cell.type > CorpType.Empty && cell.type < CorpType.litFire &&          // If this cell is any type of tree
                    num.Next(0, 100000000) < (int)(_ignitionPercent * 1000000))                       // Chance to spread the fire, this could be an array depending on the type, ie tree less likely but grass/freshshoots more likely
        {
            cell.changeType(CorpType.litFire, 1);
        }
        else if (num.Next(0, 10000) < (int)(_typeChangePercent[(int)cell.type] * 100) &&    // percentage to change type
                    cell.type < CorpType.Tree)                                               // Is empty / shoot / shrub type
        {
            cell.changeType(cell.type + 1, 1);
        }else if (TypeNeighbourDirections[(int)CorpType.litFire].Count > 0 &&
            cell.type > CorpType.shrubs && cell.type < CorpType.litFire)
        {
            cell.health += _stressincrease;
        }
        // Weed Rules
        else if (cell.type > CorpType.Tree)
        {
            cell.health += num.Next(0, _maxChangeInHealth);
        }

        if (cell.type > CorpType.Tree &&
                    cell.health >= CAManager._maxHealth)
        {
            if ((int)(cell.type + 1) >= HexCell.nTypes)
            {
                cell.changeType(CorpType.Empty, 1);
            }
            else
            {
                cell.changeType(cell.type + 1, 1);
            }
        }
        else if (cell.type == CorpType.Tree &&
            neighbourHealth[(int)CorpType.Tree].Count != 0 &&
                neighbourHealth[(int)CorpType.Tree].Max() > cell.health &&
                num.Next(0, 10000) < (int)(_stressSpreadChance * 100))
        {
            cell.health =(int)( neighbourHealth[(int)CorpType.Tree].Max() / 1.05f);
        }else if (cell.type == CorpType.Tree && cell.health > _stressDecrease)
        {
            cell.health -= _stressDecrease;
        }
        
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    //
    //
    //               MESH RENDERER RULES
    //
    //      Edit them here and they link to the "MeshRendererStyles" selected per type in the inspector.
    //      If you want to add another one the process is similar to rules for the CA, look at the Dynamics Handout pdf on slack.
    //      
    //      NOTE:   _minRenderType is used in all of them to not draw types less than that, if you have a look at the HexType they are in an order.
    //
    //              If you want help have a look in the "PartCRules" script for useful commands, and talk on slack! :)
    //              
    //              MaterialsPerType[(int)CAcell.Type]  is selecting the material in array of materials which is specified in the inspector under the CARenderer Manager
    //                                                  this is a standard practice for things per HexType where you convert the CAcell type to a number.
    //
    //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    */

    public static void Custom(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {
        // This will update the position of the CA, as agent CA cells move around this makes sure there interpolating between cells
        CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);


        // The meshes drawn need a position, scale and rotation. This is the VaxTransform. 
        voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f, 0.8f, 0.8f));


        //
        // This uses a custom mesh list. If you want variables in the inspector to be used here you must make a public/Static variable and update the static with the public in the update or Start methods
        // Read the dynamics handout pdf on slack if that doesnt make sense and post on slack.
        if (CAcell != null && (int)CAcell.type < _CustomMeshes.Length)
        {
            voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f, 0.8f, 0.8f));
            Graphics.DrawMesh(_CustomMeshes[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
        }
    }

    /// <summary>
    /// This will render what ever mesh you have as the meshPerType in the inspector but duplicate it depending on the cells health.
    /// If the cell has 30 Hp it will duplicate it 3 times.
    /// The offset is specified "new Vector3(0, i*1, 0)" where the 1 means 1 unit upwards per duplication. this is when it sets the VoxTransform within the Forloop
    /// </summary>
    /// <param name="CAcell"></param>
    /// <param name="gridIndex"></param>
    /// <param name="meshPerType"></param>
    /// <param name="MaterialsPerType"></param>
    public static void healthHeightRenderer(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {

        if (CAcell != null && (int)CAcell.type > (int)_minRenderType)
        {
            CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);
            
            tempIntensity = CSV.getIntensity(CSVmaps.Topo, gridIndex);

            if (CAcell.type >= CorpType.Tree)
            {
                for (int i = 0; i <= CAcell.health  / 10; i++)
                {
                    voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0) + new Vector3(0, i*1, 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f, 0.8f, 0.8f));
                    Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
                }
            }
            else
            {
                voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(.8f, .8f, .8f) * 1);
                Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
            }
        }
    }

    /// <summary>
    /// This renders one type per CACell as specified in the inspector of the CAManager. The material is seperate and  specified at the same location
    /// </summary>
    /// <param name="CAcell"></param>
    /// <param name="gridIndex"></param>
    /// <param name="meshPerType"></param>
    /// <param name="MaterialsPerType"></param>
    public static void SingleRenderer(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {

        if (CAcell != null && (int)CAcell.type > (int)_minRenderType)
        {
            CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);

            // This is how you select the intensity map that is being rendered, ie the height of the CAcell even when the maps are changed on the fly.
            tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex);

            voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f*3, 0.8f*3, 0.8f*3));
            Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
        }
    }
    public static void ScaleRenderer(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {

        if (CAcell != null && (int)CAcell.type > (int)_minRenderType)
        {
            CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);

            // This is how you select the intensity map that is being rendered, ie the height of the CAcell even when the maps are changed on the fly.
            tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex);

            voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex)/2, 0), Quaternion.Euler(-90, 0, 0), new Vector3(CAcell.health*1.0f /50, CAcell.health * 1.0f / 50, 0.01f)*5f);
            Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
        }
    }

    /// <summary>
    /// This was an old mesh rendereer, you can play around with it, have it there for history
    /// This will change the style of rendereing depending on the intensity map.
    /// </summary>
    /// <param name="CAcell"></param>
    /// <param name="gridIndex"></param>
    /// <param name="meshPerType"></param>
    /// <param name="MaterialsPerType"></param>
    public static void StandardRenderer(HexCell CAcell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {

        if (CAcell != null && (int)CAcell.type > (int)_minRenderType)
        {
            CAcell.currentPosition = Vector3.Lerp(CAcell.currentPosition, CAcell.cellPosition, Time.deltaTime);

            voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(.8f, .8f, .8f) * 3);
            tempIntensity = CSV.getIntensity(CSVmaps.Topo, gridIndex);


            if (CAcell.health > 0)
            {
				if (CAcell.type != CorpType.Tree)
                {
                    Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
                }

                else
                {
                    if (tempIntensity <= 2)
                    {
                        Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, MaterialsPerType[(int)CAcell.type], 0, null, 0, block);
                    }

                    else
                    {
                        voxTransform.SetTRS(CAcell.currentPosition + new Vector3(0, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex), 0), Quaternion.Euler(-90, 0, 0), new Vector3(0.8f, .8f, 0.8f) * 3);
                        Graphics.DrawMesh(meshPerType[0], voxTransform, MaterialsPerType[0], 0, null, 0, block);
                    }
                }

            }
        }
    }

    

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour {
    
    public int sizeOfEffect = 3;
    public float mapValue = 1;
    public static float _mapValue;
    public float threeFingerValue = -1;

    public GameObject playerPosition;
    public static GameObject _playerPosition;

    bool CanPaint;
    public static bool _CanPaint = false;
    public bool alwaysPaint = false;
    // Use this for initialization
    void Start () {
        _playerPosition = playerPosition;
    }
	
	// Update is called once per frame
	void Update () {
        _playerPosition = playerPosition;
        if (alwaysPaint) {
            CanPaint = true;
        }else {
            CanPaint = _CanPaint;
        }
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved|| Input.GetTouch(0).phase == TouchPhase.Stationary)
            {

                // Construct a ray from the current touch coordinates
                Ray inputRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                // Create a particle if hit
                HandleInput(inputRay);
            }
        }
        if (CanPaint  && Input.touchCount == 3)
        {
            int fingersTouching = 0;
            for (int i = 0; i < 3; i++)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began || Input.GetTouch(i).phase == TouchPhase.Moved|| Input.GetTouch(i).phase == TouchPhase.Stationary)
                {
                    fingersTouching++;
                }
            }
            if (fingersTouching == 3)
            {

                Vector2 averagePosition = (Input.GetTouch(0).position + Input.GetTouch(1).position + Input.GetTouch(2).position)/3;
                // Construct a ray from the current touch coordinates
                Ray inputRay = Camera.main.ScreenPointToRay(averagePosition);
                // Create a particle if hit

                RaycastHit hit;

                if (Physics.Raycast(inputRay, out hit))
                {
                    if (hit.transform.tag == "Terrain")
                    {
                        selectedCells(hit.point, threeFingerValue,sizeOfEffect);
                    }
                }
            }
        }
    }



    void HandleInput(Ray inputRay)
    {
        RaycastHit hit;
        ChangeRulesSurrounding collidedObject;

        if (Physics.Raycast(inputRay, out hit))
        {
            if (CanPaint && hit.transform.tag == "Terrain")
            {
                        //selectedCells(hit.point, -5,10);
                selectedCells(hit.point, _mapValue, sizeOfEffect);
            }
            else if (hit.transform.tag == "PowerUp")
            {
                collidedObject = hit.transform.GetComponent<ChangeRulesSurrounding>();
                if (collidedObject != null)
                {
                    if (!collidedObject.isChanging)
                    {
                        collidedObject.changeDelay(float.NaN);
                       // Debug.Log("Collided with an object at : " + hit.point.ToString());
                    }
                }

            }
            // Debug.Log("TagetLocked: " + HexCoordinates.iFp(hit.point).ToString());

        }
    }

    void selectedCells(Vector3 position, float mapValue, int sizeOfEffect)
    {
        int gridIndex = HexCoordinates.iFp(position);
        if (gridIndex >= 0 && gridIndex < CSV.dataMap.Count)
        {
            HexCoordinates hexCoor = HexCoordinates.hFp(position);
            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
            {
                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                {
                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                    {
                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                        {

                            lock (CSV.specialRulesLock)
                            {
                                if (CSV.SpecialRules.ContainsKey(tempIndex))
                                {
                                    CSV.SpecialRules[tempIndex] = mapValue;
                                }
                                else
                                {
                                    CSV.SpecialRules.Add(tempIndex, mapValue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

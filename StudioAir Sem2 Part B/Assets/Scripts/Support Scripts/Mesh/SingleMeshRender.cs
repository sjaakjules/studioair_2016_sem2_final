﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This is used to draw the mesh.
/// An empty mesh is created at wakeup and then using an array of Hex cells drawn using Triangulate.
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SingleMeshRender : MonoBehaviour
{
    Mesh[] hexMesh;
    List<Vector3>[] MeshVertices;

   // List<Vector3> vertices;
    List<int> triangles;
    

    //MeshCollider meshCollider;
    List<Color> colors;

    Vector3[] terrain;
    //HexCell neightbour, nextNeighbour;

    Vector3 cellCenter;

    public meshType MeshType = meshType.Tri;

    public CSVmaps intensityMap;
    public static CSVmaps _intensityMap;
    public Material TerrainMaterial;

    public bool constantUpdate = false;

    private MaterialPropertyBlock block; 
    int nMeshes;

    bool hasLoaded = false;
    int currentMesh = 0;
    int estimatedVerticies = 0;
    int currentCell = 0;

    Vector3 neighbourCentre;
    Vector3 nextNeighbourCentre;
    Vector3 nextNextNeighbourCentre;
    int neighbourIndex;
    int nextNeighbourIndex;
    int nextNextNeightbourIndex;


    public float HexMeshScale = 0.8f;
    float meshCellSize;

    void Start()
    {
        _intensityMap = intensityMap;
        if (MeshType == meshType.Tri)
        {
            estimatedVerticies = 6 * CSV.gridZLength * CSV.gridXLength;
        }
        else
        {
            estimatedVerticies = 42 * CSV.gridZLength * CSV.gridXLength;
        }
         
        nMeshes = Mathf.CeilToInt(1.0f * estimatedVerticies / 65000);

        terrain = CSV.dataMap.ToArray();

        hexMesh = new Mesh[nMeshes];
        MeshVertices = new List<Vector3>[nMeshes];
        //meshCollider = gameObject.AddComponent<MeshCollider>();

        //vertices = new List<Vector3>();
        colors = new List<Color>();
        triangles = new List<int>();

        if (MeshType == meshType.Hex)
        {
            CreateHexGridMesh(terrain);
        }
        else
        {
            CreateTriGridMesh(terrain);
        }

        block = new MaterialPropertyBlock();
        hasLoaded = true;
        currentMesh = 0;
    }

    void Update()
    {

        _intensityMap = intensityMap;

        if (constantUpdate)
        {
            if (MeshType == meshType.Hex)
            {
                UpdateHexGridMesh(terrain);
            }
            else
            {
                UpdateTriGridMesh(terrain);
            }
        }
        for (int i = 0; i < nMeshes; i++)
        {
            Graphics.DrawMesh(hexMesh[i], Matrix4x4.identity, TerrainMaterial, 0, null, 0, block);
        }
    }

    public void CreateHexGridMesh(Vector3[] cells)
    {
        meshCellSize = HexMeshScale * CSV.CellRadius;
        int j = 0;

        for (currentMesh = 0; currentMesh < nMeshes; currentMesh++)
        {
            if (j >= cells.Length)
            {
                break;
            }
            hexMesh[currentMesh] = new Mesh();
            hexMesh[currentMesh].MarkDynamic();
            MeshVertices[currentMesh] = new List<Vector3>();
            colors.Clear();
            triangles.Clear();

            hexMesh[currentMesh].name = "Hex Mesh" + currentMesh.ToString();

            while (j <= ((cells.Length / nMeshes) * (currentMesh + 1)))
            {
                TriangulateHex(cells[j], j);
                j++;
            }
            hexMesh[currentMesh].SetVertices(MeshVertices[currentMesh]);
            hexMesh[currentMesh].colors = colors.ToArray();
            hexMesh[currentMesh].triangles = triangles.ToArray();
            hexMesh[currentMesh].RecalculateNormals();
            //meshCollider.sharedMesh = hexMesh[i];
        }
    }
    public void CreateTriGridMesh(Vector3[] cells)
    {
        meshCellSize = HexMeshScale * CSV.CellRadius;
        int j = 0;

        for (currentMesh = 0; currentMesh < nMeshes; currentMesh++)
        {
            if (j >= cells.Length)
            {
                break;
            }
            hexMesh[currentMesh] = new Mesh();
            hexMesh[currentMesh].MarkDynamic();
            MeshVertices[currentMesh] = new List<Vector3>();
            colors.Clear();
            triangles.Clear();
            hexMesh[currentMesh].name = "Hex Mesh" + currentMesh.ToString();

            while (j < ((cells.Length / nMeshes) * (currentMesh + 1)))
            {
                TriangulateTri(cells[j], j);
                j++;
            }
            hexMesh[currentMesh].SetVertices(MeshVertices[currentMesh]);
            hexMesh[currentMesh].colors = colors.ToArray();
            hexMesh[currentMesh].triangles = triangles.ToArray();
            hexMesh[currentMesh].RecalculateNormals();
            //meshCollider.sharedMesh = hexMesh[i];
        }
    }

    public void UpdateHexGridMesh(Vector3[] cells)
    {
        meshCellSize = HexMeshScale * CSV.CellRadius;
        if (currentMesh < nMeshes)
        {
            refreshHexBatch(cells);
            /*
            int triIndex = 0;
            while (currentCell <= ((cells.Length / nMeshes) * (currentMesh + 1)))
            {
                TriangulateHex(cells[currentCell], currentCell, ref triIndex);
                currentCell++;
            }
            hexMesh[currentMesh].SetVertices(MeshVertices[currentMesh]);
            hexMesh[currentMesh].RecalculateNormals();
            //meshCollider.sharedMesh = hexMesh[i];
            currentMesh++;
            */
        }
        else
        {
            currentCell = 0;
            currentMesh = 0;
        }
    }

    void refreshHexBatch(Vector3[] cells)
    {
        int meshLimit = currentMesh + 3;
       while (currentMesh < meshLimit)
        {
            if (currentMesh >= nMeshes)
            {
                break;
            }
            int triIndex = 0;
            while (currentCell <= ((cells.Length / nMeshes) * (currentMesh + 1)))
            {
                TriangulateHex(cells[currentCell], currentCell, ref triIndex);
                currentCell++;
            }
            hexMesh[currentMesh].SetVertices(MeshVertices[currentMesh]);
            hexMesh[currentMesh].RecalculateNormals();
            //meshCollider.sharedMesh = hexMesh[i];
            currentMesh++;
        }
    }


    public void UpdateTriGridMesh(Vector3[] cells)
    {
        meshCellSize = HexMeshScale * CSV.CellRadius;
        if (currentMesh < nMeshes)
        {
            int triIndex = 0;
            while (currentCell < ((cells.Length / nMeshes) * (currentMesh + 1)))
            {
                TriangulateTri(cells[currentCell], currentCell, ref triIndex);
                currentCell++;
            }
            hexMesh[currentMesh].SetVertices(MeshVertices[currentMesh]);
            hexMesh[currentMesh].RecalculateNormals();
            //meshCollider.sharedMesh = hexMesh[i];
            currentMesh++;
        }
        else
        {
            currentCell = 0;
            currentMesh = 0;
        }
    }
    void TriangulateTri(Vector3 cell, int index, ref int triIndex)
    {
        cellCenter = cell;
        cellCenter.y = CSV.getIntensity(intensityMap, index);


        updateNeightbours(index);


        if ((index / CSV.gridZLength) % 2 == 0 && (index / CSV.gridZLength) < CSV.gridXLength)
        {
            // we are in an even row
            if (index % CSV.gridZLength != 0)
            {
                AddTriangle(
                cellCenter,
                nextNeighbourCentre,
                neighbourCentre, triIndex
                );
                triIndex++;
            }
            if ((index + 1) % CSV.gridZLength != 0)
            {
                AddTriangle(
                cellCenter,
                nextNextNeighbourCentre,
                nextNeighbourCentre, triIndex
                );
                triIndex++;
            }
        }
        else if ((index / CSV.gridZLength) % 2 == 1 && (index / CSV.gridZLength) < CSV.gridXLength && (((index + 1) % CSV.gridZLength) != 0))
        {
            // we are in an odd row
            AddTriangle(
            cellCenter,
            nextNeighbourCentre,
            neighbourCentre, triIndex
            );
            triIndex++;

            AddTriangle(
            cellCenter,
            nextNextNeighbourCentre,
            nextNeighbourCentre, triIndex
            );
            triIndex++;
        }


    }
    void TriangulateTri(Vector3 cell, int index)
    {
        cellCenter = cell;
        cellCenter.y = CSV.getIntensity(intensityMap, index); 


        updateNeightbours(index);


        if ((index / CSV.gridZLength) % 2 == 0 && (index / CSV.gridZLength) < CSV.gridXLength)
        {
            // we are in an even row
            if (index % CSV.gridZLength != 0)
            {
                AddTriangle(
                cellCenter,
                nextNeighbourCentre,
                neighbourCentre
                );
            }
            if ((index + 1) % CSV.gridZLength != 0)
            {
                AddTriangle(
                cellCenter,
                nextNextNeighbourCentre,
                nextNeighbourCentre
                );
            }
        }
        else if ((index / CSV.gridZLength) % 2 == 1 && (index / CSV.gridZLength) < CSV.gridXLength && (((index + 1) % CSV.gridZLength) != 0))
        {
            // we are in an odd row
            AddTriangle(
            cellCenter,
            nextNeighbourCentre,
            neighbourCentre
            );

            AddTriangle(
            cellCenter,
            nextNextNeighbourCentre,
            nextNeighbourCentre
            );
        }


    }

    void updateNeightbours(int index)
    {
        neighbourIndex = index + CSV.gridZLength - 1 + (index / CSV.gridZLength) % 2;
        nextNeighbourIndex = index + CSV.gridZLength + (index / CSV.gridZLength) % 2;
        nextNextNeightbourIndex = index + 1;

        if (neighbourIndex >= 0 && neighbourIndex < CSV.dataMap.Count)
        {
            neighbourCentre = CSV.dataMap[neighbourIndex];
            neighbourCentre.y = CSV.getIntensity(intensityMap, neighbourIndex);
        }

        if (nextNeighbourIndex >= 0 && nextNeighbourIndex < CSV.dataMap.Count)
        {
            nextNeighbourCentre = CSV.dataMap[nextNeighbourIndex];
            nextNeighbourCentre.y = CSV.getIntensity(intensityMap, nextNeighbourIndex);
        }

        if (nextNextNeightbourIndex >= 0 && nextNextNeightbourIndex < CSV.dataMap.Count)
        {
            nextNextNeighbourCentre = CSV.dataMap[nextNextNeightbourIndex];
            nextNextNeighbourCentre.y = CSV.getIntensity(intensityMap, nextNextNeightbourIndex);
        }
    }

    void TriangulateHex(Vector3 cell, int index, ref int triIndex)
    {
        cellCenter = cell;
        cellCenter.y = CSV.getIntensity(intensityMap, index);

        for (int i = 0; i < 6; i++)
        {
            AddTriangle(
                cellCenter,
                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                cellCenter + HexMetrics.corners[i] * meshCellSize, triIndex
            );
            triIndex++;
            if (i < 3)
            {
                neighbourIndex = index;
                nextNeighbourIndex = index;
                if (i == 0)
                {
                    neighbourIndex = index + CSV.gridZLength - 1 + (index / CSV.gridZLength) % 2;
                    nextNeighbourIndex = index + CSV.gridZLength + (index / CSV.gridZLength) % 2;
                }
                else if (i == 1)
                {
                    neighbourIndex = index + CSV.gridZLength + (index / CSV.gridZLength) % 2;
                    nextNeighbourIndex = index + 1;
                }
                else if (i == 2)
                {
                    neighbourIndex = index + 1;
                    nextNeighbourIndex = index + 1;
                }
                if (neighbourIndex > 0 && neighbourIndex < CSV.dataMap.Count)
                {
                    neighbourCentre = CSV.dataMap[neighbourIndex];
                    neighbourCentre.y = CSV.getIntensity(intensityMap, neighbourIndex);

                    nextNeighbourCentre = CSV.dataMap[nextNeighbourIndex];
                    nextNeighbourCentre.y = CSV.getIntensity(intensityMap, nextNeighbourIndex);

                    if ((index / CSV.gridZLength) % 2 == 0 && (index / CSV.gridZLength) < CSV.gridXLength)
                    {
                        // we are in an even row
                        if ((i == 1) || (i == 0 && index % CSV.gridZLength != 0) || (i == 2 && (index + 1) % CSV.gridZLength != 0))
                        {
                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize, triIndex
                            );
                            triIndex++;

                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 4] * meshCellSize, triIndex
                            );
                            triIndex++;
                        }
                        if (i < 2)
                        {
                            // for i == 0 and i == 1,
                            if ((((index + 1) % CSV.gridZLength) != 0 && ((index) % CSV.gridZLength) != 0) || ((index + 1) % CSV.gridZLength == 0 && i == 0) || ((index) % CSV.gridZLength == 0 && i == 1))
                            {
                                AddTriangle(
                                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                                nextNeighbourCentre + HexMetrics.corners[i + 5] * meshCellSize,
                                neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize, triIndex
                                );
                                triIndex++;
                            }
                        }
                    }
                    else if ((index / CSV.gridZLength) % 2 == 1)
                    {
                        // we are in an odd row
                        if ((i == 0) || (i == 1 && (index + 1) % CSV.gridZLength != 0) || (i == 2 && (index + 1) % CSV.gridZLength != 0))
                        {
                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize, triIndex
                            );
                            triIndex++;

                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 4] * meshCellSize, triIndex
                            );
                            triIndex++;
                        }
                        if (i < 2)
                        {
                            // for i == 0 and i == 1,
                            if ((((index + 1) % CSV.gridZLength) != 0))//&& ((index) % CSV.gridXLength) != 0))// || ((index + 1) % CSV.gridXLength != 0 && i == 1) || ((index + 1) % CSV.gridXLength != 0 && i == 0))
                            {
                                AddTriangle(
                                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                                nextNeighbourCentre + HexMetrics.corners[i + 5] * meshCellSize,
                                neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize, triIndex
                                );
                                triIndex++;
                            }
                        }
                    }

                }
            }
        }
    }
    void TriangulateHex(Vector3 cell, int index)
    {
        cellCenter = cell;
        cellCenter.y = CSV.getIntensity(intensityMap, index);

        for (int i = 0; i < 6; i++)
        {
            AddTriangle(
                cellCenter,
                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                cellCenter + HexMetrics.corners[i] * meshCellSize
            );
            if (i < 3)
            {
                neighbourIndex = index;
                nextNeighbourIndex = index;
                if (i == 0)
                {
                    neighbourIndex = index + CSV.gridZLength -1 + (index / CSV.gridZLength) % 2;
                    nextNeighbourIndex = index + CSV.gridZLength + (index / CSV.gridZLength) % 2;
                }
                else if (i == 1)
                {
                    neighbourIndex = index + CSV.gridZLength + (index / CSV.gridZLength) % 2;
                    nextNeighbourIndex = index + 1;
                }
                else if (i == 2)
                {
                    neighbourIndex = index + 1;
                    nextNeighbourIndex = index + 1;
                }
                if (neighbourIndex > 0 && neighbourIndex < CSV.dataMap.Count)
                {
                    neighbourCentre = CSV.dataMap[neighbourIndex];
                    neighbourCentre.y = CSV.getIntensity(intensityMap, neighbourIndex);

                    nextNeighbourCentre = CSV.dataMap[nextNeighbourIndex];
                    nextNeighbourCentre.y = CSV.getIntensity(intensityMap, nextNeighbourIndex);

                    if ((index / CSV.gridZLength) % 2 == 0 && (index / CSV.gridZLength) < CSV.gridXLength)
                    {
                        // we are in an even row
                        if ((i == 1) || (i == 0 && index % CSV.gridZLength != 0) || (i == 2 && (index + 1) % CSV.gridZLength != 0))
                        {
                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize
                            );

                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 4] * meshCellSize
                            );
                        }
                        if (i < 2)
                        {
                            // for i == 0 and i == 1,
                            if ((((index+1) % CSV.gridZLength) != 0 && ((index) % CSV.gridZLength) != 0) || ((index + 1) % CSV.gridZLength == 0 && i == 0) || ((index ) % CSV.gridZLength == 0 && i == 1))
                            {
                                AddTriangle(
                                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                                nextNeighbourCentre + HexMetrics.corners[i + 5] * meshCellSize,
                                neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize
                                );
                            }
                        }
                    }
                    else if ((index / CSV.gridZLength) % 2 == 1)
                    {
                        // we are in an odd row
                        if ((i == 0) || (i == 1 && (index + 1) % CSV.gridZLength != 0) || (i == 2 && (index + 1) % CSV.gridZLength != 0))
                        {
                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize
                            );

                            AddTriangle(
                            cellCenter + HexMetrics.corners[i] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize,
                            neighbourCentre + HexMetrics.corners[i + 4] * meshCellSize
                            );
                        }
                        if (i < 2)
                        {
                            // for i == 0 and i == 1,
                            if ((((index + 1) % CSV.gridZLength) != 0 ))//&& ((index) % CSV.gridXLength) != 0))// || ((index + 1) % CSV.gridXLength != 0 && i == 1) || ((index + 1) % CSV.gridXLength != 0 && i == 0))
                            {
                                AddTriangle(
                                cellCenter + HexMetrics.corners[i + 1] * meshCellSize,
                                nextNeighbourCentre + HexMetrics.corners[i + 5] * meshCellSize,
                                neighbourCentre + HexMetrics.corners[i + 3] * meshCellSize
                                );
                            }
                        }
                    }

                }
            }
        }
    }


    void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = MeshVertices[currentMesh].Count;
        MeshVertices[currentMesh].Add(v1);
        MeshVertices[currentMesh].Add(v2);
        MeshVertices[currentMesh].Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }
    void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3, int index)
    {
        if (hasLoaded)
        {
            MeshVertices[currentMesh][3 * index] = v1;

            MeshVertices[currentMesh][3 * index+1] = v2;

            MeshVertices[currentMesh][3 * index+2] = v3;
        }

    }

    void AddTriangleColor(Color color)
    {
        colors.Add(color);
        colors.Add(color);
        colors.Add(color);
    }
}


public enum meshType { Tri, Hex}

/// <summary>
/// This defines the size of a hexCell. This is used when converting Vector3 positions to Hex / zigzag and indicie coordinates.
/// </summary>
public static class HexMetrics
{
    public const float RadiusChange = 0.866025404f;
    static float outerRadius = 1f;

    static float innerRadius = outerRadius * RadiusChange;

    public static Vector3[] corners = {
        new Vector3( 0.5f * outerRadius, 0f,-innerRadius),
        new Vector3(outerRadius, 0f, 0f),
        new Vector3( 0.5f * outerRadius, 0f,innerRadius),
        new Vector3(-0.5f * outerRadius, 0f,innerRadius),
        new Vector3( -outerRadius,0f, 0f),
        new Vector3( -0.5f * outerRadius, 0f,-innerRadius),
        new Vector3( 0.5f * outerRadius, 0f,-innerRadius)
    };

    public static float cellRadius
    {
        get
        {
            return outerRadius;
        }
        set
        {
            outerRadius = value;
            innerRadius = outerRadius * RadiusChange;
        }
    }

    public static float InnerRadius { get { return innerRadius; } }
    public static float OuterRadius { get { return outerRadius; } }
}



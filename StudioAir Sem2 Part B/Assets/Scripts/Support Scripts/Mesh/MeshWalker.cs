﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshWalker : MonoBehaviour {

    Agent thisAgent;

    TextAsset LegVerticies;
    TextAsset LegTriangles;
    public Material feetMaterial;

    List<Mesh> feetMesh = new List<Mesh>();
    List<int> currentLegIndexs = new List<int>();

    List<int> gridVerticies = new List<int> { 3 };
    Vector3[] Verticies = new Vector3[] { Vector3.forward/2, Vector3.left / 2, Vector3.right / 2, Vector3.zero };
    int[] Triangles = new int[] { 1, 2, 3, 0, 1, 3, 2, 0, 3 };


    List<int> legsToRemove = new List<int>();

    Vector3[] tempVerticies;
    Vector3 tempGridLocation;

    private MaterialPropertyBlock block;

    // Use this for initialization
    void Start ()
    {
        block = new MaterialPropertyBlock();
        thisAgent = GetComponent<Agent>();
        if (thisAgent == null)
        {
            thisAgent = GetComponentInParent<Agent>();
        }
        drawFeet();
    }
	
	// Update is called once per frame
	void Update () {


        drawFeet();
    }

    void drawFeet()
    {
        UpdateMeshLists(thisAgent.gridIndiciesInSight);

        for (int i = 0; i < feetMesh.Count; i++)
        {
            // for each mesh get the verticies and update the body verticies.
            tempVerticies = feetMesh[i].vertices;
            for (int j = 0; j < tempVerticies.Length; j++)
            {
                if (!gridVerticies.Contains(j))
                {
                    // if the verticie is a body vert
                    // set it to be relative to the gameobject position
                    tempVerticies[j] = Verticies[j] + transform.position;
                }
                // Set the updated verts 
                feetMesh[i].vertices = tempVerticies;
            }


            Graphics.DrawMesh(feetMesh[i], Matrix4x4.identity, feetMaterial, 0, null, 0, block);
            // draw mesh
        }

    }

    Mesh createFeetMesh(int gridIndex)
    {
        tempGridLocation = CSV.dataMap[gridIndex];
        tempGridLocation.y = CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex);

        Mesh newFootMesh = new Mesh();
        Vector3[] verticies = (Vector3[])Verticies.Clone();
        // for each vert,
        for (int j = 0; j < verticies.Length; j++)
        {
            if (gridVerticies.Contains(j))
            {
                // if it is grid - set its position relative to grid location
                verticies[j] = Verticies[j] + tempGridLocation;
            }
            else
            {
                // if it is a body (not grid) set it's position relative to gameobject transform
                verticies[j] = Verticies[j] + transform.position;
            }
            // Set the updated verts 
        }

        // set the triangles
        // set the normals
        newFootMesh.vertices = verticies;
        newFootMesh.triangles = Triangles;
        newFootMesh.RecalculateNormals();
        return newFootMesh;
    }

    void UpdateMeshLists(List<int> FeetGridLocations)
    {
        legsToRemove.Clear();
        // count what legs need to be removed
        for (int i = 0; i < currentLegIndexs.Count; i++)
        {
            if (!FeetGridLocations.Contains(currentLegIndexs[i]))
            {
                legsToRemove.Add(i);
            }
        }
        // Add any new leg locations
        for (int i = 0; i < FeetGridLocations.Count; i++)
        {
            if (!currentLegIndexs.Contains(FeetGridLocations[i]))
            {
                currentLegIndexs.Add(FeetGridLocations[i]);
                feetMesh.Add(createFeetMesh(FeetGridLocations[i]));
            }
        }
        legsToRemove.Sort();
        // Remove the legs
        for (int i = 0; i < legsToRemove.Count; i++)
        {
            currentLegIndexs.RemoveAt(legsToRemove[i]-i);
            feetMesh.RemoveAt(legsToRemove[i]-i);
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This is used to draw the mesh.
/// An empty mesh is created at wakeup and then using an array of Hex cells drawn using Triangulate.
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class DynamicMesh : MonoBehaviour
{
    public TextAsset Verticies;
    public TextAsset Triangles;

    public CSVmaps MapToChangeMesh;

    public Material AgentMaterial;

    Mesh[] agentMesh;
    Vector3[][] vertices;
    Vector3[][] Startvertices;
    int[][] triangles;

    Color colors;


    MaterialPropertyBlock block;


    void Start()
    {
        vertices = CSV.readPoints(Verticies);
        triangles = CSV.readTriangles(Triangles);

        agentMesh = new Mesh[vertices.Length];

        Startvertices = new Vector3[vertices.Length][];

        for (int i = 0; i < vertices.Length; i++)
        {
            Startvertices[i] = (Vector3[])vertices[i].Clone();

            for (int j = 0; j < vertices[i].Length; j++)
            {
                Startvertices[i][j] = vertices[i][j];
                vertices[i][j] = Startvertices[i][j] + transform.position;
            }

            agentMesh[i] = new Mesh();
            agentMesh[i].vertices = vertices[i];
            agentMesh[i].triangles = triangles[i];
            agentMesh[i].RecalculateNormals();
        }


        block = new MaterialPropertyBlock();

        Graphics.DrawMesh(agentMesh[0], Matrix4x4.identity, AgentMaterial, 0, null, 0, block);
    }

    void Update()
    {

        for (int i = 0; i < vertices.Length; i++)
        {
            for (int j = 0; j < vertices[i].Length; j++)
            {
                vertices[i][j] = Startvertices[i][j] + transform.position;
            }
        }

        

        if (CSV.getIntensity(MapToChangeMesh, CSV.FindIndex(transform.position)) > 4)
        {
            int meshSelected = 0;
            agentMesh[meshSelected].Clear();
            agentMesh[meshSelected].vertices = vertices[meshSelected];
            agentMesh[meshSelected].triangles = triangles[meshSelected];
            agentMesh[meshSelected].RecalculateBounds();
            agentMesh[meshSelected].RecalculateNormals();
            Graphics.DrawMesh(agentMesh[meshSelected], Matrix4x4.identity, AgentMaterial, 0, null, 0, block);
        }
        else
        {
            int meshSelected = 1;

            agentMesh[meshSelected].Clear();

            agentMesh[meshSelected].vertices = vertices[meshSelected];
            agentMesh[meshSelected].RecalculateBounds();
            agentMesh[meshSelected].triangles = triangles[meshSelected];
            agentMesh[meshSelected].RecalculateNormals();
            Graphics.DrawMesh(agentMesh[meshSelected], Matrix4x4.identity, AgentMaterial, 0, null, 0, block);
        }

    }

}



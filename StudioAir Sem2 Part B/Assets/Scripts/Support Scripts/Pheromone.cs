﻿using UnityEngine;
using System.Collections;

//public enum AgentType { Type1 }

public struct Pheromone {

    // Variables which every pheromone share (because of the static word)
    public static Pheromone[] Grid;

    // Variables of a pheromone
    public float[] strength;

    public Pheromone(float StartingStrength, int pheromoneType)
    {
        strength = new float[AgentManager.nAgentsTypes];
        strength[pheromoneType] = StartingStrength;
    }

    public float totalStrength()
    {
        if (strength != null)
        {
            float strengthOut = 0;
            for (int i = 0; i < strength.Length; i++)
            {
                strengthOut += strength[i];
            }
            return strengthOut;
        }
        return 0;
    }
    

}

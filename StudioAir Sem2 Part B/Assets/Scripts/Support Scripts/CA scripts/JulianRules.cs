﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class JulianRules : MonoBehaviour {

    [Header("Percentage chance for Tree to spawn")]
    [Range(0,100)]
    public float spawnTreePercentage; static float SpawnTreePercentage; static float _SpawnTreePercentage;
    [Header("Stress Perameters")]
    public int stressDecreaseAmount; static int StressDecreaseAmount; public static int _StressDecreaseAmount;
    public int stressIncreaseAmount; static int StressIncreaseAmount; static int _StressIncreaseAmount;
    [Range(0, 100)]
    public float stressIncreasePercentage; static float StressIncreasePercentage; static float _StressIncreasePercentage;
    [Range(0, 100)]
    public float stressSpreadPercentage; static float StressSpreadPercentage; static float _StressSpreadPercentage;
    [Range(0, 1)]
    public float stressSpreadDecay; static float StressSpreadDecay; static float _StressSpreadDecay;
    [Range(100, 199)]
    public int stressMinWeedAge; static int StressMinWeedAge; static int _StressMinWeedAge;
    [Header("Chance for lightnight strike")]
    [Range(0, 0.01f)]
    public float randomWeedGeminationPercentage; static float RandomWeedGeminationPercentage; static float _RandomWeedGeminationPercentage;
    [Header("Weed Propergation Perameters")]
    [Range(100, 199)]
    public int weedGerminationAge; static int WeedGerminationAge; static int _WeedGerminationAge;
    [Range(0, 100)]
    public float weedGerminationPercentage; static float WeedGerminationPercentage; static float _WeedGerminationPercentage;
    [Range(0, 100)]
    public float weedAgingPercentage; static float WeedAgingPercentage; static float _WeedAgingPercentage;
    public int weedAgingAmount; static int WeedAgingAmount; static int _WeedAgingAmount;
    

    float Size1 = 5; static float Scale1;  static float _Scale1;
    float Size2 = 10; static float Scale2; static float _Scale2;
    float Size3 = 15; static float Scale3; static float _Scale3;
    float Size4 = 20; static float Scale4; static float _Scale4;
    static float drawnScale;


    static Matrix4x4 voxTransform = Matrix4x4.identity;
    static MaterialPropertyBlock block;
    static int tempGridIndex;
    static float tempHealth;
    static int level;
    static List<int> SpawnableWeeds = new List<int>(4);

    static int maxNumber;
    static int indexOfMax;
    static HexDirection newCellType;
    static CorpType newType;
    static CorpType newWeedType;
    static CorpType neighbourWeedType;


    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        _SpawnTreePercentage = spawnTreePercentage;
        _StressDecreaseAmount = stressDecreaseAmount;
        _StressIncreaseAmount = stressIncreaseAmount;
        _StressIncreasePercentage = stressIncreasePercentage;
        _StressSpreadPercentage = stressSpreadPercentage;
        _StressMinWeedAge = stressMinWeedAge;
        _RandomWeedGeminationPercentage = randomWeedGeminationPercentage;
        _WeedGerminationAge = weedGerminationAge;
        _WeedGerminationPercentage = weedGerminationPercentage;
        _WeedAgingPercentage = weedAgingPercentage;
        _WeedAgingAmount = weedAgingAmount;

        _Scale1 = Size1;
        _Scale2 = Size2;
        _Scale3 = Size3;
        _Scale4 = Size4;

    }

    public static void TestingRules(HexCell cell, List<HexDirection>[] Neighbours, List<int>[] Neighbourhealth, List<int>[] LevelNeighbourHealth, List<HexDirection>[] LevelNeighbourPos, List<HexDirection> sizeNeighboursPos, List<int> neighbourSize, List<HexDirection> hiddenNeighbours, System.Random num)
    {
        tempGridIndex = HexCoordinates.iFh(cell.coordinates);
        //tempIntensity = CSV.getIntensity (SingleMeshRender._intensityMap, tempGridIndex);
        float elevation = CSV.getIntensity(CSVmaps.Topo, tempGridIndex);
        float hydro = CSV.getIntensity(CSVmaps.Elevation, tempGridIndex);

        // Use a tempory variable which will vary when this cell is different 
        // cororations and at a location of different intensity values.
        SpawnTreePercentage = _SpawnTreePercentage;
        StressDecreaseAmount = _StressDecreaseAmount;
        StressIncreaseAmount = _StressIncreaseAmount;
        StressIncreasePercentage = _StressIncreasePercentage;
        StressSpreadPercentage = _StressSpreadPercentage;
        StressSpreadDecay = _StressSpreadDecay;
        StressMinWeedAge = _StressMinWeedAge;
        WeedGerminationAge = _WeedGerminationAge;
        WeedGerminationPercentage = _WeedGerminationPercentage;
        RandomWeedGeminationPercentage = _RandomWeedGeminationPercentage;
        WeedAgingPercentage = _WeedAgingPercentage;
        WeedAgingAmount = _WeedAgingAmount;

        if (cell.type == CorpType.Tree && Neighbourhealth[HexCell.nTypes].Count > 0)
        {
            ///////////////////////
            //      This is where you can change this to be for only levle1 weeds, so the max will not be Neighbourhealth but be LevelNeighbourHealth
            ////////////////////
            // This will get the oldest weed of allthe weeds then find the index and then the hexPositon (N/S/E/W) so the type can be known
            maxNumber = Neighbourhealth[HexCell.nTypes].Max();
            indexOfMax = Neighbourhealth[HexCell.nTypes].FindIndex(x => x == maxNumber);
            newCellType = Neighbours[HexCell.nTypes][indexOfMax];
            neighbourWeedType = cell.GetNeighbor(newCellType).type;
            if (CorpType.Blackberry == neighbourWeedType)
            {
                StressIncreasePercentage = 2;

            }
            if (CorpType.Fennel == neighbourWeedType)
            {
                StressIncreasePercentage = 14;

            }
            if (CorpType.Stinkwort == neighbourWeedType)
            {
                StressIncreasePercentage = 8;

            }
            if (CorpType.Hawthorn == neighbourWeedType)
            {
                StressIncreasePercentage = 14;      // Neighbourhealth[HexCell.nTypes].Max()
            }
        }

        if (cell.type == CorpType.Tree && LevelNeighbourHealth[1].Count > 0)
        {
            // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
            maxNumber = LevelNeighbourHealth[1].Max();
            indexOfMax = LevelNeighbourHealth[1].FindIndex(x => x == maxNumber);
            newCellType = LevelNeighbourPos[1][indexOfMax];
            newWeedType = cell.GetNeighbor(newCellType).type;
            if (CorpType.Blackberry == newWeedType)
            {
                WeedGerminationPercentage = 5;
                //WeedGerminationAge
            }

            if (CorpType.Fennel == newWeedType)
            {
                WeedGerminationPercentage = 1;
                //WeedGerminationAge
            }

            if (CorpType.Stinkwort == newWeedType)
            {
                WeedGerminationPercentage = 4;
                //WeedGerminationAge
            }

            if (CorpType.Hawthorn == newWeedType)
            {
                WeedGerminationPercentage = 3;      // LevelNeighbourHealth[1].Max()
                //WeedGerminationAge
            }
        }

        // This can not be specific to each weed as it applies to a tree when there are no weed neighbours.
        StressSpreadPercentage = 15;


        if (CorpType.Blackberry == cell.type)
        {
            WeedAgingAmount = 7;
            WeedAgingPercentage = 100;
          //  StressSpreadPercentage = 10;
        }
        if (CorpType.Fennel == cell.type)
        {
            WeedAgingAmount = 2;
            WeedAgingPercentage = 30;
          //  StressSpreadPercentage = 20;
        }
        if (CorpType.Stinkwort == cell.type)
        {
            WeedAgingAmount = 6;
            WeedAgingPercentage = 50;
         //   StressSpreadPercentage = 15;
        }
        if (CorpType.Hawthorn == cell.type)
        {
            WeedAgingAmount = 2;
            WeedAgingPercentage = 70;
          //  StressSpreadPercentage = 20;        // unknown.....
        }

        //This will spawn trees on empty cells
        if (cell.type == CorpType.Empty)
        {
            if (num.Next(0, 10000) < (int)(SpawnTreePercentage * 100))
            {
                cell.changeType(CorpType.Tree, 1);
            }
        }
        // If your tree
        else if (cell.type == CorpType.Tree)
        {
            // Special rules which trump all other rules
            // For example, will spread weed if any weed is surrounding it. will become oldest of the surrounding weeds
            if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 1 && Neighbours[6].Count > 0)
            {
                // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
                maxNumber = Neighbourhealth[6].Max();
                indexOfMax = Neighbourhealth[6].FindIndex(x => x == maxNumber);
                newCellType = Neighbours[6][indexOfMax];
                newType = cell.GetNeighbor(newCellType).type;

                cell.changeType(newType, 100);
            }
            //this will lit trees on fire by its fire neighbours
            else if (LevelNeighbourHealth[1].Count > 0 && LevelNeighbourHealth[1].Max() > WeedGerminationAge && (num.Next(0, 10000) < (int)(WeedGerminationPercentage * 100)))
            {
                /////////////////////////
                // add according to intenity values
                /////////////////////////
                cell.changeType(newWeedType, 100);
            }
            //this will randomly lit trees on fire
            else if (num.Next(0, 100000000) < (int)(RandomWeedGeminationPercentage * 1000000))
            {
                SpawnableWeeds.Clear();
                for (int i = 0; i < 4; i++)
                {
                    if (CAManager._typeCount[2 + i] < 10)
                    {
                        SpawnableWeeds.Add(2 + i);
                        CAManager._typeCount[2 + i] += 10;
                    }
                }
                if (SpawnableWeeds.Count > 0)
                {
                    CorpType newWeedType = (CorpType)SpawnableWeeds[num.Next(0, SpawnableWeeds.Count)];
                    // if you are within an intensity map of blah, spawn, otherwise dont....
                    cell.changeType(newWeedType, 100);
                }
            }

            //this increases the stress of a tree with a fire neighbour
            else if (cell.health < 99 && Neighbourhealth[HexCell.nTypes].Count > 0 && Neighbourhealth[HexCell.nTypes].Max() > StressMinWeedAge && Neighbourhealth[HexCell.nTypes].Min() < 200 && (num.Next(0, 10000) < (int)(StressIncreasePercentage * 100)))
            {
                // the StressIncreaseAmount can change depending on neighbour type, this may help influence the spread...
                if (cell.health + StressIncreaseAmount >= 99)
                {
                    cell.health = 99;
                }
                else
                {
                    cell.health += StressIncreaseAmount;
                }
            }

            //this will create stress amongst stressed tree neighbours
            if (cell.type == CorpType.Tree && Neighbourhealth[(int)CorpType.Tree].Count > 0 && Neighbourhealth[(int)CorpType.Tree].Max() > cell.health && (num.Next(0, 10000) < (int)(StressSpreadPercentage * 100)))
            {
                cell.health = (int)(1.0f * Neighbourhealth[(int)CorpType.Tree].Max());
            }
            if (cell.type == CorpType.Tree && cell.health > StressDecreaseAmount)
            {
                cell.health -= num.Next(0, StressDecreaseAmount);
            }

        }


        // If your a weed type

        else if (cell.type > CorpType.Tree)
        {
            if (num.Next(0, 10000) < (int)(WeedAgingPercentage * 100))
            {
                cell.health += (num.Next(0, WeedAgingAmount));
            }


            // Appy rules to weeds where the parameters have been modified according to the intensity map and weed type
            if (cell.health >= 500)
            {
                if (cell.size != 4)
                {
                    cell.changeType(CorpType.Empty, 0);
                    cell.NewSize = 0;
                }
                else
                {
                    cell.health = 499;
                }
            }
        }

        MultiSizeSystem.MakeLarger(cell, tempGridIndex, Neighbours, hiddenNeighbours, num);


    }


    public static void SingleRenderer(HexCell cell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {
        if (cell != null && cell.health >= 0 && !cell.isHidden)
        {
            cell.currentPosition = Vector3.Lerp(cell.currentPosition, cell.cellPosition, Time.deltaTime);
            level = cell.health / 100;
            tempHealth =(1.0f * (cell.health - (100 * level) + 100) / 100) * level;      // Scale health from Total 0 to 500 to relative 0.5 to 1 for each type
            Scale1 = _Scale1;
            Scale2 = _Scale2;
            Scale3 = _Scale3;
            Scale4 = _Scale4;

            // Check if this is a large cell, ie the size, 0 is normal, size=1 is 7up and so on. Will only auto make a size 1,
            if (cell.size == 0)
            {
                drawnScale = 1;
            }
            else if (cell.size == 1)
            {
                drawnScale = Scale1;
            }
            else if (cell.size == 2)
            {
                drawnScale = Scale2;
            }
            else if (cell.size == 3)
            {
                drawnScale = Scale3;
            }
            else if (cell.size == 4)
            {
                drawnScale = Scale4;
            }

            if (cell.type == CorpType.Tree)
            {
                if (cell.health == 0)
                {
                    voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) + 2, cell.currentPosition.z), Quaternion.Euler(-90, 0, 0), new Vector3(1, 1, 1)*0.2f );
                    Graphics.DrawMesh(meshPerType[3], voxTransform, MaterialsPerType[3], 0, null, 0, block);
                }
                else if (cell.health > _StressDecreaseAmount)
                {
                    voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) + 2, cell.currentPosition.z), Quaternion.Euler(-90, 0, 0), new Vector3(1, 1, 1 + 3f*(1.0f * cell.health) / 100f) );
                    Graphics.DrawMesh(meshPerType[0], voxTransform, MaterialsPerType[0], 0, null, 0, block);

                }
            }
            else if (cell.type > CorpType.Tree)
            {
                voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex)+2, cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, tempHealth, 1) * drawnScale);
                Graphics.DrawMesh(meshPerType[cell.health / 100 ], voxTransform, MaterialsPerType[cell.health / 100], 0, null, 0, block);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{

    public class CAThread : GenericWorker
    {
        HexCell[] CAgrid;
        Vector3 playerPosition;
        List<HexCell>[] TypeCells = new List<HexCell>[HexCell.nTypes];
        Rules typeBehaviours;

        // //////////////////////////////////////////////////////////////
        //              Plant Bacterial variables
        // //////////////////////////////////////////////////////////////
        int maxHealth = 50;

        // Used for random numbers within the background thread.
        System.Random num;

        //int[] typeFreq = new int[HexCell.nTypes];
        //HexDirection[] lowerNeighbour = new HexDirection[6];
        List<HexDirection>[] TypeNeighbourDirections = new List<HexDirection>[HexCell.nTypes + 1];
		List<int>[] TypeNeighbourHealth = new List<int>[HexCell.nTypes + 1];
		List<int>[] LevelNeighbourHealth = new List<int>[HexCell.nTypes + 1];
        List<HexDirection>[] LevelNeighbourPos = new List<HexDirection>[HexCell.nTypes + 1];
        List<HexDirection> hiddenNeighbours = new List<HexDirection>(6);
        List<HexDirection> sizeNeighboursPos = new List<HexDirection>(6);
        List<int> neighbourSize = new List<int>(6);
        //int lowerFreq;
        //HexDirection lowestNeighbour = HexDirection.N;
        //float[][] NeighbourTypeIntensityValues = new float[CSV.nData][];


        // These are buffer values so they can be updated while the CA is calculated.
        // Think of it like the public variables to store static variables with unity GUI...
        public int New_maxHealth { private get; set; }
        public Vector3 New_playerPosition { private get; set; }
        public Rules New_typeRules { private get; set; }





        // Constructor, used to load the default values.
        public CAThread(int _maxHealth, Rules _typeBehaviours)
        {
            for (int i = 0; i < HexCell.nTypes+1; i++)
            {
                TypeNeighbourDirections[i] = new List<HexDirection>(6);
                TypeNeighbourHealth[i] = new List<int>(6);
                LevelNeighbourHealth[i] = new List<int>(6);
                LevelNeighbourPos[i] = new List<HexDirection>(6);
            }
			for (int i = 0; i < HexCell.nTypes; i++)
			{
				TypeCells[i] = new List<HexCell>(CSV.gridXLength * CSV.gridZLength);

            }
            maxHealth = _maxHealth;
            typeBehaviours = _typeBehaviours;

            // Buffer values to update dynamically
            New_maxHealth = _maxHealth;
            New_typeRules = _typeBehaviours;

        }

        // This loads the CA which could allow for multiple CA calculations... 
        // but we wont explore this thought any further...
        public bool loadCA()
        {
            if (!this.running)
            {
                CAgrid = CAManager.CAGrid;
                return true;
            }
            return false;
        }

        // This starts the loop of a CA calculation, it loads the new values before starting and will only restart once last one ahs finished.
        public new void Start()
        {
            updateCAinfo();
            base.Start();
        }

        public void updateCAinfo()
        {
            maxHealth = New_maxHealth;
            playerPosition = New_playerPosition;
            typeBehaviours = New_typeRules;
        }

        // This is the main section which is called when base.Start() is called.
        public override void run(BackgroundWorker worker)
        {
            AgentMovement();

        }



        public void AgentMovement()
        {

            num = new System.Random(System.DateTime.Now.Second * System.DateTime.Now.Millisecond);

            HexCell neighbour, cell;
            int index;

            //Veg particle cell positions
            //testParticlePlacer.CAPos.Clear();

            //float lowestneighbour;


            for (int i = 0; i < HexCell.nTypes; i++)
            {
                CAManager._typeCount[i] = 10*TypeCells[i].Count()/10;
                TypeCells[i].Clear();
                CAManager._TypeScore[i] = 0;
            }
            

            //check entire grid
            foreach (int z in CAManager.createRandomList(CSV.gridZLength))
            {
                foreach (int x in CAManager.createRandomList(CSV.gridXLength))
                {
                    index = HexCoordinates.iFr(x, z);
                    cell = CAgrid[index];
                    hiddenNeighbours.Clear();
                    sizeNeighboursPos.Clear();
                    neighbourSize.Clear();


                    /*if (cell.type == CorpType.Tree)
                    {
                        testParticlePlacer.CAPos.Add(cell.cellPosition);
                    }
                    */


                    TypeCells[(int)cell.type].Add(cell);
                    CAManager._TypeScore[(int)cell.type] += cell.health;
                    //CAManager._typeCount[(int)cell.type] += 1;

                    if (cell.largeIndex != index && !cell.isHidden)
                    {
                        cell.isHidden = true;
                    }

                    if (!cell.hasUpdated)
                    {
						// Reset counting variables.
						for (int i = 0; i < HexCell.nTypes + 1; i++)
                        {
                            TypeNeighbourDirections[i].Clear();
                            TypeNeighbourHealth[i].Clear();
							LevelNeighbourHealth[i].Clear ();
                            LevelNeighbourPos[i].Clear();
                        }
                        
                        foreach (HexDirection neightborDirection in System.Enum.GetValues(typeof(HexDirection)))
                        {
                            neighbour = cell.GetNeighbor(neightborDirection);
                            if (neighbour != null)
                            {
                                TypeNeighbourDirections[(int)neighbour.type].Add(neightborDirection);
                                TypeNeighbourHealth[(int)neighbour.type].Add(neighbour.health);
								if ( neighbour.type > CorpType.Tree) {
									TypeNeighbourHealth[HexCell.nTypes].Add(neighbour.health);
                                    TypeNeighbourDirections[HexCell.nTypes].Add(neightborDirection);

                                }
								LevelNeighbourHealth [neighbour.health / 100].Add (neighbour.health);
                                LevelNeighbourPos[neighbour.health / 100].Add(neightborDirection);
                                if (neighbour.isHidden)
                                {
                                    hiddenNeighbours.Add(neightborDirection);
                                }
                                sizeNeighboursPos.Add(neightborDirection);
                                neighbourSize.Add(neighbour.size);
                            }
                        }

						PartCRules.applyBehaviour(typeBehaviours, cell, TypeNeighbourDirections, TypeNeighbourHealth,LevelNeighbourHealth, LevelNeighbourPos, sizeNeighboursPos, neighbourSize,  hiddenNeighbours, num);
                        updateHiddenCells(cell,index, hiddenNeighbours, TypeNeighbourDirections[(int)cell.type]);

                        if (cell.health < 0)
                        {
                            cell.changeType(CorpType.Empty, 0);
                        }
                        else if (cell.health > maxHealth)
                        {
                            //cell.health = maxHealth;
                        }
                    }

                    cell.hasUpdated = false;
                }
            }
            lock (CSV.specialRulesLock)
            {
                CSV.clearSpecialRules = true;
                //CSV.SpecialRules.Clear();
            }

        }

        void updateHiddenCells(HexCell cell,int gridIndex, List<HexDirection> hiddenNeighbours, List<HexDirection> sameTypeNeighbours)
        {

            if (cell.NewSize != -1)
            {
                if (cell.NewSize > cell.size)
                {
                    for (int dx = -cell.NewSize; dx <= cell.NewSize; dx++)
                    {
                        for (int dz = -cell.NewSize; dz <= cell.NewSize; dz++)
                        {
                            if (Mathf.Abs(dx + dz) <= cell.NewSize && Mathf.Abs(dx) + Mathf.Abs(dz) != 0)
                            {
                                int tempIndex = HexCoordinates.iFh(new HexCoordinates(cell.coordinates.X + dx, cell.coordinates.Z + dz));
                                if (tempIndex >= 0 && tempIndex < CAgrid.Length)
                                {
                                    if (CAgrid[tempIndex].isHidden)
                                    {
                                        CAgrid[CAgrid[tempIndex].largeIndex].NewSize = 0;
                                        CAManager.CellSizes[CAgrid[CAgrid[tempIndex].largeIndex].size].Remove(tempIndex);
                                    }
                                    CAgrid[tempIndex].isHidden = true;
                                    CAgrid[tempIndex].largeIndex = gridIndex;
                                }
                            }
                        }
                    }
                }
                else if (cell.NewSize < cell.size)
                {
                    for (int dx = -cell.size; dx <= cell.size; dx++)
                    {
                        for (int dz = -cell.size; dz <= cell.size; dz++)
                        {
                            int tempIndex = HexCoordinates.iFh(new HexCoordinates(cell.coordinates.X + dx, cell.coordinates.Z + dz));
                            if (tempIndex >= 0 && tempIndex < CAgrid.Length && Mathf.Abs(dx) + Mathf.Abs(dz) != 0)
                            {
                                if (Mathf.Abs(dx + dz) <= cell.size && !(Mathf.Abs(dx + dz) <= cell.NewSize && Mathf.Abs(dx) <= cell.NewSize && Mathf.Abs(dz) <= cell.NewSize) )
                                {
                                    // This is within old size but not new size
                                    if (CAgrid[tempIndex].largeIndex == gridIndex)
                                    {
                                        CAgrid[tempIndex].isHidden = false;
                                        CAgrid[tempIndex].largeIndex = tempIndex;
                                    }
                                }
                                else if (Mathf.Abs(dx + dz) <= cell.NewSize && Mathf.Abs(dx) <= cell.NewSize && Mathf.Abs(dz) <= cell.NewSize)
                                {
                                    // This is within the new cell size
                                    if (CAgrid[tempIndex].largeIndex != gridIndex && (CAgrid[CAgrid[tempIndex].largeIndex].size < cell.NewSize || CAgrid[CAgrid[tempIndex].largeIndex].NewSize < cell.NewSize) )
                                    {
                                        CAgrid[CAgrid[tempIndex].largeIndex].NewSize = 0;
                                        CAManager.CellSizes[CAgrid[CAgrid[tempIndex].largeIndex].size].Remove(tempIndex);
                                    }
                                    CAgrid[tempIndex].isHidden = true;
                                    CAgrid[tempIndex].largeIndex = gridIndex;
                                }
                            }
                        }
                    }
                }
                if (cell.size > 0)
                {
                    CAManager.CellSizes[cell.size].Remove(HexCoordinates.iFh(cell.coordinates));
                }
                cell.size = cell.NewSize;
                cell.NewSize = -1;
                cell.isHidden = false;
            }
        }
    }
}
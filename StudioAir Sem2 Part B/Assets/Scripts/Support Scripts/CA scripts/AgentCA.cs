﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class AgentCA : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void MoveBetween(HexCell cell, System.Random num, float[] NeighbourAttributeValues)
    {
        float minDistance = NeighbourAttributeValues.Min(n => Mathf.Abs(CAManager._targetValue - n));
        if (minDistance < CAManager._valueRange)
        {
            int directionIndex = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.First(n => Mathf.Abs(CAManager._targetValue - n) == minDistance));
            cell.Direction = (HexDirection)directionIndex;
        }

        // Make sure your not near the edge of the map
        while (cell.GetNeighbor(cell.Direction) == null)
        {
            cell.Direction = (HexDirection)num.Next(0, 6);
        }


        // Move if there is nothing in front of you, otherwise join if same cell
        if (cell.GetNeighbor(cell.Direction).type == CorpType.Empty)
        {
            (cell.GetNeighbor(cell.Direction)).MoveToCell(cell);
        }
        else if (cell.GetNeighbor(cell.Direction).type == cell.type)
        {
            cell.GetNeighbor(cell.Direction).AddtoCell(cell);
        }
    }

    public static void MoveAway(HexCell cell, System.Random num, float[] NeighbourAttributeValues)
    {
        int minDirection = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.Min());
        cell.Direction = (HexDirection)minDirection;

        while (cell.GetNeighbor(cell.Direction) == null)
        {
            cell.Direction = (HexDirection)num.Next(0, 6);
        }

        if (cell.GetNeighbor(cell.Direction).type == CorpType.Empty)
        {
            (cell.GetNeighbor(cell.Direction)).MoveToCell(cell);
        }
        else if (cell.GetNeighbor(cell.Direction).type == cell.type)
        {
            cell.GetNeighbor(cell.Direction).AddtoCell(cell);
        }
    }

    public static void MoveTowards(HexCell cell, System.Random num, float[] NeighbourAttributeValues)
    {
        int maxDirection = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.Max());
        cell.Direction = (HexDirection)maxDirection;

        while (cell.GetNeighbor(cell.Direction) == null)
        {
            cell.Direction = (HexDirection)num.Next(0, 6);
        }

        if (cell.GetNeighbor(cell.Direction).type == CorpType.Empty)
        {
            (cell.GetNeighbor(cell.Direction)).MoveToCell(cell);
        }
        else if (cell.GetNeighbor(cell.Direction).type == cell.type)
        {
            cell.GetNeighbor(cell.Direction).AddtoCell(cell);
        }
    }



    public static void MoveDownHill(HexCell cell, System.Random num, int lowerNeighbours, HexDirection lowestNeighbour, List<HexDirection>[] NeighbourTypeDirections)
    {
        if (lowerNeighbours > 0) 
        {
            if (cell.GetNeighbor(lowestNeighbour).type == CorpType.Empty)
            {
                cell.GetNeighbor(lowestNeighbour).MoveToCell(cell);
            }
            else if (cell.GetNeighbor(lowestNeighbour).type == cell.type)
            {
                cell.GetNeighbor(lowestNeighbour).AddtoCell(cell);
            }
        }
        else if (NeighbourTypeDirections[(int)CorpType.Empty].Count > 0)
        {
            MoveRandomly(cell, NeighbourTypeDirections, num);
        }

    }

    /// <summary>
    /// Will move in the same direction if it can. if not will popint left or right and then next loop move in that direction. if not will repeat until it can move.
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="num"></param>
    public static void ChangeDirectionRandomly(HexCell cell, System.Random num)
    {

        // Change direction if edge of map
        while (cell.GetNeighbor(cell.Direction) == null)
        {
            cell.Direction = (HexDirection)num.Next(0, 6);
        }

        if (cell.GetNeighbor(cell.Direction) != null && cell.GetNeighbor(cell.Direction).type == CorpType.Empty)
        {
            (cell.GetNeighbor(cell.Direction)).MoveToCell(cell);
        }
        else if (cell.GetNeighbor(cell.Direction).type == cell.type)
        {
            cell.GetNeighbor(cell.Direction).AddtoCell(cell);
        }
        else
        {
            int randomDirection = num.Next(1);
            cell.Direction += randomDirection == 0 ? -1 : randomDirection;
        }

    }



    /// <summary>
    /// If there are empty cells surrounding, move randomly
    /// </summary>
    /// <param name="cell"></param> This is the cell which is being modified
    /// <param name="typeFreq"></param> The types of the neighbours, either empty of water
    /// <param name="NeighbourTypeDirections"></param> A list of different types with the direction of each of them
    /// <param name="num"></param> A random number generator
    public static void MoveRandomly(HexCell cell, List<HexDirection>[] NeighbourTypeDirections, System.Random num)
    {

        // Change direction if edge of map
        while (cell.GetNeighbor(cell.Direction) == null)
        {
            cell.Direction = (HexDirection)num.Next(0, 6);
        }
        int nEmptyNeighbours = NeighbourTypeDirections[(int)CorpType.Empty].Count;
        if (nEmptyNeighbours > 0)
        {
            int newIndex = num.Next(nEmptyNeighbours);                                                      // Find a random neighbour, ie Select a number between 0 and the number of surrounding empty cells

            cell.Direction = (NeighbourTypeDirections[(int)CorpType.Empty])[newIndex];         // Find the direction of a random empty cell

            (cell.GetNeighbor(cell.Direction)).MoveToCell(cell);                                        // Make the empty cell a plant
        }
        else
        { 
            int newIndex = num.Next(6);                                                                     // Find a random neighbour, ie Select a number between 0 and the number of surrounding empty cells

            cell.Direction =  (HexDirection)newIndex;
            while (cell.GetNeighbor(cell.Direction) == null)
            {
                cell.Direction = (HexDirection)num.Next(0, 6);
            }
            // Find the direction of a random empty cell
            if (cell.GetNeighbor(cell.Direction).type == cell.type)
            {
                cell.GetNeighbor(cell.Direction).AddtoCell(cell);
            }
        }
    }


}

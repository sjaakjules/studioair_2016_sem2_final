﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum HexDirection { SE, NE, N, NW, SW, S }

public static class enumExtensions
{
    public static HexDirection Opposite(this HexDirection direction)
    {
        return (HexDirection)(((int)direction + 3) > 6 ? ((int)direction - 3) : ((int)direction + 3));

    }
    public static int[] indexArray
    {
        get
        {
            int[] output = new int[System.Enum.GetNames(typeof(HexDirection)).Length];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = i;
            }
            return output;
        }
    }
}

public class HexCell {

    public readonly HexCoordinates coordinates;

    public HexDirection Direction;

    public CorpType type;
    
    public int health;
        
    public Vector3 cellPosition;
    public Vector3 currentPosition;

    public bool isHidden = false;
    public int largeIndex = -1;
    public int size = 0;
    public int NewSize = -1;

    public bool hasUpdated;

    public List<int> forrestNumber = new List<int>();
    public List<Vector3> forrestLocation = new List<Vector3>();
    
    HexCell[] neighbors = new HexCell[6];

    int[] neighbourGridIndex;

    static int tempIndex;

    public Vector3 CellPosition { get { return cellPosition; } }

    public static int nTypes {get { return System.Enum.GetNames(typeof(CorpType)).Length;}} 

    public HexCell(Vector3 _position, CorpType _type, int _Health)
    {
        this.type = _type;
        this.health = _Health;
        this.coordinates = HexCoordinates.hFp(_position);
        this.cellPosition = _position;
        this.currentPosition = _position;
        Direction = (HexDirection)Random.Range(0, 6);
        forrestNumber.Add(HexCoordinates.iFh(coordinates));
        largeIndex = HexCoordinates.iFh(coordinates);
        forrestLocation.Add(_position);
    }

    public HexCell(Vector3 _position)
    {
        this.type = CorpType.Empty;
        this.health = 0;
        this.coordinates = HexCoordinates.hFp(_position);
        this.cellPosition = _position;
		this.currentPosition = _position;
        Direction = (HexDirection)Random.Range(0, 6);
        forrestNumber.Add(HexCoordinates.iFh(coordinates));
        largeIndex = HexCoordinates.iFh(coordinates);
        forrestLocation.Add(_position);

    }

    public HexCell(HexCell cell)
    {
        this.type = cell.type;
        this.health = cell.health;
        this.coordinates = cell.coordinates;
        this.cellPosition = cell.cellPosition;
        this.currentPosition = cell.currentPosition;
		this.Direction= cell.Direction;
        largeIndex = HexCoordinates.iFh(cell.coordinates);
    }
    
    /// <summary>
    /// Returns the hex cell in the direction specified. Will return null otherwise.
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public HexCell GetNeighbor(HexDirection direction)
    {
        return neighbors[(int)direction];
    }

    /// <summary>
    /// DO NOT USE! 
    /// This is called to populate the CA grid and should not be used afterwards.
    /// Sets this cell with a neighbor in the direction specified. Also sets this cell as the neighbor in the oposite direction.
    /// </summary>
    /// <param name="direction"></param> Direction from this cell to the neighbor cell passed into this function
    /// <param name="cell"></param> The neighbor cell passed into this function which is a neighbor.
    public void SetNeighbor(HexDirection direction, HexCell cell)
    {
        neighbors[(int)direction] = cell; 
        cell.neighbors[(int)direction.Opposite()] = this;
    }

    public void updateGridIndex()
    {
        List<int> tempIndex = new List<int>(6);
        for (int i = 0; i < neighbors.Length; i++)
        {
            if (neighbors[i] != null)
            {
                tempIndex.Add(HexCoordinates.iFh(neighbors[i].coordinates));
            }
        }
    }

    /// <summary>
    /// This will change the type of this cell with starting health. 
    /// The cell will not be updated for the loop and neighbour cells will assume old type.
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    public void changeType(CorpType _type, int _Health)
    {
        type = _type;
        health = _Health;
        hasUpdated = true;
        NewSize = 0;
        CAManager.CellSizes[size].Remove(HexCoordinates.iFh(coordinates));
    }

    /// <summary>
    /// This will change the info of this cell.
    /// The cell will not be updated for the loop and neighbour cells will assume old type and values.
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    /// <param name="_ChangeHealth"></param>
    /// <param name="_Direction"></param>
    public void changeInfo(CorpType _type, int _Health, HexDirection _Direction)
    {
        type = _type;
        health = _Health;
        Direction = _Direction;
        hasUpdated = true;
    }

    /// <summary>
    /// This will move the oldCell into this cell. the current positions will swap so it will lerp there not teleport.
    /// The old cell is set to empty and zero health.
    /// </summary>
    /// <param name="oldCell"></param> The old cell which will move
    public void MoveToCell(HexCell oldCell)
    {
        type = oldCell.type;
        health = oldCell.health;
        Direction = oldCell.Direction;
        currentPosition = oldCell.currentPosition;
        oldCell.changeType(CorpType.Empty, 0);
        hasUpdated = true;
    }

    /// <summary>
    /// This will add the health to this cell and kill the old cell, only if the type are the same.
    /// </summary>
    /// <param name="oldCell"></param> Old cell which gets killed after health is merged.
    public void AddtoCell(HexCell oldCell)
    {
        if (this.type == oldCell.type)
        {
            this.health += oldCell.health;
            oldCell.changeType(CorpType.Empty, 0);
            hasUpdated = true;
        }
    }

    /// <summary>
    /// This will swap cells, the current positions are swapped so they lerp to eachothers cell position not teleport.
    /// </summary>
    /// <param name="oldCell"></param>
    public void swapCell(HexCell oldCell)
    {
        CorpType _tempType = this.type;
        int _tempHealth = this.health;
        HexDirection _tempMotion = this.Direction;
        Vector3 _tempPosition = this.currentPosition;

        type = oldCell.type;
        health = oldCell.health;
        Direction = oldCell.Direction;
        currentPosition = oldCell.currentPosition;
        
        oldCell.changeInfo(_tempType, _tempHealth, _tempMotion);
        oldCell.currentPosition = _tempPosition;
        hasUpdated = true;
    }


    public static void makeLarge(HexCell cell, int gridIndex, int newSize)
    {
        List<int> neighboursToTurnOff = new List<int>();
        bool canEnlarge = true;

        for (int dx = -newSize; dx <= newSize; dx++)
        {
            if (!canEnlarge)
            {
                break;
            }
            for (int dz = -newSize; dz <= newSize; dz++)
            {
                if (Mathf.Abs(dx + dz) <= newSize && !(Mathf.Abs(dx) + Mathf.Abs(dz) == 0))
                {
                    tempIndex = HexCoordinates.iFh(new HexCoordinates(cell.coordinates.X + dx, cell.coordinates.Z + dz));
                    if (tempIndex >= 0 && tempIndex < CAManager.CAGrid.Length && CAManager.CAGrid[tempIndex].isHidden)
                    {
                        if (CAManager.CAGrid[CAManager.CAGrid[tempIndex].largeIndex].size < newSize)
                        {
                            neighboursToTurnOff.Add(CAManager.CAGrid[tempIndex].largeIndex);
                        }
                        else
                        {
                            canEnlarge = false;
                            break;
                        }
                    }
                }
            }
        }
        if (canEnlarge)
        {
            for (int i = 0; i < neighboursToTurnOff.Count; i++)
            {
                CAManager.CAGrid[neighboursToTurnOff[i]].NewSize = 0;
            }
            cell.NewSize = newSize;
            CAManager.CellSizes[cell.NewSize].Add(gridIndex);
            cell.largeIndex = gridIndex;
            cell.isHidden = false;
        }
    }
    /*
    public void updateInfo()
    {
        if (willChange)
        {
            this.type = tempType;
            this.health = tempHealth;
            this.changeInHealth = tempChangeInHealth;
            this.direction = tempDirection;
            this.currentPosition = tempPosition;
            willChange = false;
            hasChanged = true;
        }
    }
    */


}

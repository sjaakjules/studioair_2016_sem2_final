﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using System.Linq;


/// <summary>
/// This is the original CA manager, 
/// </summary>
public class CAManager : MonoBehaviour
{
    [Header("-- DEBUG: TURN ON TO CHECK ERROR IN CA --")]
    public bool CheckErrorInCA = false;

    [HeaderAttribute("-- CA INFORMATION --")]
    public CorpType CellTypes;
    public static int  _totalKilled = 0;
    public int[] TypeScore = new int[HexCell.nTypes];
    public static int[] _TypeScore = new int[HexCell.nTypes];
    public int[] TypeCount = new int[HexCell.nTypes];

    public int[] CellSizeCount = new int[5];
    public static List<int>[] CellSizes = new List<int>[5];


    [HeaderAttribute("Player position")]
    public Transform playerPosition;


    [HeaderAttribute("CA Cell settings - for each type")]
	public Rules RulesForEachType = Rules.nothing;
    public int startingHealthForEachType = 1;
    public int maxHealth = 50;
    public static int _maxHealth;

    [HeaderAttribute("Spawn via Intensity Map Settings")]
    public bool spawnCAFromIntensityMap = false;
    public CSVmaps spawnIntensityMap;
    public static CSVmaps _spawnIntensityMap;
    public bool replaceAllExistingTypes = false;
    public static bool _spawnCAFromIntensityMap = false;

    [HeaderAttribute("Random spawn settings")]
    public float randomPopulatePercent;
    static float _randomPopulatePercent;
    public CorpType typeToRandomSpawn;
    public bool spawnOneTypeRandomly = false;
    public bool InjectOneTypesRandomly = false;
    public bool spawnAllTypeRandomly = false;
    public bool InjectAllTypesRandomly = false;

    //public int SaveCAThisFrequently = 20;
    int currentState = 0;


    public static HexCell[] CAGrid;

    // Ignore these settings, they are future settings or house keeping
    float targetValue;
    public static float _targetValue;
    float valueRange;
    public static float _valueRange;
    public static int[] _typeCount = new int[HexCell.nTypes];
    static Vector3 tempPosition;
    static int tempIndex;
    CAThread CAhandler;
    public float refreshrate = 0.3f;
    float elapseTime = 0;
    static bool hasPopulatedGrid = false;

    void Awake()
    {
        for (int i = 0; i < CellSizes.Length; i++)
        {
            CellSizes[i] = new List<int>();
        }
    }

    void updateStaticVariables()
    {
        _randomPopulatePercent = randomPopulatePercent;
        _maxHealth = maxHealth;
        _targetValue = targetValue;
        _valueRange = valueRange;
        _spawnIntensityMap = spawnIntensityMap;
        TypeScore = (int[])_TypeScore.Clone();
        TypeCount = (int[])_typeCount.Clone();
        for (int i = 0; i < CellSizes.Length; i++)
        {
            CellSizeCount[i] = CellSizes[i].Count();
        }
    }

    // /////////////////////////////////////////////////////////
    //                  Start
    // /////////////////////////////////////////////////////////
    void Start()
    {
        // Static setters
        updateStaticVariables();

        // Set size of the CA according to the CSV or standard size.
        CAGrid = new HexCell[CSV.gridXLength * CSV.gridZLength];

        // Populate the grid with empty cells
        for (int z = 0; z < CSV.gridZLength; z++)
        {
            for (int x = 0; x < CSV.gridXLength; x++)
            {
                CreateEmptyCell(CAGrid, x, z);
            }
        }

        // Move the cells to the terrain height.
        for (int i = 0; i < CSV.dataMap.Count; i++)
        {
            updateHeight(CSV.dataMap[i]);
        }

        CAhandler = new CAThread(maxHealth, RulesForEachType);

        if (spawnCAFromIntensityMap)
        {
            SpawnFromIntensity(CAGrid, spawnIntensityMap, replaceAllExistingTypes, startingHealthForEachType);
            _spawnCAFromIntensityMap = false;
        }

        if (spawnOneTypeRandomly)
        {
            randomPopulateType(ref CAGrid, typeToRandomSpawn, startingHealthForEachType);
        }

        if (spawnAllTypeRandomly)
        {
            randomPopulate(ref CAGrid, startingHealthForEachType);
        }

        CAhandler.loadCA();

    }


    // /////////////////////////////////////////////////////////
    //                  Loop, Update
    // /////////////////////////////////////////////////////////
    void Update()
    {
        updateStaticVariables();
        

        elapseTime += Time.deltaTime;
        if (elapseTime > refreshrate)
        {
            if (!CAhandler.running)
            {
                if (_spawnCAFromIntensityMap)
                {
                    SpawnFromIntensity(CAGrid, spawnIntensityMap, replaceAllExistingTypes, startingHealthForEachType);
                    _spawnCAFromIntensityMap = false;
                }
                if (InjectAllTypesRandomly)
                {
                    randomPopulate(ref CAGrid, startingHealthForEachType);
                    InjectAllTypesRandomly = false;
                }
                if (InjectOneTypesRandomly)
                {
                    randomPopulateType(ref CAGrid,typeToRandomSpawn, startingHealthForEachType);
                    InjectOneTypesRandomly = false;
                }
                // Run the CA after the non CA rules have been applied,
                CAhandler.New_maxHealth = maxHealth;
                CAhandler.New_playerPosition = playerPosition.position;
                CAhandler.New_typeRules = RulesForEachType;

                
                if (SaveStateManager._saveCAStates && SaveStateManager._CASaveFrequency != 0 && currentState % SaveStateManager._CASaveFrequency == 0)
                {
                    CSV.SaveCA(currentState, CAGrid, true);
                }
                currentState++;

                if (CheckErrorInCA)
                {
                    CAhandler.AgentMovement();
                }
                else
                {
                    CAhandler.Start();
                }
                //
                elapseTime = 0;
            }
        }

    }



    void updateHeight(Vector3 position)
    {
        int i = HexCoordinates.iFp(position);
        if (i >= 0 && i < CAGrid.Length)
        {
            CAGrid[i].cellPosition.y = position.y;
        }
    }



    // /////////////////////////////////////////////////////////
    //                  Support Functions
    // /////////////////////////////////////////////////////////

    public static void randomPopulate(ref HexCell[] cellGrid, int startingHealth)
    {
        int rndNum1, rndNum2;
        for (int i = 0; i < cellGrid.Length; i++)
        {
            rndNum1 = Random.Range(0, 10000);
            if (rndNum1 < _randomPopulatePercent*100)
            {
                rndNum2 = Random.Range(1, HexCell.nTypes);
                cellGrid[i].changeType((CorpType)rndNum2, startingHealth);
            }
        }

    }

    public static void SpawnFromIntensity(HexCell[] cellGrid,CSVmaps spawnMap, bool replaceExisting, int startHealth)
    {
        int tempIntensityValue;
        for (int i = 0; i < cellGrid.Length; i++)
        {
            tempIntensityValue = (int)(CSV.getIntensity(spawnMap, i));
            if (tempIntensityValue > 0 && tempIntensityValue < HexCell.nTypes)
            {
                if (replaceExisting)
                {
                    cellGrid[i].changeType((CorpType)tempIntensityValue, startHealth);
                }
                else if (cellGrid[i].type == CorpType.Empty)
                {
                    cellGrid[i].changeType((CorpType)tempIntensityValue, startHealth);
                }
            }
            CSV.addToIntensityValue(-CSV.getIntensity(spawnMap, i), spawnMap, i);
        }
    }

    public static void randomPopulateType(ref HexCell[] cellGrid, CorpType populationType, int startingHealth)
    {
        int rndNum1;
        for (int i = 0; i < cellGrid.Length; i++)
        {
            rndNum1 = Random.Range(0, 100);
            if (rndNum1 < _randomPopulatePercent && cellGrid[i].type == CorpType.Empty)
            {
                cellGrid[i].changeType(populationType, startingHealth);

            }
        }

    }

    public static void CreateEmptyCell(HexCell[] cellArray, int x, int z)
    {
        // Make empty cell from CSV 
        tempPosition = HexCoordinates.pFr(x, z);
        tempIndex = HexCoordinates.iFr(x, z);
        if (cellArray[tempIndex] == null)
        {
            cellArray[tempIndex] = new HexCell(tempPosition);
        }

        if (z > 0)
        {
            cellArray[tempIndex].SetNeighbor(HexDirection.S, cellArray[tempIndex - 1]);
            if ((x & 1) == 0) // This will be true for even numbers!!!
            {
                if (x < CSV.gridXLength - 1)
                {
                    cellArray[tempIndex].SetNeighbor(HexDirection.SE, cellArray[tempIndex + CSV.gridZLength - 1]);
                }
                if (x > 0)
                {
                    cellArray[tempIndex].SetNeighbor(HexDirection.SW, cellArray[tempIndex - CSV.gridZLength - 1]);
                }
            }
        }
        if (!((x & 1) == 0)) // if its odd and all Z
        {
            cellArray[tempIndex].SetNeighbor(HexDirection.SW, cellArray[tempIndex - CSV.gridZLength]);

            if (x < CSV.gridXLength - 1)
            {
                if (cellArray[tempIndex + CSV.gridZLength] == null)
                {
                    cellArray[tempIndex + CSV.gridZLength] = new HexCell(HexCoordinates.pFi(tempIndex + CSV.gridZLength));
                }
                cellArray[tempIndex].SetNeighbor(HexDirection.SE, cellArray[tempIndex + CSV.gridZLength]);
            }
        }
    }


    public static List<HexDirection> findLowerNeighbours(CSVmaps intensityMap, float[][] NeighbourIntensities, HexCell cell)
    {
        float tempValue;
        float cellValue = CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.coordinates));
        List<HexDirection> lowerNeighbours = new List<HexDirection>();
        for (int i = 0; i < 6; i++)
        {
            tempValue = NeighbourIntensities[(int)intensityMap][i];
            if (tempValue < cellValue)
            {
                lowerNeighbours.Add((HexDirection)i);
            }
        }
        lowerNeighbours.Sort((y, x) => CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.GetNeighbor(x).coordinates)).CompareTo(CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.GetNeighbor(y).coordinates))));
        return lowerNeighbours;
    }

    public static List<HexDirection> findHeigherNeighbours(CSVmaps intensityMap, float[][] NeighbourIntensities, HexCell cell)
    {
        float tempValue;
        float cellValue = CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.coordinates));
        List<HexDirection> HeigherNeighbours = new List<HexDirection>();
        for (int i = 0; i < 6; i++)
        {
            tempValue = NeighbourIntensities[(int)intensityMap][i];
            if (tempValue > cellValue)
            {
                HeigherNeighbours.Add((HexDirection)i);
            }
        }
        HeigherNeighbours.Sort((x, y) => CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.GetNeighbor(x).coordinates)).CompareTo(CSV.getIntensity(intensityMap, HexCoordinates.iFh(cell.GetNeighbor(y).coordinates))));
        return HeigherNeighbours;
    }


    public static int[] createRandomList(int max)
    {
        List<int> shuffledList = Enumerable.Range(0, max).ToList<int>();
        var rnd = new System.Random();
        return shuffledList.OrderBy(item => rnd.Next()).ToArray<int>();
    }
}

﻿using UnityEngine;
using System.Collections;

public class HexCellTest : MonoBehaviour {

    public Vector3 Position;
    public Vector3 posInd, posZig, posHex;
    public int IndexPos, indexHex, indexZig;
    public Vector2 zigInd, Zighex, zigPos;
    public HexCoordinates hexPos, hexInd, hexZig;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Position = gameObject.transform.position;
        hexPos = HexCoordinates.hFp(Position);
        IndexPos = HexCoordinates.iFp(Position);
        zigPos = HexCoordinates.rFp(Position);

        zigInd = HexCoordinates.rFi(IndexPos);
        Zighex = HexCoordinates.rFh(hexPos);

        indexHex = HexCoordinates.iFh(hexPos);
        indexZig = HexCoordinates.iFr((int)zigPos.x, (int)zigPos.y);

        posInd = HexCoordinates.pFi(IndexPos);
        posHex = HexCoordinates.pFh(hexPos);
        posZig = HexCoordinates.pFr((int)zigPos.x, (int)zigPos.y);

        hexZig = HexCoordinates.hFr((int)zigPos.x, (int)zigPos.y);
        hexInd = HexCoordinates.hFi(IndexPos);

    }
}

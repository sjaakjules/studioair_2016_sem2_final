﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;


public class CSV : MonoBehaviour
{
    static object IntensityWriterLock = new object();
    public static object specialRulesLock = new object();
    CSVmaps MapTypes;
    [Header("The names of your CSV maps (in EnumManager)")]
    public string[] MapNames = Enum.GetNames(typeof(CSVmaps));
    // Load you CSV in the inspector
    [Header("Load you CSV Here:")]
    public TextAsset[] DataMapCSV = new TextAsset[Enum.GetNames(typeof(CSVmaps)).Length];

    [Header("What do you want the Max intensity to be?")]
    public float maxIntensity = 20;
    public static float _maxIntensity;

    [Header("Do you want to save the intensity maps??")]
    //public bool saveIntensity = false;
   // public float intensityHistorySaveRate = 1.0f;

    [Header("CSV status")]
    public int dataValuesRead;
    public bool[] hasReadCSV;
    public static int gridZLength;
    public static int gridXLength;
    public float cellRadius;

    static string filePathAgentBase;
    static string filePathIntensityBase;
    static string filePathCABase;
    static bool hasSetupFilePath = false;

    

    List<float[]>[] IntensityHistory;
    List<float> HistroyTime = new List<float>();

    static int tempintensityMapIndex;
    static List<float[]> tempintensities;
    static List<float> temptimeStamp;
    static bool tempoverWriteFile;
    


    // These are variables which you access from other scripts.
    public static List<Vector3> dataMap;
    static List<float>[] data;
    public static float CellRadius;
    public static Dictionary<int,float> SpecialRules = new Dictionary<int, float>();
    public static bool clearSpecialRules = false;

    public static int nData { get { return data.Length; } }

    public static int nCSVMaps { get { return Enum.GetNames(typeof(CSVmaps)).Length; } }
    

    char row = '\n';
    char column = ',';
    static string delimiter = ",";

    public static int[] lowRender;


    // Use this for initialization
    void Awake()
    {
        setUpFilePaths();
        _maxIntensity = maxIntensity;
        if (Enum.GetNames(typeof(CSVmaps)).Length < DataMapCSV.Length)
        {
            Debug.LogError("You have a different number of CSV maps in your CSVManager from the types written in your EnumManager script!");
        }
        // Setup the list of data and point lists to be the same size as the number of intensity maps loaded
        dataMap = new List<Vector3>();
        data = new List<float>[DataMapCSV.Length];
        hasReadCSV = new bool[DataMapCSV.Length];

        bool isSettingPositions = false;
        bool hasSetPositions = false;

        // For each intensity map we need to tell the computer we will have a list of points (vector3) and data values
        for (int j = 0; j < DataMapCSV.Length; j++)
        {
            data[j] = new List<float>();


            string[] lines = DataMapCSV[j].text.Split(row);
            string[] line;

            float tempx, tempy, tempz;


            if (lines[0].Split(column).Length == 4 && hasSetPositions == false)
            {
                isSettingPositions = true;
            }

            for (int i = 1; i < lines.Length; i++)
            {
                line = lines[i].Split(column);
                if (line.Length == 3 && float.TryParse(line[0], out tempx) && float.TryParse(line[1], out tempy) && float.TryParse(line[2], out tempz))
                {
                    if (isSettingPositions)
                    {
                        dataMap.Add(new Vector3(tempx, 0, tempy));
                    }
                    data[j].Add(tempz);
                }
            }

            if (isSettingPositions)
            {
                line = lines[0].Split(column);
                if (line.Length == 4)
                {
                    if (float.TryParse(line[3], out cellRadius))
                    {
                        CellRadius = cellRadius;
                        HexMetrics.cellRadius = cellRadius;

                    }
                    if (int.TryParse(line[1], out gridZLength))
                    {
                        gridXLength = dataMap.Count / gridZLength;
                    }
                }
            }

            dataValuesRead = data[j].Count;

            if (data[j].Count > 0)
            {
                hasReadCSV[j] = true;
            }

            if (lines[0].Split(column).Length == 4 && isSettingPositions == true)
            {
                hasSetPositions = true;
                isSettingPositions = false;
            }
        }
    }

    void Start()
    {
        //SpecialRules = new float[dataMap.Count];
        IntensityHistory = new List<float[]>[data.Length];
        for (int i = 0; i < IntensityHistory.Length; i++)
        {
            IntensityHistory[i] = new List<float[]>();
        }
        addHistoryInfo();
        if (SaveStateManager._saveIntensityStates)
        {
            setupIntensityHistory();
            InvokeRepeating("addHistoryInfo", SaveStateManager._intensitySaveStateTime, SaveStateManager._intensitySaveStateTime);
            InvokeRepeating("asyncUpdateCSV", SaveStateManager._intensitySaveStateTime * 1.5f, SaveStateManager._intensitySaveStateTime);
        }

        lowRender = new int[dataMap.Capacity];


    }

    // Update is called once per frame
    void Update()
    {
        _maxIntensity = maxIntensity;
        lock (specialRulesLock)
        {
            if (clearSpecialRules)
            {
                SpecialRules.Clear();
                clearSpecialRules = false;
            }
        }

    }

    void OnValidate()
    {
        MapNames = Enum.GetNames(typeof(CSVmaps));
        if (DataMapCSV.Length != nCSVMaps)
        {
            Array.Resize(ref DataMapCSV, nCSVMaps);
        }
    }

    public void setupIntensityHistory()
    {
        for (int i = 0; i < IntensityHistory.Length; i++)
        {
            Savecsv(i, IntensityHistory[i], HistroyTime, true);
        }
        HistroyTime.Clear();
    }

    void addHistoryInfo()
    {
        lock (IntensityWriterLock)
        {
            for (int i = 0; i < IntensityHistory.Length; i++)
            {
                IntensityHistory[i].Add(data[i].ToArray());
            }
            HistroyTime.Add(Time.time);
        }
    }

    void asyncUpdateCSV()
    {
        ThreadPool.QueueUserWorkItem(updateIntensities);
    }

    void updateIntensities(object state)
    {
        lock (IntensityWriterLock)
        {
            if (HistroyTime.Count > 0)
            {
                for (int i = 0; i < DataMapCSV.Length; i++)
                {
                    string IntensityfilePath = filePathIntensityBase + i.ToString() + "IntensityMap.csv";//@"/Saved_data.csv";

                    string[][] output = new string[IntensityHistory[i].Count][];
                    for (int j = 0; j < IntensityHistory[i].Count; j++)
                    {
                        output[j] = new string[IntensityHistory[i][0].Length + 1];
                        output[j][0] = HistroyTime[j].ToString("F2");
                        for (int k = 0; k < IntensityHistory[i][0].Length; k++)
                        {
                            output[j][k + 1] = IntensityHistory[i][j][k].ToString("F3");
                        }
                    }
                    IntensityHistory[i].Clear();

                    using (StreamWriter writer = File.AppendText(IntensityfilePath))
                    {
                        for (int j = 0; j < output.Length; j++)
                        {
                            writer.WriteLine(string.Join(delimiter, output[j]));
                        }
                    }
                }
                HistroyTime.Clear();
            }
        }
    }

    public static int FindIndex(Vector3 Position)
    {
        return HexCoordinates.iFp(Position);
    }

    public static float getIntensity(CSVmaps intensityMap, int GridIndex)
    {
        if (GridIndex >= 0 && GridIndex < data[(int)intensityMap].Count)
        {
            return data[(int)intensityMap][GridIndex];
        }
        return float.NaN;
    }

    /// <summary>
    /// This will add the new value to the intensity map at the GridIndex
    /// </summary>
    /// <param name="newValue"></param>
    /// <param name="intensityMap"></param>
    /// <param name="GridIndex"></param>
    public static void addToIntensityValue(float newValue, CSVmaps intensityMap, int GridIndex)
    {
        if (GridIndex>=0 && GridIndex< data[(int)intensityMap].Count)
        {
            if (data[(int)intensityMap][GridIndex] < _maxIntensity && data[(int)intensityMap][GridIndex] > -_maxIntensity)
            {
                data[(int)intensityMap][GridIndex] += newValue;
            }
        }
    }

    public static float[] getIntensities(int GridIndex)
    {
        if (GridIndex >= 0 && GridIndex < data[0].Count)
        {
            float[] intensityOut = new float[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                intensityOut[i] = data[i][GridIndex];
            }
            return intensityOut;
        }
        return null;
    }


    void setUpFilePaths()
    {

        if (!hasSetupFilePath && Application.platform == RuntimePlatform.WindowsEditor)
        {

            filePathAgentBase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/Agent/";
            filePathIntensityBase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/IntensityMaps/";
            filePathCABase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/CAstates/";
            Directory.CreateDirectory(filePathAgentBase);
            Directory.CreateDirectory(filePathIntensityBase);
            Directory.CreateDirectory(filePathCABase);
            hasSetupFilePath = true;
        }
        else if (!hasSetupFilePath && Application.platform == RuntimePlatform.OSXEditor)
        {
            filePathAgentBase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/Agent/";
            filePathIntensityBase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/IntensityMaps/";
            filePathCABase = Application.dataPath + "/Output/" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + "/CAstates/";
            Directory.CreateDirectory(filePathAgentBase);
            Directory.CreateDirectory(filePathIntensityBase);
            Directory.CreateDirectory(filePathCABase);
            hasSetupFilePath = true;
        }
    }

    public static void SaveCA(int LoopNumber, HexCell[] CA, bool overWriteFile)
    {

        string CAfilePath = filePathCABase + LoopNumber.ToString() + " CASystem.csv";

        string[][] output = new string[CA.Length][];
        for (int i = 0; i < CA.Length; i++)
        {
            output[i] = new string[7];
            output[i][0] = i.ToString();
            output[i][1] = CA[i].type.ToString();
            output[i][2] = ((int)CA[i].type).ToString();
            output[i][3] = CA[i].health.ToString();
            output[i][4] = (CA[i].health/100).ToString();
            output[i][5] = CA[i].size.ToString();
            output[i][6] = CA[i].isHidden.ToString();

        }

        if (overWriteFile)
        {
            using (StreamWriter writer = File.CreateText(CAfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
        else
        {
            using (StreamWriter writer = File.AppendText(CAfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
    }
    
    /*
    public static void AsyncSavecsv(object state)
    {
        string IntensityfilePath = filePathIntensityBase + tempintensityMapIndex.ToString() + "IntensityMap.csv";//@"/Saved_data.csv";

        string[][] output = new string[tempintensities.Count][];
        for (int i = 0; i < tempintensities.Count; i++)
        {
            output[i] = new string[tempintensities[0].Length + 1];
            output[i][0] = temptimeStamp[i].ToString("F2");
            for (int j = 0; j < tempintensities[0].Length; j++)
            {
                output[i][j + 1] = tempintensities[i][j].ToString("F3");
            }
        }
        tempintensities.Clear();

        if (tempoverWriteFile)
        {
            using (StreamWriter writer = File.CreateText(IntensityfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
        else
        {
            using (StreamWriter writer = File.AppendText(IntensityfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
    }
    */

    public static void Savecsv(int intensityMapIndex, List<float[]> intensities, List<float> timeStamp, bool overWriteFile)
    {
        string IntensityfilePath = filePathIntensityBase + intensityMapIndex.ToString() + "IntensityMap.csv";//@"/Saved_data.csv";

        string[][] output = new string[intensities.Count][];
        for (int i = 0; i < intensities.Count; i++)
        {
            output[i] = new string[intensities[0].Length + 1];
            output[i][0] = timeStamp[i].ToString("F2"); 
            for (int j = 0; j < intensities[0].Length; j++)
            {
                output[i][j + 1] = intensities[i][j].ToString("F3");
            }
        }
        intensities.Clear();

        if (overWriteFile)
        {
            using (StreamWriter writer = File.CreateText(IntensityfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
        else
        {
            using (StreamWriter writer = File.AppendText(IntensityfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
    }


    public static void Savecsv(int AgentNumber, List<Vector3> Positions, List<float[]> intensities,List<int> AgentIndex,List<float> timeStamp,string agentNumber, bool overWriteFile)
    {

        string AgentfilePath = filePathAgentBase + AgentNumber.ToString() + "Agent.csv";


        string[][] output = new string[Positions.Count][];
        for (int i = 0; i < Positions.Count; i++)
        {
            output[i] = new string[intensities[0].Length+6];
            output[i][0] = timeStamp[i].ToString("F2");
            output[i][1] = agentNumber;
            output[i][2] = AgentIndex[i].ToString();
            output[i][3] = Positions[i].x.ToString("F3");
            output[i][4] = Positions[i].z.ToString("F3");
            output[i][5] = Positions[i].y.ToString("F3");
            for (int j = 0; j < intensities[0].Length; j++)
            {
                output[i][j + 6] = intensities[i][j].ToString("F3");
            }
        }
        Positions.Clear();
        intensities.Clear();
        AgentIndex.Clear();
        timeStamp.Clear();

        if (overWriteFile)
        {
            using (StreamWriter writer = File.CreateText(AgentfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
        else
        {
            using (StreamWriter writer = File.AppendText(AgentfilePath))
            {
                for (int i = 0; i < output.Length; i++)
                {
                    writer.WriteLine(string.Join(delimiter, output[i]));
                }
            }
        }
    }

    public static Vector3[][] readPoints(TextAsset CSVFile)
    {
        int nPoints;
        int nMeshes;


        // Setup the list of data and point lists to be the same size as the number of intensity maps loaded
        Vector3[][] dataOut = new Vector3[1][];
        bool hasReadCSVOK = false;

        char row = '\n';
        char column = ',';

        string[] lines = CSVFile.text.Split(row);
        string[] line;

        float tempx, tempy, tempz;

        string[] Settingsline = lines[0].Split(column);

        int[] settingsValue = new int[Settingsline.Length / 2];
        string[] settingsName = new string[Settingsline.Length / 2];
        int tempValue;

        for (int i = 0; i < Settingsline.Length / 2; i++)
        {
            settingsName[i] = Settingsline[2 * i];
            if (int.TryParse(Settingsline[2 * i + 1], out tempValue))
            {
                settingsValue[i] = tempValue;
                hasReadCSVOK = true;
            }
            else
            {
                Debug.LogError("Error reading CSV, one of the settings isnt an int.");
                hasReadCSVOK = false;
            }
        }

        if (hasReadCSVOK)
        {
            nPoints = settingsValue[0];
            nMeshes = settingsValue[1];

            dataOut = new Vector3[nMeshes][];
            for (int i = 0; i < nMeshes; i++)
            {
                dataOut[i] = new Vector3[nPoints];
            }

            for (int i = 1; i < nPoints+1; i++)
            {
                line = lines[i].Split(column);

                for (int j = 0; j < nMeshes; j++)
                {
                    if (float.TryParse(line[j], out tempx) && float.TryParse(line[j+1], out tempy) && float.TryParse(line[j+2], out tempz))
                    {
                        dataOut[j][i-1] =new Vector3(tempx, tempz, tempy);
                        hasReadCSVOK = true;
                    }
                    else if (line.Length > 0)
                    {
                        Debug.LogError("Error reading CSV, Row {0} could not be read.");
                        hasReadCSVOK = false;
                    }
                }
            }
        }

        return dataOut;
    }

    public static int[][] readTriangles(TextAsset CSVFile)
    {
        int nMeshes;
        int nTriangles;


        // Setup the list of data and point lists to be the same size as the number of intensity maps loaded
        int[][] dataOut = new int[1][];
        bool hasReadCSVOK = false;

        char row = '\n';
        char column = ',';

        string[] lines = CSVFile.text.Split(row);
        string[] line;

        int tempx, tempy, tempz;

        string[] Settingsline = lines[0].Split(column);

        int[] settingsValue = new int[Settingsline.Length / 2];
        string[] settingsName = new string[Settingsline.Length / 2];
        int tempValue;

        for (int i = 0; i < Settingsline.Length / 2; i++)
        {
            settingsName[i] = Settingsline[2 * i];
            if (int.TryParse(Settingsline[2 * i + 1], out tempValue))
            {
                settingsValue[i] = tempValue;
                hasReadCSVOK = true;
            }
            else
            {
                Debug.LogError("Error reading CSV, one of the settings isnt an int.");
                hasReadCSVOK = false;
            }
        }

        if (hasReadCSVOK)
        {
            nMeshes = settingsValue[1];
            nTriangles = settingsValue[2];

            dataOut = new int[nMeshes][];
            for (int i = 0; i < nMeshes; i++)
            {
                dataOut[i] = new int[nTriangles * 3];
            }

            for (int i = 1; i < nTriangles+1; i++)
            {
                line = lines[i].Split(column);

                for (int j = 0; j < nMeshes; j++)
                {
                    if (int.TryParse(line[j], out tempx) && int.TryParse(line[j + 1], out tempy) && int.TryParse(line[j + 2], out tempz))
                    {
                        dataOut[j][(i - 1) * 3] = tempx;
                        dataOut[j][(i - 1) * 3 + 1] = tempz;
                        dataOut[j][(i - 1) * 3 + 2] = tempy;
                        hasReadCSVOK = true;
                    }
                    else if (line.Length > 0)
                    {
                        Debug.LogError("Error reading CSV, Row {0} could not be read.");
                        hasReadCSVOK = false;
                    }
                }
            }
        }

        return dataOut;
    }
}

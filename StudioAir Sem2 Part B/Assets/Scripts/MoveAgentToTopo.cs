﻿using UnityEngine;
using System.Collections;

public class MoveAgentToTopo : MonoBehaviour
{
    Vector3 tempPosition;
    Vector3 startPosition;
    float tempIntensity;
    public float heightChange = 1;

    Transform parentsTransform;

    Agent agent;
    
    
    // Use this for initialization
    void Start()
    {
        agent = transform.GetComponentInParent<Agent>();

        startPosition = transform.position;
        tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, agent.gridIndexAtAgent);
        if (!float.IsNaN(tempIntensity))
        {
            tempPosition = transform.position;
            tempPosition.y = startPosition.y + CSV.getIntensity(SingleMeshRender._intensityMap, agent.gridIndexAtAgent) + heightChange;
            transform.position = tempPosition;
        }

    }

    // Update is called once per frame
    void Update()
    {
        tempIntensity = CSV.getIntensity(SingleMeshRender._intensityMap, agent.gridIndexAtAgent);
        if (!float.IsNaN(tempIntensity))
        {
            tempPosition = transform.parent.position;
            tempPosition.y = CSV.getIntensity(SingleMeshRender._intensityMap, agent.gridIndexAtAgent) + heightChange;
            transform.position = tempPosition;
        }
    }
}
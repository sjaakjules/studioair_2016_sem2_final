﻿using UnityEngine;

public class PCCamera : MonoBehaviour {

    Transform swivel, stick;
	float zoom = 1f;
    public float stickMinZoom, stickMaxZoom;

    void Awake()
	{
        swivel = transform.GetChild(0);
        stick = swivel.GetChild(0);
    }

    // Use this for initialization
    void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        float zoomDelta = Input.GetAxis("Mouse ScrollWheel");
		if (zoomDelta != 0f)
		{
            AdjustZoom(zoomDelta);
        }

    }

	void AdjustZoom(float delta)
	{
        zoom = Mathf.Clamp01(zoom + delta);
    }
}

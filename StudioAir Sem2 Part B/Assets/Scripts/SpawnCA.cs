﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnCA : MonoBehaviour {

    [HeaderAttribute("-- Hex Cell Types --")]
    public CorpType HexCellTypes;

    [HeaderAttribute("Percent to spawn for each type")]
    public bool SpawnRandomly;
    public float[] spawnPercent;
    Agent parentAgent;
    bool isAgent = false;

	// Use this for initialization
	void Start () {
        parentAgent = transform.GetComponentInParent<Agent>();
        if (parentAgent != null)
        {
            isAgent = true;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (isAgent && SpawnRandomly)
        {
            int chance = Random.Range(0, 10000);
            for (int i = 0; i < spawnPercent.Length; i++)
            {
                chance = Random.Range(0, 10000);
                if (chance < spawnPercent[i]*100f)
                {
                    int pos = parentAgent.gridIndiciesInSight[Random.Range(0, parentAgent.gridIndiciesInSight.Count)];
                    CSV.addToIntensityValue(i - CSV.getIntensity(CAManager._spawnIntensityMap, pos), CAManager._spawnIntensityMap, pos);
                    CAManager._spawnCAFromIntensityMap = true;
                }
            }
        }
    }

    void OnValidate()
    {
        if (spawnPercent.Length != HexCell.nTypes)
        {
            System.Array.Resize(ref spawnPercent, HexCell.nTypes);
        }
    }
    
}

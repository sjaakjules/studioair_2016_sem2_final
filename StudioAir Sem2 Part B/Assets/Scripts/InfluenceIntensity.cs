﻿using UnityEngine;
using System.Collections;

public class InfluenceIntensity : MonoBehaviour {

    [Header("3. Define how the agent affects intensity maps")]
    public CSVmaps[] CSVIntensityMapChange;
    public float[] changeToIntensityValuePerMap;

    public GameObject tracker;

    int gridIndicie;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (tracker != null)
        {
            gridIndicie = CSV.FindIndex(tracker.transform.position);
        }
        else
        {
            gridIndicie = CSV.FindIndex(transform.position);
        }


        // -------------------------------------------------------------------------------------
        //                   Change the intensity where the agent is
        // -------------------------------------------------------------------------------------
        for (int i = 0; i < changeToIntensityValuePerMap.Length; i++)
        {
            CSV.addToIntensityValue(changeToIntensityValuePerMap[i], (CSVmaps)i, gridIndicie);
        }

    }
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SuchRulesMuchCoding : MonoBehaviour {

	public bool allweeds = true;
	static bool _allweeds;

    [Header("Percentage chance for Tree to spawn")]
    [Range(0,100)]
	public float populatePercentage; static float populatevegpercent; static float _populatevegpercent;
    [Header("Stress Perameters")]
    public int stressDecreaseAmount; static int stressdecreaseamount; public static int _StressDecreaseAmount;
    public int stressIncreaseAmount; static int stressincreaseamount; static int _StressIncreaseAmount;
    [Range(0, 100)]
     float stressIncreasePercentage; static float stressincreasepercentage; static float _stressincreasepercentage;
    [Range(0, 100)]
    public float stressSpreadPercentage; static float stressspreadpercent; static float _stressspreadpercentage;
    [Range(0, 1)]
    public float stressSpreadDecay; static float StressSpreadDecay; static float _StressSpreadDecay;
    [Range(100, 199)]
    public int stressMinWeedAge; static int StressMinWeedAge; static int _StressMinWeedAge;
    [Header("Chance for lightnight strike")]
    [Range(0, 0.1f)]
    public float randomWeedGeminationPercentage; static float RandomWeedGeminationPercentage; static float _RandomWeedGeminationPercentage;
    [Header("Weed Propergation Perameters")]
    [Range(100, 199)]
    public int weedGerminationAge; static int treeInfectionAge; static int _treeInfectionAge;
    [Range(0, 100)]
     float weedGerminationPercentage; static float treeInfectionPercentage; static float _treeInfectionPercentage;
    [Range(0, 100)]
     float weedAgingPercentage; static float healthIncreasePercentage; static float _healthincreasepercent;
     int weedAgingAmount; static int healthIncreasePerTurn; static int _weedmaxhealthchange;
    

    float Size1 = 5; static float Scale1;  static float _Scale1;
    float Size2 = 10; static float Scale2; static float _Scale2;
    float Size3 = 15; static float Scale3; static float _Scale3;
    float Size4 = 20; static float Scale4; static float _Scale4;
    static float drawnScale;

    static Matrix4x4 voxTransform = Matrix4x4.identity;
    static MaterialPropertyBlock block;
    static int tempGridIndex;
    static float tempHealth;
    static int level;
    static List<int> SpawnableWeeds = new List<int>(4);

    static int maxNumber;
    static int indexOfMax;
    static HexDirection newCellType;
    static CorpType newType;
    static CorpType newWeedType;
    static CorpType neighbourWeedType;



    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
		_populatevegpercent = populatePercentage;
        _StressDecreaseAmount = stressDecreaseAmount;
        _StressIncreaseAmount = stressIncreaseAmount;
        _stressincreasepercentage = stressincreasepercentage;
        _stressspreadpercentage = stressSpreadPercentage;
        _StressMinWeedAge = stressMinWeedAge;
        _RandomWeedGeminationPercentage = randomWeedGeminationPercentage;
        _treeInfectionAge = weedGerminationAge;
        _treeInfectionPercentage = weedGerminationPercentage;
        _healthincreasepercent = weedAgingPercentage;
        _weedmaxhealthchange = weedAgingAmount;
		_allweeds = allweeds;

        _Scale1 = Size1;
        _Scale2 = Size2;
        _Scale3 = Size3;
        _Scale4 = Size4;

    }



    public static void PaperRules(HexCell cell, List<HexDirection>[] Neighbours, List<int>[] Neighbourhealth, List<int>[] LevelNeighbourHealth, List<HexDirection>[] LevelNeighbourPos, List<HexDirection> sizeNeighboursPos, List<int> neighbourSize, List<HexDirection> hiddenNeighbours, System.Random num)
    {
        tempGridIndex = HexCoordinates.iFh(cell.coordinates);
        //tempIntensity = CSV.getIntensity (SingleMeshRender._intensityMap, tempGridIndex);
        float elevation = CSV.getIntensity(CSVmaps.Elevation, tempGridIndex);
        float hydro = CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex);
        float vegetation = CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex);
        float humanactivity = CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex);
        
        // Use a tempory variable which will vary when this cell is different 
        // cororations and at a location of different intensity values.
        populatevegpercent = _populatevegpercent;
        stressdecreaseamount = _StressDecreaseAmount;
        stressincreaseamount = _StressIncreaseAmount;
        stressincreasepercentage = _stressincreasepercentage;
        stressspreadpercent = _stressspreadpercentage;
        StressSpreadDecay = _StressSpreadDecay;
        StressMinWeedAge = _StressMinWeedAge;
        treeInfectionAge = _treeInfectionAge;
        treeInfectionPercentage = _treeInfectionPercentage;
        RandomWeedGeminationPercentage = _RandomWeedGeminationPercentage;
        healthIncreasePercentage = _healthincreasepercent;
        healthIncreasePerTurn = _weedmaxhealthchange;




        // Stress spreading rules
        if (cell.type == CorpType.Tree && LevelNeighbourHealth[1].Count > 0)
        {
            // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
            maxNumber = LevelNeighbourHealth[1].Max();
            indexOfMax = LevelNeighbourHealth[1].FindIndex(x => x == maxNumber);
            newCellType = LevelNeighbourPos[1][indexOfMax];
            newWeedType = cell.GetNeighbor(newCellType).type;
            if (CorpType.Blackberry == newWeedType)
            {

                treeInfectionPercentage = (21 * CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) / 2);

                stressincreasepercentage = 2;
            }

            if (CorpType.Fennel == newWeedType)
            {

                treeInfectionPercentage = 3 * CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex);

                stressincreasepercentage = 30;
            }

            if (CorpType.Stinkwort == newWeedType)
            {

                //float increaseIndex = Mathf.Sqrt(CSV.getIntensity(CSVmaps.Topo, tempGridIndex));
                treeInfectionPercentage = 6 * (CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex));
                //best case (low vegetation,low elevation): spreadpercent=6, worst case (high vegetation, high elevation): spreadpercent=2.7

                stressincreasepercentage = 8;

            }

            if (CorpType.Hawthorn == newWeedType)
            {


                if (CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) == 1)
                {
                    treeInfectionPercentage = 4;      // LevelNeighbourHealth[1].Max()
                }
                else
                {
                    treeInfectionPercentage = 2;
                }


                stressincreasepercentage = 20;
            }
        }



        lock (CSV.specialRulesLock)
        {
            if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 2)
            {
                treeInfectionPercentage = 8;
            }
        }



        if (CorpType.Blackberry == cell.type)
        {
            healthIncreasePerTurn = 7;
            healthIncreasePercentage = 100;

        }
        if (CorpType.Fennel == cell.type)
        {
            healthIncreasePerTurn = 15;
            healthIncreasePercentage = 35;

        }
        if (CorpType.Stinkwort == cell.type)
        {
            healthIncreasePerTurn = 3;
            healthIncreasePercentage = 50;

        }
        if (CorpType.Hawthorn == cell.type)
        {
            healthIncreasePerTurn = 20;
            healthIncreasePercentage = 65;
        }

        //this is K1 rule

        lock (CSV.specialRulesLock)
        {
            if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -1)
            {
                healthIncreasePercentage = 0;
            }
        }

        lock (CSV.specialRulesLock)
        {
            //This will spawn trees on empty cells
            if (cell.type == CorpType.Empty && CSV.getIntensity(CSVmaps.River, tempGridIndex) < 1 && (num.Next(0, 10000) < (int)(populatevegpercent * 100)))
            {
                cell.changeType(CorpType.Tree, 1);
                CSV.lowRender[tempGridIndex] = 0;
            }
            // If your tree

            else if (cell.type == CorpType.Tree)
            {
                // Special rules which trump all other rules
                // For example, will spread weed if any weed is surrounding it. will become oldest of the surrounding weeds
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 1 && Neighbours[6].Count > 0)
                {
                    // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
                    maxNumber = Neighbourhealth[6].Max();
                    indexOfMax = Neighbourhealth[6].FindIndex(x => x == maxNumber);
                    newCellType = Neighbours[6][indexOfMax];
                    newType = cell.GetNeighbor(newCellType).type;

                    cell.changeType(newType, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                else if (neighbourSize.Contains(4))
                {
                    indexOfMax = neighbourSize.FindIndex(x => x == 4); // google lambda expressions....
                    newCellType = sizeNeighboursPos[indexOfMax];
                    newType = cell.GetNeighbor(newCellType).type;
                    cell.changeType(newType, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                //this will lit trees on fire by its fire neighbours
                else if (LevelNeighbourHealth[1].Count > 0 && LevelNeighbourHealth[1].Max() > treeInfectionAge && (num.Next(0, 10000) < (int)(treeInfectionPercentage * 100)))
                {
                    /////////////////////////
                    // add according to intenity values
                    /////////////////////////
                    cell.changeType(newWeedType, 100);

                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                //this will randomly lit trees on fire
                else if (num.Next(0, 100000000) < (int)(RandomWeedGeminationPercentage * 1000000))
                {
                    SpawnableWeeds.Clear();
                    for (int i = 0; i < 4; i++)
                    {
                        if (CAManager._typeCount[2 + i] < 10)
                        {
                            SpawnableWeeds.Add(2 + i);
                            //CAManager._typeCount [2 + i] += 10;
                        }
                    }
                    if (SpawnableWeeds.Count > 0)
                    {
                        CorpType newWeedType = (CorpType)SpawnableWeeds[num.Next(0, SpawnableWeeds.Count)];
                        // if you are within an intensity map of blah, spawn, otherwise dont....

                        if (_allweeds)
                        {

                            if (newWeedType == CorpType.Blackberry && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.5)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Fennel && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.5)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Stinkwort && CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex) < 0.5)
                            {

                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Hawthorn && CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) > 0.300519)
                            {

                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                        }
                        else
                        {
                            if (newWeedType == CorpType.Fennel && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.3)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Hawthorn && CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) > 0.300519)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                        }

                    }
                }
                //this increases the stress of a tree with a fire neighbour
                else if (cell.health < 99 && LevelNeighbourHealth[1].Count > 0 && LevelNeighbourHealth[1].Max() > StressMinWeedAge && (num.Next(0, 10000) < (int)(stressincreasepercentage * 100)))
                {
                    // the StressIncreaseAmount can change depending on neighbour type, this may help influence the spread...

                    if (cell.health + stressincreaseamount >= 99)
                    {
                        cell.health = 99;
                    }
                    else
                    {
                        cell.health += stressincreaseamount;
                    }
                }

                //this will create stress amongst stressed tree neighbours
                if (Neighbourhealth[(int)CorpType.Tree].Count > 0 && Neighbourhealth[(int)CorpType.Tree].Max() > cell.health && (num.Next(0, 10000) < (int)(stressspreadpercent * 100)))
                {
                    cell.health = (int)(Neighbourhealth[(int)CorpType.Tree].Max() * 0.9f);
                }
                else if (cell.type == CorpType.Tree && cell.health > stressdecreaseamount)
                {
                    cell.health -= num.Next(0, stressdecreaseamount);
                }


            }

            // If your a weed type

            else if (cell.type > CorpType.Tree)
            {
                if (num.Next(0, 10000) < (int)(healthIncreasePercentage * 100))
                {
                    cell.health += (num.Next(0, healthIncreasePerTurn));
                }


                //this can be used later to make rules for different levels

                if (cell.health >= 100 && cell.health < 200)
                {
                }
                else if (cell.health >= 200 && cell.health < 300)
                {
                }
                else if (cell.health >= 300 && cell.health < 400)
                {
                }
                else if (cell.health >= 400 && cell.health < 500)
                {
                }


                //this is the fertile rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 1)
                {
                    cell.changeType(CorpType.Tree, 1);
                    CSV.lowRender[tempGridIndex] = 0;
                }

                //this is K2 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -2)
                {
                    CSV.addToIntensityValue(0.6f, CSVmaps.Hydrology, tempGridIndex);
                }

                //this is K3 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -3)
                {
                    cell.changeType(CorpType.Blackberry, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }

                //this is K4 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -4)
                {
                    HexCell.makeLarge(cell, tempGridIndex, 4);
                }

                if (cell.size == 4)
                {
                    if (cell.type == CorpType.Blackberry)
                    {

                        int sizeOfEffect = 5;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -1;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -1);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Fennel)
                    {

                        int sizeOfEffect = 4;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -2;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -2);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Stinkwort)
                    {

                        int sizeOfEffect = 3;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -3;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -3);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Hawthorn)
                    {

                        int sizeOfEffect = 2;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -4;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -4);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Appy rules to weeds where the parameters have been modified according to the intensity map and weed type
                if (cell.health >= 500)
                {
                    if (cell.size != 4)
                    {
                        cell.changeType(CorpType.Empty, 0);
                        cell.NewSize = 0;

                        CSV.lowRender[tempGridIndex] = 0;
                    }
                    else
                    {
                        cell.health = 499;
                    }
                }
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -5)
                {
                    cell.changeType(CorpType.Empty, 0);
                    cell.NewSize = 0;
                    CSV.lowRender[tempGridIndex] = 0;
                    CAManager._totalKilled++;
                }
            }
        }
        MultiSizeSystem.MakeLarger(cell, tempGridIndex, Neighbours, hiddenNeighbours, num);


    }

    public static void TestingRules(HexCell cell, List<HexDirection>[] Neighbours, List<int>[] Neighbourhealth, List<int>[] LevelNeighbourHealth, List<HexDirection>[] LevelNeighbourPos, List<HexDirection> sizeNeighboursPos, List<int> neighbourSize, List<HexDirection> hiddenNeighbours, System.Random num)
    {
        tempGridIndex = HexCoordinates.iFh(cell.coordinates);
        //tempIntensity = CSV.getIntensity (SingleMeshRender._intensityMap, tempGridIndex);
        float elevation = CSV.getIntensity(CSVmaps.Elevation, tempGridIndex);
        float hydro = CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex);
        float vegetation = CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex);
        float humanactivity = CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex);


        // Use a tempory variable which will vary when this cell is different 
        // cororations and at a location of different intensity values.
        populatevegpercent = _populatevegpercent;
        stressdecreaseamount = _StressDecreaseAmount;
        stressincreaseamount = _StressIncreaseAmount;
        stressincreasepercentage = _stressincreasepercentage;
        stressspreadpercent = _stressspreadpercentage;
        StressSpreadDecay = _StressSpreadDecay;
        StressMinWeedAge = _StressMinWeedAge;
        treeInfectionAge = _treeInfectionAge;
        treeInfectionPercentage = _treeInfectionPercentage;
        RandomWeedGeminationPercentage = _RandomWeedGeminationPercentage;
        healthIncreasePercentage = _healthincreasepercent;
        healthIncreasePerTurn = _weedmaxhealthchange;




        // Stress spreading rules
        if (cell.type == CorpType.Tree && LevelNeighbourHealth[1].Count > 0)
        {
            // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
            maxNumber = LevelNeighbourHealth[1].Max();
            indexOfMax = LevelNeighbourHealth[1].FindIndex(x => x == maxNumber);
            newCellType = LevelNeighbourPos[1][indexOfMax];
            newWeedType = cell.GetNeighbor(newCellType).type;
            if (CorpType.Blackberry == newWeedType)
            {

                treeInfectionPercentage = (21 * CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) / 2);

                stressincreasepercentage = 2;
            }

            if (CorpType.Fennel == newWeedType)
            {

                treeInfectionPercentage = 3 * CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex);

                stressincreasepercentage = 30;
            }

            if (CorpType.Stinkwort == newWeedType)
            {

                //float increaseIndex = Mathf.Sqrt(CSV.getIntensity(CSVmaps.Topo, tempGridIndex));
                treeInfectionPercentage = 6 * (CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex));
                //best case (low vegetation,low elevation): spreadpercent=6, worst case (high vegetation, high elevation): spreadpercent=2.7

                stressincreasepercentage = 8;

            }

            if (CorpType.Hawthorn == newWeedType)
            {


                if (CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) == 1)
                {
                    treeInfectionPercentage = 4;      // LevelNeighbourHealth[1].Max()
                }
                else
                {
                    treeInfectionPercentage = 2;
                }


                stressincreasepercentage = 20;
            }
        }



        lock (CSV.specialRulesLock)
        {
            if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 2)
            {
                treeInfectionPercentage = 8;
            }
        }



        if (CorpType.Blackberry == cell.type)
        {
            healthIncreasePerTurn = 7;
            healthIncreasePercentage = 100;

        }
        if (CorpType.Fennel == cell.type)
        {
            healthIncreasePerTurn = 15;
            healthIncreasePercentage = 35;

        }
        if (CorpType.Stinkwort == cell.type)
        {
            healthIncreasePerTurn = 3;
            healthIncreasePercentage = 50;

        }
        if (CorpType.Hawthorn == cell.type)
        {
            healthIncreasePerTurn = 20;
            healthIncreasePercentage = 65;
        }

        //this is K1 rule

        lock (CSV.specialRulesLock)
        {
            if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -1)
            {
                healthIncreasePercentage = 0;
            }
        }

        lock (CSV.specialRulesLock)
        {
            //This will spawn trees on empty cells
            if (cell.type == CorpType.Empty && CSV.getIntensity(CSVmaps.River, tempGridIndex) < 1 && (num.Next(0, 10000) < (int)(populatevegpercent * 100)))
            {
                cell.changeType(CorpType.Tree, 1);
                CSV.lowRender[tempGridIndex] = 0;
            }
            // If your tree

            else if (cell.type == CorpType.Tree)
            {
                // Special rules which trump all other rules
                // For example, will spread weed if any weed is surrounding it. will become oldest of the surrounding weeds
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 1 && Neighbours[6].Count > 0)
                {
                    // This will get the largest level 1 and then find the index and then the hexPositon (N/S/E/W) so the type can be known
                    maxNumber = Neighbourhealth[6].Max();
                    indexOfMax = Neighbourhealth[6].FindIndex(x => x == maxNumber);
                    newCellType = Neighbours[6][indexOfMax];
                    newType = cell.GetNeighbor(newCellType).type;

                    cell.changeType(newType, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                else if (neighbourSize.Contains(4))
                {
                    indexOfMax = neighbourSize.FindIndex(x => x == 4); // google lambda expressions....
                    newCellType = sizeNeighboursPos[indexOfMax];
                    newType = cell.GetNeighbor(newCellType).type;
                    cell.changeType(newType, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                //this will lit trees on fire by its fire neighbours
                else if (LevelNeighbourHealth[1].Count > 0 && LevelNeighbourHealth[1].Max() > treeInfectionAge && (num.Next(0, 10000) < (int)(treeInfectionPercentage * 100)))
                {
                    /////////////////////////
                    // add according to intenity values
                    /////////////////////////
                    cell.changeType(newWeedType, 100);

                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }
                //this will randomly lit trees on fire
                else if (num.Next(0, 100000000) < (int)(RandomWeedGeminationPercentage * 1000000))
                {
                    SpawnableWeeds.Clear();
                    for (int i = 0; i < 4; i++)
                    {
                        if (CAManager._typeCount[2 + i] < 10)
                        {
                            SpawnableWeeds.Add(2 + i);
                            //CAManager._typeCount [2 + i] += 10;
                        }
                    }
                    if (SpawnableWeeds.Count > 0)
                    {
                        CorpType newWeedType = (CorpType)SpawnableWeeds[num.Next(0, SpawnableWeeds.Count)];
                        // if you are within an intensity map of blah, spawn, otherwise dont....

                        if (_allweeds)
                        {

                            if (newWeedType == CorpType.Blackberry && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.5)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Fennel && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.5)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Stinkwort && CSV.getIntensity(CSVmaps.Vegetation, tempGridIndex) < 0.5)
                            {

                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Hawthorn && CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) > 0.300519)
                            {

                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                        }
                        else
                        {
                            if (newWeedType == CorpType.Fennel && CSV.getIntensity(CSVmaps.Hydrology, tempGridIndex) < 0.3)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                            if (newWeedType == CorpType.Hawthorn && CSV.getIntensity(CSVmaps.humanactivity, tempGridIndex) > 0.300519)
                            {
                                cell.changeType(newWeedType, 100);
                                if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                                {
                                    CSV.lowRender[tempGridIndex] = 1;
                                }
                            }
                        }

                    }
                }
                //this increases the stress of a tree with a fire neighbour
                else if (cell.health < 99 && LevelNeighbourHealth[1].Count > 0 && LevelNeighbourHealth[1].Max() > StressMinWeedAge && (num.Next(0, 10000) < (int)(stressincreasepercentage * 100)))
                {
                    // the StressIncreaseAmount can change depending on neighbour type, this may help influence the spread...

                    if (cell.health + stressincreaseamount >= 99)
                    {
                        cell.health = 99;
                    }
                    else
                    {
                        cell.health += stressincreaseamount;
                    }
                }

                //this will create stress amongst stressed tree neighbours
                if (Neighbourhealth[(int)CorpType.Tree].Count > 0 && Neighbourhealth[(int)CorpType.Tree].Max() > cell.health && (num.Next(0, 10000) < (int)(stressspreadpercent * 100)))
                {
                    cell.health = (int)(Neighbourhealth[(int)CorpType.Tree].Max() * 0.9f);
                }
                else if (cell.type == CorpType.Tree && cell.health > stressdecreaseamount)
                {
                    cell.health -= num.Next(0, stressdecreaseamount);
                }


            }

            // If your a weed type

            else if (cell.type > CorpType.Tree)
            {
                if (num.Next(0, 10000) < (int)(healthIncreasePercentage * 100))
                {
                    cell.health += (num.Next(0, healthIncreasePerTurn));
                }


                //this can be used later to make rules for different levels

                if (cell.health >= 100 && cell.health < 200)
                {
                }
                else if (cell.health >= 200 && cell.health < 300)
                {
                }
                else if (cell.health >= 300 && cell.health < 400)
                {
                }
                else if (cell.health >= 400 && cell.health < 500)
                {
                }


                //this is the fertile rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == 1)
                {
                    cell.changeType(CorpType.Tree, 1);
                    CSV.lowRender[tempGridIndex] = 0;
                }

                //this is K2 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -2)
                {
                    CSV.addToIntensityValue(0.6f, CSVmaps.Hydrology, tempGridIndex);
                }

                //this is K3 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -3)
                {
                    cell.changeType(CorpType.Blackberry, 100);
                    if (num.Next(0, 10000) < MultiSizeSystem._PercentageChanceForLowPoly * 100f)
                    {
                        CSV.lowRender[tempGridIndex] = 1;
                    }
                }

                //this is K4 rule
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -4)
                {
                    HexCell.makeLarge(cell, tempGridIndex, 4);
                }

                if (cell.size == 4)
                {
                    if (cell.type == CorpType.Blackberry)
                    {

                        int sizeOfEffect = 5;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -1;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -1);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Fennel)
                    {

                        int sizeOfEffect = 4;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -2;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -2);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Stinkwort)
                    {

                        int sizeOfEffect = 3;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -3;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -3);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (cell.type == CorpType.Hawthorn)
                    {

                        int sizeOfEffect = 2;  // this is how many neighbour rings will this apply to
                        if (tempGridIndex >= 0 && tempGridIndex < CSV.dataMap.Count)
                        {
                            //HexCoordinates.hFi(tempGridIndex);  // this is how you change a grid index into a hex coordinate so you can circle around that location
                            HexCoordinates hexCoor = cell.coordinates;
                            for (int dx = -sizeOfEffect; dx <= sizeOfEffect; dx++)
                            {
                                for (int dz = -sizeOfEffect; dz <= sizeOfEffect; dz++)
                                {
                                    if (Mathf.Abs(dx + dz) <= sizeOfEffect)
                                    {
                                        int tempIndex = HexCoordinates.iFh(new HexCoordinates(hexCoor.X + dx, hexCoor.Z + dz));
                                        if (tempIndex >= 0 && tempIndex < CSV.dataMap.Count)
                                        {
                                            if (CSV.SpecialRules.ContainsKey(tempIndex))
                                            {
                                                CSV.SpecialRules[tempIndex] = -4;        // this will add this value to the specialsRules list
                                            }
                                            else
                                            {
                                                CSV.SpecialRules.Add(tempIndex, -4);        // this will add this value to the specialsRules list
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Appy rules to weeds where the parameters have been modified according to the intensity map and weed type
                if (cell.health >= 500)
                {
                    if (cell.size != 4)
                    {
                        cell.changeType(CorpType.Empty, 0);
                        cell.NewSize = 0;

                        CSV.lowRender[tempGridIndex] = 0;
                    }
                    else
                    {
                        cell.health = 499;
                    }
                }
                if (CSV.SpecialRules.ContainsKey(tempGridIndex) && CSV.SpecialRules[tempGridIndex] == -5)
                {
                    cell.changeType(CorpType.Empty, 0);
                    cell.NewSize = 0;
                    CSV.lowRender[tempGridIndex] = 0;
                    CAManager._totalKilled++;
                }
            }
        }
        MultiSizeSystem.MakeLarger(cell, tempGridIndex, Neighbours, hiddenNeighbours, num);


    }

    public static void SingleRenderer(HexCell cell, int gridIndex, Mesh[] meshPerType, Material[] MaterialsPerType)
    {
        if (cell != null && cell.health >= 0 && !cell.isHidden)
        {
            cell.currentPosition = Vector3.Lerp(cell.currentPosition, cell.cellPosition, Time.deltaTime);
            level = cell.health / 100;
            tempHealth =(1.0f * (cell.health - (100 * level) + 100) / 100) * level;      // Scale health from Total 0 to 500 to relative 0.5 to 1 for each type
            Scale1 = _Scale1;
            Scale2 = _Scale2;
            Scale3 = _Scale3;
            Scale4 = _Scale4;

            // Check if this is a large cell, ie the size, 0 is normal, size=1 is 7up and so on. Will only auto make a size 1,
            if (cell.size == 0)
            {
                drawnScale = 1;
            }
            else if (cell.size == 1)
            {
                drawnScale = Scale1;
            }
            else if (cell.size == 2)
            {
                drawnScale = Scale2;
            }
            else if (cell.size == 3)
            {
                drawnScale = Scale3;
            }
            else if (cell.size == 4)
            {
                drawnScale = Scale4;
            }

            if (cell.type == CorpType.Tree)
            {
                if (cell.health == 0)
                {
                    voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) + 2, cell.currentPosition.z), Quaternion.Euler(-90, 0, 0), new Vector3(1, 1, 1)*0.2f );
                    Graphics.DrawMesh(meshPerType[3], voxTransform, MaterialsPerType[3], 0, null, 0, block);
                }
                else if (cell.health > _StressDecreaseAmount)
                {
                    voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex) + 2, cell.currentPosition.z), Quaternion.Euler(-90, 0, 0), new Vector3(1, 1, 1 + 3f*(1.0f * cell.health) / 100f) );
                    Graphics.DrawMesh(meshPerType[0], voxTransform, MaterialsPerType[0], 0, null, 0, block);

                }
            }
            else if (cell.type > CorpType.Tree)
            {
                voxTransform.SetTRS(new Vector3(cell.currentPosition.x, CSV.getIntensity(SingleMeshRender._intensityMap, gridIndex)+2, cell.currentPosition.z), Quaternion.Euler(0, 0, 0), new Vector3(1, tempHealth, 1) * drawnScale);
                Graphics.DrawMesh(meshPerType[cell.health / 100 ], voxTransform, MaterialsPerType[cell.health / 100], 0, null, 0, block);
            }
        }
    }
}
